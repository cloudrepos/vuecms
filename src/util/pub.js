var host = location.host;
//差异化配置
var domainConf = {
    'localhost:8080': {apiDomain: 'http://api.cms.lc/', loginPage: 'http://localhost:8080/login'},//调试环境
    'admin.cms.lc': {apiDomain: 'http://api.cms.lc/', loginPage: 'http://admin.cms.lc/login'},//本地编译域名
    'mvc.qn40.com': {apiDomain: 'http://mvc_apix.qn40.com/', loginPage: 'http://mvc.qn40.com/login'},//线上地址，每个域名添加一行
};
const pub = {
    apiDomain: domainConf[host].apiDomain,    //接口域名
    loginPage: domainConf[host].loginPage,    //登录页
    center: domainConf[host].center,        //应用中心

    pageSize: 10,   //默认一页显示数

    //设置本地缓存,支持数组,对象
    //sessionStorage(关闭浏览器失效)localStorage会一直有效直到用户清除浏览器缓存
    //但是目标退出登录时，遍历清除了缓存，这是为怕有些表格设置参数导致接口返回错误，这样退出再进就行了，稳定之后需要重新考虑哪些参数使用local哪些使用session
    setCache: function(key, value)
    {
    	sessionStorage.setItem(key, JSON.stringify(value));	
    },
    //获取本地缓存
    getCache: function(key)
    {
        var str = sessionStorage.getItem(key);
        if(str == 'undefined' || str == 'null') {
            return '';
        }
    	return JSON.parse(str);
    },
    //清空本地缓存
    emptyCacle: function (){
        let count = sessionStorage.length;
        for(let i = 0; i < count; i++) {
            let key = sessionStorage.key(i);       
            sessionStorage.setItem(key, null);
        }
    },
    //获取token
    token: function ()
    {
        return this.getCache('token');
    },
    //获取admin信息,key为空获取整个对象,有key就获取具体的值
    admin: function (key)
    {
        let admin = this.getCache('admin');
        if(key) {
            return admin ? admin[key] : '';
        }else{
            return admin ? admin : '';
        }
    },
    //时间戳转字符串
    formatDateTime: function(timeStamp) {
        if(timeStamp < 1) return '';
        var date = new Date();  
        date.setTime(timeStamp * 1000);
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? ('0' + m) : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        return y + '-' + m + '-' + d;
    },
    //性别显示
    gender: function (v) {
        return v == '1' ? '男' : '女';
    },
    //是/否
    shifou: function (v) {
        return v == '1' ? '是' : '否';
    },
    //自定义验证规则
    checkPhone: (rule, value, callback) => {
        if(value == '') {
            //return callback(new Error('请输入手机号码'));
            return callback();
        }
        var isMobile = /^1\d{10}$/;
        if (!isMobile.test(value)) {
            return callback(new Error('请输入有效的手机号码'));
        }
        callback();
    },
    //封装类似PHP的$_GET数组, 可以方便获取url什么的参数
    getParmas : function (){
        let url = window.location.search;
        let $_GET = {};
        if(url) {
            let parameter_str = url.split('?')[1];
            if(parameter_str) parameter_str = parameter_str.split('#')[0];
            
            if(!parameter_str) return;
            let parameter_arr = parameter_str.split('&');  
            let tmp_arr;  
            for(let i = 0, len = parameter_arr.length; i <= len -1; i++){  
                tmp_arr = parameter_arr[i].split('=');  
                $_GET[tmp_arr[0]] = decodeURIComponent(tmp_arr[1]);  
            }  
        }
        window.$_GET = $_GET;
    },
    //默认日期
    dftDate (){
        var date = new Date();
        var day = date.getDate();
        if(day < 10) day = '0' + day;
        return this.getCache('dft_month') + '-' + day;
    },
    //当前月份
    currentMonth(){
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth();
        month += 1;
        if(month < 10) month = '0' + month;
        return year + '-' + month;
    },
    //今天
    today(){
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        if(month < 10) month = '0' + month;
        if(day < 10) day = '0' + day;
        return year + '-' + month + '-' + day;
    },
    //复制对象 默认情况下，js的数组和对象赋值是“引用”，
    copy(obj){
        return JSON.parse(JSON.stringify(obj));
    },
    //文件预览或下载
    download(url, name)
    {
        let ext = name.toLowerCase().split('.').splice(-1);
        if(ext == 'pdf' || ext == 'txt' || ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif' || ext == 'bmp' || ext == 'htm' || ext == 'html') {
            //预览
            window.open(url);
        }else{
            //下载
            var url = pub.apiDomain + 'index/download' + '?url=' + url + '&name=' + name;
            location.href = url;
        }
    },
    //公用修改状态接口
    stchange(vue, module, row){
        let api = this.apiDomain + module + '/st/id/'+row.id+'/st/'+row.st;
        vue.$axios.get(api).then((res) => {
            vue.$Message.success('修改状态成功');
        });
    },
    getColumns(cacheKey, columns){
        let setting = pub.getCache(cacheKey);
        if(setting) {
            var newCol = [];
            for(let i in setting) {
                var field = setting[i];
                for(let k in columns) {
                    if(columns[k].title == field) {
                        newCol.push(columns[k]);
                    }
                }
            }
            columns = newCol;
        }
        return columns;
    },
    inArray(value, array)
    {
        for(let k in array) {
            if(value == array[k]) return true;
        }
        return false;
    },
};

export default pub;