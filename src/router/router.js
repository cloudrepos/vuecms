import Vue from 'vue';
import Router from 'vue-router';
import {Message} from 'iview';

Vue.use(Router);

//子路由配置
let sub_router = [
    {
        path: '/',
        name: 'default',
        component: resolve => require(['../components/news/news.vue'], resolve)
    },
    {
        path: '/news',
        name: 'news',
        component: resolve => require(['../components/news/news.vue'], resolve)
    },
    {
        path: '/news_form/:id',
        name: 'news_form',
        component: resolve => require(['../components/news/news_form.vue'], resolve)
    },
    {
        path: '/news_cate',
        name: 'news_cate',
        component: resolve => require(['../components/news/news_cate.vue'], resolve)
    },
    {
        path: '/product',
        name: 'product',
        component: resolve => require(['../components/product/product.vue'], resolve)
    },
    {
        path: '/product_form/:id',
        name: 'product_form',
        component: resolve => require(['../components/product/product_form.vue'], resolve)
    },
    {
        path: '/product_cate',
        name: 'product_cate',
        component: resolve => require(['../components/product/product_cate.vue'], resolve)
    },
    {
        path: '/special',
        name: 'special',
        component: resolve => require(['../components/special/special.vue'], resolve)
    },
    {
        path: '/special_form/:id',
        name: 'special_form',
        component: resolve => require(['../components/special/special_form.vue'], resolve)
    },
    {
        path: '/special_cate',
        name: 'special_cate',
        component: resolve => require(['../components/special/special_cate.vue'], resolve)
    },
    {
        path: '/cate_custom/:id',
        name: 'cate_custom',
        component: resolve => require(['../components/cate_custom.vue'], resolve)
    },
    {
        path: '/ueditor',
        name: 'ueditor',
        component: resolve => require(['../components/sample/ueditor.vue'], resolve)
    },
    {
        path: '/center',
        name: 'center',
        component: resolve => require(['../components/center.vue'], resolve)
    },
    {
        path: '/repeat',
        name: 'repeat',
        component: resolve => require(['../components/repeat/repeat.vue'], resolve)
    },
    {
        path: '/repeat_item/:id',
        name: 'repeat_item',
        component: resolve => require(['../components/repeat/repeat_item.vue'], resolve)
    },
    {
        path: '/piece',
        name: 'piece',
        component: resolve => require(['../components/piece.vue'], resolve)
    },
    {
        path: '/site',
        name: 'site',
        component: resolve => require(['../components/site.vue'], resolve)
    },
    {
        path: '/remark',
        name: 'remark',
        component: resolve => require(['../components/remark.vue'], resolve)
    },
    //系统设置
    {
        path: '/admin',
        name: 'admin',
        component: resolve => require(['../components/system/admin.vue'], resolve)
    },
    {
        path: '/role',
        name: 'role',
        component: resolve => require(['../components/system/role.vue'], resolve)
    },
    {
        path: '/role/:id',
        name: 'role_form',
        component: resolve => require(['../components/system/role_form.vue'], resolve)
    },


    //示例模块
    {
        path: '/resume',
        name: 'resume',
        component: resolve => require(['../components/sample/resume.vue'], resolve)
    },
    {
        path: '/resume/:id',
        name: 'resume_form',
        component: resolve => require(['../components/sample/resume_form.vue'], resolve)
    },
    {
        path: '/notice',
        name: 'notice',
        component: resolve => require(['../components/sample/notice.vue'], resolve)
    },
    {
        path: '/graph',
        name: 'graph',
        component: resolve => require(['../components/graph/graph.vue'], resolve)
    }
];

var router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'login',
            //懒加载模式,不会一次加载所有组件
            component: resolve => require(['../components/common/login.vue'], resolve)
        },
        {
            path: '/',
            component: resolve => require(['../components/common/home.vue'], resolve),
            children: sub_router
        },
    ]
});

export default router;

/**
 * 进入路由/切换路由时运行,相当于PHP框架的钩子
 * next()       正常运行
 * next(false)  停止跳转
 * next('/')    跳转到其他页面
 * next(error)    错误实例
 */
router.beforeEach((to, from, next) => {
    if(to.name == 'login') {
        //无需验证权限的
        next();
    }else{
        //验证登录
        let token = pub.getCache('token');
        if(!token) {
            return router.push('/login');
        }
        if(to.name == 'default') {
            return next();
        }
        //验证路由权限
        let allow_routers = pub.admin('router');//登录时保存的可用路由列表
        if(allow_routers.indexOf(to.name) == -1) {
            Message.error('没有路由权限:' + to.name);
            return router.push({name: from.name});
        }
        next();
    }
})