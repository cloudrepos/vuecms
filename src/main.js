import Vue from 'vue';
import App from './App';
import router from './router/router';
import axios from 'axios';
import "babel-polyfill";

//导入公共函数
import pub from 'src/util/pub';
window.pub = pub;
pub.getParmas();

//弹窗可拖动插件
import 'util/directives.js';

//总线
import bus from 'util/eventBus.js';


Vue.prototype.$axios = axios;
Vue.prototype.bus = bus;


import iView from 'iview';
import 'iview/dist/styles/iview.css';
Vue.use(iView, {size: 'default'});



var app = new Vue({
    router,
    render: h => h(App)
}).$mount('#app');

//全局请求钩子
axios.interceptors.request.use(function(config){
    //附带token
    config.headers['Authorization'] = pub.token();
    //附带全局参数：站点
    config.headers['site-id'] = pub.getCache('site');
    return config;
},function(error){
    return Promise.reject(error);
});


//全局响应钩子
axios.interceptors.response.use((res) => {
    const data = res.data;
    if(data.c > 1000 && data.c < 2000) {
        app.$Message.error(data.m);
        return Promise.reject(res.d);
    }
    if(data.c == 0) {
        return data;
    }else{
        if(data.m) {
            app.$Message.error(data.m);
        }
        return Promise.reject(res.d);
    }
    throw err;
}, (err) => {
    
    if (err && err.response) {
        switch (err.response.status) {
          case 400:
            err.message = '请求错误'
            break
    
          case 403:
            err.message = '拒绝访问'
            break
    
          case 404:
            err.message = `请求地址出错: ${err.response.config.url}`
            break
    
          case 408:
            err.message = '请求超时'
            break
    
          case 500:
            err.message = '服务器内部错误'
            break
    
          case 501:
            err.message = '服务未实现'
            break
    
          case 502:
            err.message = '网关错误'
            break
    
          case 503:
            err.message = '服务不可用'
            break
    
          case 504:
            err.message = '网关超时'
            break
    
          case 505:
            err.message = 'HTTP版本不受支持'
            break
          default:
        }
      }
      app.$Message.error(err.response.status + ':' + err.message);
      return Promise.reject(err)
});