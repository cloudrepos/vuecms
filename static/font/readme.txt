阿里图标库
http://www.iconfont.cn

1、添加新图标的方法
支持自己挑选图标、挑选图标集
如果想批量将图标加入购物车可以在控制台输入
var span = document.querySelectorAll('span.cover-item');
for (var i = 0, len = span.length; i < len; i++) {
     console.log(span[i].click());
}
然后添加到项目，添加完要更新一下生成新的在线css地址

2、使用方式
已经在App.vue中引入
@import "//at.alicdn.com/t/font_652403_x2kfqwu66ewb3xr.css";    /* 阿里图标库，与上面生成的地址一致 */

使用方法,以下两种都可以
<i class="icon-toys"></i>
<el-button icon="icon-toys">计算</el-button>

3、提示
目前这个目录其实没有用，留着是为了方便打开demo_fontclass.html查看图标
