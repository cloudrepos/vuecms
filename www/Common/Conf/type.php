<?php
/**
 * 类型配置
 */
return [
	//样本信息 病理类型列表
	'patient_type' => ['鳞癌','腺癌','未分化癌','横纹肌肉瘤','平滑肌肉瘤','纤维肉瘤','脂肪肉瘤','骨肉瘤','软骨肉瘤','血管肉瘤','淋巴肉瘤','黑色素瘤','其它'],

	//样本信息 分化程度
	'level' => ['高分化','高中分化','中分化','中低分化','低分化','低未分化','未分化','分化差','不能提示分类'],

	//样本信息 活检部位
	'biopsy_organ' => ['肺','鼻咽','喉','胃','肝','食道','结直肠','胰腺','胆','腹膜','肛门','壶腹','口腔','肾','膀胱','前列腺','尿道','宫颈','子宫','卵巢','睾丸','输卵管','阴道','乳腺','皮肤','甲状腺','头颈','脑','淋巴结','骨','胸膜','纵隔','其它'],

	//影像学 检测项目列表
	'pro_list' => ['X线', 'CT', '超声', 'MRI', 'PET-CT'],

	//影像学 检测部位列表
	'organ_list' => [
		'头颅' => ['颅脑', '眼与眼眶', '鞍区', '鼻与鼻窦', ' 颈部 ', '甲状腺', ' 喉 ', '咽 ', ' 腮腺'],
		'胸部' => ['纵膈',  '肺',  ' 胸膜',  '胸壁',  '心脏',  '大血管',  '乳房',  ' 腋窝'],
		'腹部' => ['肝',  '胆',  ' 胰 ',  '胰胆管造影',  ' 脾 ',  '肾 ',  '肾盂输尿管造影',  ' 肾上腺',  ' 后腹膜 ',  ' 盆腔',  ' 膀胱',  '前列腺',  '子宫及附件',  '卵巢',  '腹股沟'],
		'胸腹腔' => ['胸腔定位',  '腹腔定位',  ' 食管',  '胃',  '肠'],
		'脊柱/四肢骨关节' => ['颈椎',  '胸椎',  '腰椎',  '骶椎',  '髂骨',  '四肢',  '关节'],
		'血管/造影' => ['头部血管',  '颈部血管',  '胸主动脉',  '肺动脉',  '腹主动脉',  '肾动脉',  '髂动脉',  '下肢动脉腔静脉',  '门静脉'],
		'其它' => [],
	],

	//肿瘤标志物列表
	'sign_list' => ['AFP','CEA','CA199','CA125','CA153','CA50','CA242','β2-MG','Fe Pro','NSE','HCG','TNF','PSA','TPSA','FPSA','PAP','TG','SCC','NMP22','TPA','TSH','CA724','CYFRA211','TSGF','HE4','Tb','T3','POA','PROGRP','AFU','T4','HPV','HIV','HCV','HBV','WBC','血红素','染色体检测','CD4细胞数','分子基因遗传学检测','其它'],

	//病理学 免疫组化物列表
	'sign_list2' => ['AE1/AE3', 'Cam5.2', 'MNF116', 'PCK', 'CK1', 'CK14', 'NFP', 'ACPP', 'HHF35', 'CD2', 'TERT', 'CCND1', 'Ca15.3','B72.3', 'CD45RA', 'CD45RO', 'CD146', 'MUC1', 'MRP1', 'HMW', 'PAX8', 'P40', 'SF1', 'GPC3', 'ARG1', 'SALL4', 'HBME1', 'GATA3', 'GCDFP15', 'MOC31', 'SP', 'SP-A', 'SP-B', 'SP-C', 'CK', 'CK5/6', 'CK7', 'CK8', 'CK10/13', 'CK18', 'CK19', 'CK20', 'EMA', 'Calretinin', 'CHGA', 'SY', 'F8', 'αSMA', 'FKBP12', 'CD79a', 'F8RA', 'β-catenin', 'CEA', 'CA242', 'PSA', 'PSAP', '34βE12', 'AFP', 'Hepatocyte', 'β-HCG', 'CA125', 'Desmin', 'Actin', 'SMA', 'MSA', 'Myosin', 'Myoglobin', 'Myogen', 'MyoD1', 'FVⅧRag', ' CD31', 'CD34', 'UEA1', 'BNH9', 'CD68/KP-1', 'Lysozyme', 'α-AT', 'α1-ACT', 'CD45/LCA', ' TdT', 'CD30/ki-1', 'Igκ', 'Igλ', 'CD20/L26', 'BLA36', 'PAX5', ' CD10', 'CD21', 'CD23', 'CD35', 'CD38', 'CD3', 'CD5', 'CD43', 'CD1', 'CD4','CD8', 'CD57/Leu-7', ' CD56', 'CD16/Leu-11', 'CD11c/LeuM5', 'CD15/LeuM5', 'S-100','Mac387', 'CD99', ' ALK', 'HLA-DR', ' Bcl-2', 'Bcl-6', 'Bcl-10','Ki-67', 'CD25', 'NSE', 'NF', 'GFAP', ' CA', '5-HT', 'GH', 'PRL', 'ACTH', 'TSH', 'FSH', 'LH', 'TG', 'CT', 'PTH', 'Insulin', 'Glucagon', 'VIP', 'PP', 'Gastrin', 'CgA', 'Syn', 'ER', 'AR', 'PR', 'PCNA', 'Ki67', 'HMB45', 'Melanoma', 'C-erbB-2', 'P53', 'P63', 'P16', 'NM23', 'HBsAg', 'HBcAg', 'HPV', 'HTLV1', 'P504S', 'Inhibin', 'Inhibin-α','Melan-A', 'Vimentin', 'PAX2', 'WT1', 'PLAP', 'CD117', 'OCT3/4','EBV', 'TiA-1', 'Perforin', 'CD7', 'CD138', 'CD163','MPO', 'CD33', 'CD14', 'CDX2', 'GST-π', 'AB/PAS', 'CK8/18','HER2', 'TTF1', 'HMBE1', 'Gelactin-3', 'TPO', 'D2-40','Myogenin', 'VⅢ因子', 'XⅢa因子', 'Villin', ' CKAE1', 'CKAE3', 'MBP','EGFR', 'VEGF', 'E-Cadherin', 'KER', 'HPL', 'ß-Hcg', 'PGP','TOPOⅡα', 'GSTπ', 'Pro-EXC', 'CD207', 'Fli-1', 'P120-catenin', 'COX2','PS2', 'ERCC1', 'PRM1', 'BRCA1', 'uroplakin III', 'Myocobacterium Bovis','Myosin-heavy chain', '其它'],
];