<?php
if(isset($_SERVER['DB_HOST'])) {
	//本地开发环境推荐把数据库信息配置在apache虚拟主机里，省得不同开发人员的配置冲突
	/*<VirtualHost *:80>
		DocumentRoot "D:\www\sangerMVC\api\Public"
		ServerName api.sangermvc.lc
		SetEnv   DB_HOST           192.168.10.51
		SetEnv   DB_USER           sgmvc
		SetEnv   DB_PWD            123456
		SetEnv   DB_NAME           sangermvc
	</VirtualHost>*/
	$db_host = $_SERVER['DB_HOST'];
	$db_user = $_SERVER['DB_USER'];
	$db_pwd = $_SERVER['DB_PWD'];
	$db_name = $_SERVER['DB_NAME'];
}else{
	//线上推荐根据api域名配置不同参数，多一个域名就多一份配置
	if(SITE_DOMAIN == 'mvc_api.qn40.com'){
		$db_host = '127.0.0.1';
		$db_pwd = 'mvc';
		$db_user = 'mvc';
		$db_name = 'mvc';
	}
}



return array(
    'APP_TITLE'=>'API接口',
    'DEFAULT_MODULE' => 'App',
    /* 域名配置 */
    'MODULE_ALLOW_LIST' => array('App'),


   /* 数据库配置 */
     'DB_TYPE' => 'mysqli',
     'DB_HOST' => $db_host,
     'DB_NAME' => $db_name,
     'DB_USER' => $db_user,
     'DB_PWD'  => $db_pwd,
     'DB_PREFIX'  => '',
     'DB_CHARSET' => 'utf8',
     'DB_DEBUG'   =>  true,


    /* 文件缓存配置 */
    'DATA_CACHE_SUBDIR' => true,
    'DATA_PATH_LEVEL' => 3,

    /* URL设置 */
    'URL_MODEL' => 2,

    'UPLOAD_MAX_SIZE' => 52428800, // 文件上传限制,50M,设为0不限制

	'DB_FIELDS_CACHE'  => false,
    'SYNC_DOMAIN' => 'http://service.i-sanger.cn/',


    /**
     * 邮件配置
     */
    'PAGE_TITLE' => 'sangerMVC',
    'EMAIL_SMTP_USERNAME' => 'om@majorbio.com',
    'EMAIL_POP_USERNAME' => 'info@majorbio.com',
    'EMAIL_SMTP_ADDRESS' => 'smtp.majorbio.com',
    'EMAIL_SMTP_PORT' => '80',
    'EMAIL_SMTP_USERNAME' => 'om@majorbio.com',
    'EMAIL_SMTP_PASSWORD' => 'O_M_201606',
    
    'SYNC_LOGIN_KEY'    =>  '2Q6eq2gNXv36N75f4l1K1ECTVl27', //同步登录key
    
    'VIRTUAL_LOGIN'     =>  true,   //虚拟登录
    'TOKEN_OVER_TIME'   =>  86400,   //token超时时间
    'LOAD_EXT_CONFIG'   =>  ['type'], //加载自定义配置
);