<?php
/**
 * 权限节点配置
 * 节点分为3类
 *
 * 1 显示为菜单的页面：
 *  参考"用户列表"节点配置
 *
 * 2 不显示在菜单的页面，例如表单页，他不在菜单里，但是一个实际的页面（注意弹窗不算独立页面）
 *  参考"添加编辑用户"节点配置
 *
 * 3 无页面，纯API，例如“删除用户”接口
 *  参考"删除用户"节点配置
 *
 *
 * 关于API的权限设置:
 * 一般的API不需要单独添加,可以嵌入到页面,菜单节点中
 * 例如/user/ls这个接口,就和"用户列表"页面绑定了,不需要单独添加,也不需要配置给角色，只要有用户列表的权限就有user/ls接口权限
 * array(
        'name'  => 'gtl_wenku_type',
        'url'   => '/gtl/wenku_type',
        'title' => '建库上机人工系数',
        'api'   => '/wenkutype/ls,/wenkutype/st,/wenkutype/save,/wenkutype/sync',
        'type'  => 'menu',
    ),
 * 例如这个菜单打包了wenkutype下的4个接口，只要分配建库上机人工系数页面给谁，他就有这4个接口权限，这样能省很多配置的麻烦
 *
 * 如果API需要精细控制权限,例如删除用户接口,那么要单独添加这个api节点,并且配置给角色
 * 如果不需要太精细,有用户列表权限的人就有删除权限,那么不需要单独添加此接口节点,直接嵌入到"用户列表"页面即可
 */
return array(
	array(
		'icon'  => 'ios-document',
		'url'   => '/news',
		'title' => '资讯',
		'type'  => 'menu',
		'api'	=> '/news/*,news_cate/*',
		'name'  => 'news',
		'subs'	=> array(
			array(
				'icon'  => 'ios-document',
				'url'   => '/',
				'title' => '首页',
				'type'  => 'page',
				'api'	=> '/news/*,news_cate/*',
				'name'  => 'default',
			),
			array(
				'icon'  => 'ios-document',
				'url'   => '/news',
				'title' => '资讯管理',
				'type'  => 'menu',
				'api'	=> '/news/*,news_cate/*',
				'name'  => 'news',
			),
			array(
				'icon'  => 'icon-accountfilling',
				'url'   => '/news_form',
				'title' => '资讯编辑',
				'type'  => 'page',
				'api'	=> '/news/*,news_cate/*',
				'name'  => 'news_form',
			),
			array(
				'icon'  => 'ios-folder-open',
				'url'   => '/category',
				'title' => '分类管理',
				'type'  => 'menu',
				'name'  => 'category',
				'subs'	=> array(
					array(
						'url'   => '/news_cate',
						'title' => '资讯分类',
						'type'  => 'menu',
						'api'	=> '/news_cate/*',
						'name'  => 'news_cate',
					),
					array(
						'url'   => '/ueditor',
						'title' => '百度编辑器',
						'type'  => 'menu',
						'name'  => 'ueditor',
					),
				)
			),
		)
	),
	array(
		'icon'  => 'ios-cart',
		'url'   => '/product',
		'title' => '产品',
		'type'  => 'menu',
		'api'	=> '/product/*,product_cate/*',
		'name'  => 'product',
		'subs'	=> array(
			array(
				'icon'  => 'ios-cart',
				'url'   => '/product',
				'title' => '产品列表',
				'type'  => 'menu',
				'api'	=> '/product/*,product_cate/*',
				'name'  => 'product',
			),
			array(
				'url'   => '/product_form',
				'title' => '产品编辑',
				'type'  => 'page',
				'api'	=> '/product/*,product_cate/*',
				'name'  => 'product_form',
			),
			array(
				'icon'  => 'ios-folder-open',
				'url'   => '/product_cate',
				'title' => '产品分类',
				'type'  => 'menu',
				'api'	=> '/product_cate/*',
				'name'  => 'product_cate',
			),
			array(
				'url'   => '/cate_form',
				'title' => '分类详情',
				'type'  => 'page',
				'api'	=> '/cate/*',
				'name'  => 'cate_form',
			),
		)
	),
	array(
		'icon'  => 'md-ribbon',
		'url'   => '/special',
		'title' => '专题',
		'type'  => 'menu',
		'api'	=> '/special/*,special_cate/*',
		'name'  => 'special',
		'subs'	=> array(
			array(
				'icon'  => 'md-ribbon',
				'url'   => '/special',
				'title' => '专题列表',
				'type'  => 'menu',
				'api'	=> '/special/*,special_cate/*',
				'name'  => 'special',
			),
			array(
				'icon'  => 'icon-accountfilling',
				'url'   => '/special_form',
				'title' => '专题编辑',
				'type'  => 'page',
				'api'	=> '/special/*,special_cate/*',
				'name'  => 'special_form',
			),
			array(
				'icon'  => 'ios-folder-open',
				'url'   => '/special_cate',
				'title' => '专题分类',
				'type'  => 'menu',
				'api'	=> '/special_cate/*',
				'name'  => 'special_cate',
			),
		)
	),
	array(
		'icon'  => 'icon-accountfilling',
		'url'   => '/cate_custom',
		'title' => '自定义字段',
		'type'  => 'page',
		'api'	=> '/cate/*',
		'name'  => 'cate_custom',
	),
	array(
		'icon'  => 'md-more',
		'url'   => '/ads',
		'title' => '其它功能',
		'type'  => 'menu',
		'api'	=> '/ads/*',
		'name'  => 'ads',
		'subs'	=> array(
			array(
				'icon'  => 'md-cash',
				'url'   => '/ads',
				'title' => '广告位',
				'type'  => 'menu',
				'api'	=> '/ads/*',
				'name'  => 'ads',
			),
			array(
				'icon'  => 'md-cash',
				'url'   => '/ads_item',
				'title' => '广告详情',
				'type'  => 'page',
				'api'	=> '/ads/*',
				'name'  => 'ads_item',
			),
			array(
				'icon'  => 'md-crop',
				'url'   => '/section',
				'title' => '片段',
				'type'  => 'menu',
				'api'	=> '/section/*',
				'name'  => 'section',
			),
			array(
				'name'  => 'notice',
				'url'   => '/notice',
				'icon'  => 'ios-notifications',
				'title' => '公告',
				'api'   => '/notice/ls,/notice/get,/notice/submit,/notice/st',
				'type'  => 'menu',
			),
			array(
				'name'  => 'delete_notice',  //没有前端路由,但也要起个名字作为权限主键
				'title' => '删除公告',
				'api'   => '/notice/delete',
				'type'  => 'api',          //纯功能api,没有单独页面
			),
			array(
				'name'  => 'resume',
				'url'   => '/resume',
				'title' => '简历管理',
				'icon'  => 'ios-paper',
				'api'   => '/resume/ls',
				'type'  => 'menu',
			),
			array(
				'name'  => 'resume_form',
				'title' => '添加编辑简历',
				'api'   => '/resume/get, /resume/job, /resume/submit',
				'type'  => 'page',
			),
			array(
				'name'  => 'delete_resume',  //没有前端路由,但也要起个名字作为权限主键
				'title' => '删除用户简历',
				'api'   => '/resume/delete',
				'type'  => 'api',          //纯功能api,没有单独页面
			),
		)
	),
    array(
        'icon'  => 'icon-accountfilling',
        'title' => '个人中心',
        'url'   => '/center',
        'type'  => 'page',
        'name'  => 'center',
    ),
    array(
        'name'  => 'user_form',     //权限主键,必须和前端router.js中的路由name一致
        'title' => '添加编辑用户',
        'api'   => '/user/get, /user/job, /user/submit',      //可用api,如果有多个用逗号隔开
        'type'  => 'page',          //不作为菜单的页面
    ),
    //无页面,API类
    array(
        'name'  => 'delete_user',  //没有前端路由,但也要起个名字作为权限主键
        'title' => '删除用户',
        'api'   => '/user/delete',
        'type'  => 'api',          //纯功能api,没有单独页面
    ),

    array(
        'icon'  => 'md-options',
        'url'   => '/admin',
        'title' => '系统设置',
        'type'  => 'menu',
        'subs'  => array(
            array(
                'name'  => 'role',
                'url'   => '/role',
                'title' => '角色权限',
                'api'   => '/role/ls',
                'icon'  => 'ios-construct',
                'type'  => 'menu',
            ),
            array(
                'name'  => 'role_form',
                'title' => '编辑角色权限',
                'api'   => '/role/*',
                'type'  => 'page',
            ),
            array(
                'name'  => 'admin',       //权限主键,必须和前端router.js中的路由name一致
                'title' => '管理员列表',  //名称
                'url'   => '/admin',     //页面地址
                'api'   => '/admin/ls, /pri_group/map',  //可用api,如果有多个用逗号隔开
                'icon'  => 'md-contacts',
                'type'  => 'menu',     //作为菜单显示, type还有page和api两种,具体请看menu.php中的说明
            ),
        ),
    ),
);