<?php
error_reporting(E_ALL & ~E_NOTICE & ~ E_WARNING & ~E_DEPRECATED);
header("Content-type:text/html; charset=utf-8");

//设置可跨域请求在域名
$allow_domain = array('http://localhost:8080', 'http://cost.lc', 'http://cost_api.lc', 'http://cost.i-sanger.cn');
if(in_array($_SERVER['HTTP_ORIGIN'], $allow_domain)) {
    //header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
}
header("Access-Control-Allow-Origin: *");   //为避免反复删浏览器数据，重启浏览器，先用星号
header("Access-Control-Allow-Methods: PUT,POST,GET,DELETE,OPTIONS");
header("Access-Control-Allow-Headers: access-token,X-Requested-With, accept, origin, content-type");
header("Access-Control-Max-Age: 86400000"); //预检结果缓存时间
header("Cache-Control:no-cache,must-revalidate,no-store");//禁止缓存

define('APP_DEBUG', is_file('./debug'));
define('HTTP_HTTPS', 'http://');
if(APP_DEBUG) {
	ini_set('display_errors', 1);
}

// 定义站点域名
define('SITE_DOMAIN', strip_tags($_SERVER['HTTP_HOST']));
$arr = explode('.', SITE_DOMAIN);
if(count($arr) > 2) {
    unset($arr[0]);
}
//定义一级域名
define('DOMAIN', $domain = implode('.', $arr));
define('APP_PATH', '../');
define ('RUNTIME_PATH', '../Runtime/');

//if(PHP_OS == 'WINNT') usleep(100000);	//本地加些延时，更接近线上环境

require '../ThinkPHP/ThinkPHP.php';