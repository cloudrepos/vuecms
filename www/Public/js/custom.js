// JavaScript Document
$(function(){

	//给全局绑定touchstart事件，为了更兼容active替代hover在移动端中的效果
	//document.body.addEventListener('touchstart',function(){});

	//设置整体比例
	/*10px = 0.1rem*/
	var scale = $("body").width()/1920;
	$("html").css("font-size",100 * scale + 'px');
	$(window).resize(function(){
		var scale = $("body").width()/1920;
		$("html").css("font-size",100 * scale + 'px');
	});

	//顶部--当小于768px时变成底部手机端
	wwHF();
	$(window).resize(function(){wwHF();});
	function wwHF(){
		var $ww = $(window).width();
		if($ww>980){
			$(".Hnav, .Hn2nd-box, .Hlang-box, .pro-nav").attr("style","");
			$(".Hlang-menu").removeClass("cur");
			$("html, body").css("overflow-y","auto");
		}else{
		}
		$ww>768?$(".Footer-wrapper").addClass("on"):$(".Footer-wrapper").removeClass("on");
	}

	//浏览器窗口改变时，移除所有的动画
	var resizeTime;
	$(window).resize(function(){
		clearInterval(resizeTime);
		$("body").addClass("resizing");
		resizeTime = setTimeout(function(){
			$("body").removeClass("resizing");
			clearInterval(resizeTime);
		},1000);
	});

	/*-- 顶部 --*/

	$(".Hsrch-menu").bind("click",function(){
		$(".Hsrch-box").fadeIn(300);
		$(".Hsrch-block .text").focus();
	});
	$(".Hsrch-block .close").bind("click",function(){
		$(".Hsrch-box").fadeOut(300);
	});


	//顶部导航-选中
	$(".Hnav>li.Hnav-cur").css({"left":$(".Hnav>li.cur").position().left + ($(".Hnav>li.cur").outerWidth() - $(".Hnav>li.cur .Hname").width())/2,"width":$(".Hnav>li.cur .Hname").width()});
	var HnavHoverTime1;
	var HnavHoverTime2;
	$(".Hnav>li").hover(function(){
		clearInterval(HnavHoverTime1);
		clearInterval(HnavHoverTime2);
		var $this = $(this);
		var $tHname = $this.find(".Hname")
		HnavHoverTime2 = setTimeout(function(){
			$(".Hnav>li.Hnav-cur").stop().animate({"left":$this.position().left + ($this.outerWidth() - $tHname.width())/2,"width":$tHname.width()},300);

		},100);
	},function(){
		clearInterval(HnavHoverTime1);
		clearInterval(HnavHoverTime2);
		HnavHoverTime1 = setTimeout(function(){
			$(".Hnav>li.Hnav-cur").stop().animate({"left":$(".Hnav>li.cur").position().left + ($(".Hnav>li.cur").outerWidth() - $(".Hnav>li.cur .Hname").width())/2,"width":$(".Hnav>li.cur .Hname").width()});
		},100);
	});


	//手机端下拉
	$(".Header-wrapper .Hmenu-btn").bind("click",function(){
		if($(this).hasClass("cur")){
			$(this).removeClass("cur").siblings(".Hnav").slideUp(300);
		}else{
			if($(".Hsrch-menu").hasClass("cur")) $(".Hsrch-menu").removeClass("cur").siblings(".Hsrch-box").hide();
			$(".Hnav").css("height","auto");
			$(".Hnav")[0].scrollHeight>$(window).height()-$(".Header-cl").height()?$(".Hnav").css("height",$(window).height()-$(".Header-cl").height()):$(".Hnav").css("height","auto");
			$(this).addClass("cur").siblings(".Hnav").slideDown(300);
		}
	});
	$(".Hname i").bind("click",function(){
		if($(window).width()<=980){
			var $this = $(this).parent(".Hname");
			if($this.siblings(".Hn2nd-box").length > 0){
				if($this.hasClass("cur")){
					$this.removeClass("cur").siblings(".Hn2nd-box").slideUp(300,function(){
						$(".Hnav")[0].scrollHeight>$(window).height()-$(".Header-cl").height()?$(".Hnav").css({"height":$(window).height()-$(".Header-cl").height(),"overflow-y":"scroll"}):$(".Hnav").css({"height":"auto","overflow-y":"auto"});
					});
				}else{
					$(".Hnav .Hname").removeClass("cur").siblings(".Hn2nd-box").slideUp(300);
					$this.addClass("cur").siblings(".Hn2nd-box").slideDown(300,function(){
						$(".Hnav").css("height","auto");
						$(".Hnav")[0].scrollHeight>$(window).height()-$(".Header-cl").height()?$(".Hnav").css({"height":$(window).height()-$(".Header-cl").height(),"overflow-y":"scroll"}):$(".Hnav").css({"height":"auto","overflow-y":"auto"});
					});
				}
				return false;
			}
		}
	});

	$(".Hlang-menu").bind("click",function(){
		if($(window).width()<=980){
			$(this).hasClass("cur")?$(this).removeClass("cur").siblings(".Hlang-box").slideUp(300):$(this).addClass("cur").siblings(".Hlang-box").slideDown(300,function(){$(this).css("overflow","inherit")});
		}
	});


	/*-- 底部 --*/
	$(".Fnav dt").bind("click",function(){
		if($(window).width()<=640){
			if($(this).hasClass("cur")){
				$(this).removeClass("cur").siblings("dd").slideUp(300);
			}else{
				$(".Fnav dt").removeClass("cur").siblings("dd").slideUp(300);
				$(this).addClass("cur").siblings("dd").slideDown(300);
			}
		}
	});

	/*-- 产品 --*/
	$(".pn-cur").bind("click",function(){
		$(".pro-nav").slideDown(300);
		$("html, body").css("overflow-y","hidden");
	});
	$(".pn-rtn").bind("click",function(){
		$(".pro-nav").slideUp(300);
		$("html, body").css("overflow-y","auto");
	});


})

/*--返回顶部动画--*/
//goTop(500);//500ms内滚回顶部
function goTop(times){
	if (navigator.userAgent.indexOf('Firefox') >= 0){//firefox专用()
		document.documentElement.scrollTop=0;
	}else{$('html,body').animate({scrollTop: 0}, times);}
}

/*--判断是否为IE9及以下版本--*/
function IE(fn){
	if(navigator.userAgent.indexOf("MSIE")>0){
		if( (navigator.userAgent.indexOf("MSIE 7.0")>0) || (navigator.userAgent.indexOf("MSIE 8.0")>0) ||(navigator.userAgent.indexOf("MSIE 9.0")>0 && !window.innerWidth) || (navigator.userAgent.indexOf("MSIE 9.0")>0)){
			fn();
			return true;
		}
	}
}

/* 滚动效果1
 function isScrolledIntoView(elem) {
 var docViewTop = $(window).scrollTop();
 var docViewBottom = docViewTop + $(window).height();
 var elemTop = $(elem).offset().top;
 if (elemTop - $(elem).height()/3 < docViewTop) {
 return true;
 }
 } */

/* 滚动效果1 */
function isScrolledIntoView(elem){
	var scrHeight = window.screen.availHeight;
	var boxPos = $(elem).offset().top;
	var winPos = $(window).scrollTop()+scrHeight-500;
	if(boxPos<winPos){return true}
}


/* 滚动效果2 */
function scrollArrty(scrollArrtyData){
	var scrollArrty = scrollArrtyData;
	var scrI = 0;
	var scrHeight = window.screen.availHeight;
	$(window).scroll(function(){scrollFn();})
	function scrollFn(){
		if(scrI==scrollArrty.length){return false;}
		if($(scrollArrty[scrI]).length == 0){
			scrI++;
			if(scrI<scrollArrty.length){scrollFn();}else{return false;}
		}
		var boxPos = $(scrollArrty[scrI]).offset().top;
		var winPos = $(window).scrollTop()+scrHeight-300;
		if(boxPos<winPos){
			$(scrollArrty[scrI]).addClass('active');
			scrI++;
			if(scrI<scrollArrty.length){scrollFn();
			}else{return false;}
		}else{return false;}
	}
	scrollFn();
}