/*常用JS，必须先引用jquery.js*/

//JS版的Server.UrlEncode编码函数
function urlEncode(str)
{
	str = str.replace(/./g,function(sHex)
	{
		window.EnCodeStr = "";
		window.sHex = sHex;
		window.execScript('window.EnCodeStr=Hex(Asc(window.sHex))',"vbscript");
		return window.EnCodeStr.replace(/../g,"%$&");
	});
	return str;
}

function trim(s){return  s.replace(/(^\s*)|(\s*$)/g,  "");}

function IndexBanner(){
	var mySwiper = new Swiper('.swiper',{
		preventLinks : true,
		autoplay : 5000,
		loop : true,
		watchActiveIndex : true,
		paginationClickable :true,
		freeMode : false,
		autoplayDisableOnInteraction : false,
		pagination : '.swiper-pagination'
	});
}

var Ajax_msg="获取失败";
/*通用表单提交
 <form onsubmit="return initbook(this);" data-title="问题反馈" url="{siteconfig.SiteRoot}handler/ajax.ashx?action=save_book">
 <input type="text" name="txtName" data-require="true" nullmsg="请填写您的名字！" data-title="名字：" data-inp="true" data-type="text">
 <input type="text" name="txtMobile" title="手机号码：" data-inp="true" data-type="text">
 </form>
 说明：
 <form onsubmit="return initbook(this);" data-title="问题反馈" url="{siteconfig.SiteRoot}handler/ajax.ashx?action=save_book">  data-title--必填,表单的标题，方便后台查看是属于哪个表单
 <input type="text" name="txtName" data-require="true" nullmsg="请填写您的名字！" data-title="名字：" data-inp="true" data-type="text">
 title、data-inp、data-type三个属性必不可少
 data-require="true" 是否必填项
 nullmsg="请填写您的名字" 必填项为空时的提示文字
 data-title="名字："  输入项的标题，方便后台查看此项内容的意义
 data-inp="true" 是否将此项内容提交到后台
 data-type="text" 可选：text、select、radio、checkbox
 */
function initbook(theform){
	try{
		var cansubmit = true;
		var dataType = {
			tel:/^((\d{3,4}-)?\d{7,8})$|(1[3|4|5|7|8]\d{9})$/,
			email:/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
			n:/^\d+$/,
			m:/^13[0-9]{9}$|14[0-9]{9}|15[0-9]{9}$|18[0-9]{9}$/
		};
		$(theform).find('[data-require="true"]').each(function(index, element) {
			var value="",data_type=$(this).attr('data-type'),name=$(this).prop('name'),regstr=$(this).attr('data-reg');
			if(data_type=='text'||data_type=='select'){
				value = $(this).val();
			}else if(data_type=='radio'){
				$('input[name="'+name+'"]:checked').val();
			}else if(data_type=='checkbox'){
				$('input[name="'+name+'"]:checked').each(function(index, element) {
					value+=$(this).val();
				});
			}
			if(value==''){
				//alert($(this).attr('nullmsg'));
				$(this).addClass('error');
				$(this).focus();
				cansubmit = false;
				return false;
			}
			console.log(regstr);
			if(regstr!=undefined&&regstr!=''&&eval(dataType[regstr]).test(value)==false){
				//alert($(this).attr('errmsg'));
				$(this).addClass('error');
				$(this).focus();
				cansubmit = false;
				return false;
			}
		});
		if(cansubmit){
			var title=$(theform).attr('data-title');
			var url = $(theform).attr('url');
			var content="";
			$(theform).find('[data-inp="true"]').each(function(index, element) {
				var _title=$(this).attr('data-title'),value="",data_type=$(this).attr('data-type'),name=$(this).prop('name');
				if(data_type=='text'||data_type=='select'){
					value = $(this).val();
				}else if(data_type=='radio'){
					$('input[name="'+name+'"]:checked').val();
				}else if(data_type=='checkbox'){
					$('input[name="'+name+'"]:checked').each(function(index, element) {
						value+=$(this).val();
					});
				}
				if(_title==undefined){
					content+='<p>'+value+'</p>';
				}else{
					content+='<p>'+_title+' '+value+'</p>';
				}
			});
			$.post(url,"txttitle="+title+'&txtcontent='+content,function(data){
				alert(data.msg);
				if(data.status==1){
					window.location.reload();
				}
			},"json");
		}
		return false;
	}catch(e){
		console.log(e.message);
		return false;
	}
}

/*搜索*/
function CheckSearch(theform){
	var key = $(theform).find('[name="key"]').val();
	if(key==''){
		return false;
	}
	var url = $(theform).attr('url');
	if(url.indexOf('?')!=-1){
		url += '&';
	}else{
		url += '?';
	}
	url += 'keyword='+key;
	window.location.href = url;
	return false;
}

function IndexCase(){
	$('.indexcase_con li').each(function(m) {
		$(this).hover(function() {
			$(this).find('a').slideDown(200);
		}, function() {
			$(this).find('a').slideUp(200);
		});
	});
}

function ChangeCaseImg(){
	var img = $("#spic").val();
	if(!$(img).attr("src"))
	{
		img=$(img).find("img").attr("src");
	}
	else
	{
		img=$(img).attr("src");
	}
	$("#spicdiv").css("background-image","url("+img+")");
}

$(document).ready(function(){
	$("#x_online li .top").click(function(){
		$("html,body").animate({scrollTop:0},600)
	});
});
