<?php
namespace App\Logic;

class AdsItemLogic
{
	public $m;

	public function __construct()
	{
		$this->m = M('ads_item');
	}

	public function ls($pid)
	{
		$pid = (int) $pid;
		if(!$pid) json(0, '参数错误');
		$data = $this->m->where("pid = $pid")->order("px ASC")->select();
		return array('st' => 1, 'data' => $data);
	}

	public function get($id)
	{
		$id = (int) $id;
		$row = $this->m->find($id);
		return array('st' => 1, 'data' => $row);
	}

	public function save($post)
	{
		$id = (int) $post['id'];
		if($id) {
			$rs = $this->m->where("id = $id")->save($post);
			if($rs !== false) {
				json(1, $id);
			}
		}else{
			$rs = $this->m->add($post);
			if($rs !== false) {
				json(1, $rs);
			}
		}
		json(0, '保存错误');
	}

	public function del($id)
	{
		$id = (int) $id;
		if(!$id) json(0, '参数错误');
		$rs = $this->m->where("id = $id")->delete();
		if($rs === false) {
				json(0, '删除失败');
		}else{
			json(1);
		}
	}
}