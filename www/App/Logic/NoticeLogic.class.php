<?php
namespace App\Logic;

class NoticeLogic
{
    public $m;
    public function __construct()
    {
        $this->m = M('notice');
    }
    public function ls()
    {
        //列表
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $page = max($page, 1);
        $size = isset($_POST['size']) ? $_POST['size'] : 15;
        $start = $page * $size - $size;

		//自动构造搜索条件
		$where = parseWhere();
        
        $data = $this->m->where($where)->order('id desc')->limit($start, $size)->select();
        $total = $this->m->where($where)->count();
        
        return array('st' => 1, 'data' => $data, 'total' => $total);
    }
    
    public function get($id)
    {
        $id = (int) $id;
        $row = $this->m->find($id);
        return array('st' => 1, 'data' => $row);
    }
    
    public function del($id)
    {
        if(is_numeric($id)) {
            $rs = $this->m->where("id = $id")->delete();
        }else{
            $rs = $this->m->where("id IN($id)")->delete();
        }
        if($rs === false) {
            return array('st' => 0, 'msg' => '删除失败');
        }else{
            return array('st' => 1);
        }
    }
    
    public function save($post)
    {
        $id = (int) $post['id'];
        //允许编辑字段
        if($id) {
            $allow = array('type','title','content','st');
            $row = allow_edit($post, $allow);
            $rs = $this->m->where("id = $id")->save($row);
        }else{
            $post['ts'] = time();
            $rs = $this->m->add($post);
        }
        if($rs !== false) {
            return array('st' => 1);
        }else{
            return array('st' => 0, 'msg' => '保存错误');
        }
    }

    //修改状态
    public function st($id, $st)
    {
        $rs = $this->m->where("id = $id")->setField('st', $st);
        if($rs !== false) {
            return array('st' => 1);
        }else{
            return array('st' => 0, 'msg' => '修改状态错误');
        }
    }
}