<?php
namespace App\Logic;

use App\Logic\BaseLogic;

class MemberLogic extends BaseLogic
{
    public function __construct()
    {
        $this->m = M('member');
    }

    /**
     * 典型获取分页方法
     *
     * 添加了model基类的pagin方法,会自动解析$_POST里的page和size参数
     * px方法解析排序的prop和order参数
     *
     * @return array
     */
    public function ls()
    {
        $where = array();
        if($_POST['real_name']) $where['_string'] = "real_name LIKE '%{$_POST['real_name']}%'";
        if(isset($_POST['role_id']) && $_POST['role_id'] !== array()) {
            $where['role_id'] = array('IN', $_POST['role_id']);
        }
        if(isset($_POST['role']) && $_POST['role'] !== array()) {
            $where['role'] = array('IN', $_POST['role']);
        }
        if(isset($_POST['st']) && $_POST['st'] != -1) $where['st'] = $_POST['st'];

        $data = $this->m->where($where)->px()->pagin()->select();

        //数据过滤器
        $filterConfig = array(
            'real_dep_id' => 'getDep',
            'pid' => 'getRealName',
            //'gender'    =>  array(0 => '女', 1 => '男'),
        );
        $this->filter($data, $filterConfig);

        $total = $this->m->where($where)->count();
        
        return array('st' => 1, 'data' => $data, 'total' => $total);
    }

    public function get($id)
    {
        $id = (int) $id;
        $row = $this->m->find($id);
        return array('st' => 1, 'data' => $row);
    }
    
    //设置员工权限
    public function setPri($post)
    {
        $id = (int) $post['id'];
        $role = $post['role'];
        $rs = $this->m->where("id = $id")->setField('role', $role);
        if($rs !== false) {
            return array('st' => 1);
        }else{
            return array('st' => 0, 'msg' => '设置员工权限错误');
        }
    }

    //获取员工member_id,real_name列表
    public function get_id_realname_ls(){
        //当前登录用户
        $member_id = intval(session('member_id'));
        $dep_id = intval(session('dep_id'));

        $search = "";
        if($_POST['dep_ids']){

            if($_POST['sel'] == 'yj'){
                //查找是否含有下属
                $has_staff = M('member')->where("pid = $member_id")->count();
                //获取医学检验一、二部负责人
                $yj_leader_ids = M('department')->where("id in (91,163)")->col('leader_id');

                if(in_array($member_id, $yj_leader_ids) || $has_staff){
                    $where = "find_in_set($dep_id,path)  or d.id = $dep_id ";
                    //如果是多个部门负责人
                    $dep_ids = M('department')->where("leader_id = {$member_id}")->col('id');

                    if(count($dep_ids) > 1){
                        foreach ($dep_ids as $k => $v) {
                            $where .= " or " ." find_in_set($v,path) or d.id = $v "; 
                        }
                    }
                    //exit($where);
                    //查找部门成员
                    $members = M('member m')->join('department d on m.department_id = d.id','LEFT')->where("$where")->field('m.id')->select();
                    //exit(m()->getlastsql());

                    $staff_ids = implode(',',array_column($members, 'id'));
                    $search = "m.id in ($staff_ids)";
                }else{
                    //只能查看自己
                    $search = "m.id = $member_id";
                }

            }else{
                $dep_ids = $_POST['dep_ids'];
                $search = "find_in_set($dep_ids[0],d.path) or d.id = $dep_ids[0]";
                
                if(count($dep_ids) > 1){
                    foreach ($dep_ids as $k => $v) {
                        if(!$k) continue;
                        $search .= " or " ." find_in_set($v,d.path) or d.id = $v "; 
                    }
                }
            }
        }

        $data = $this->m->alias('m')->join("department d on m.department_id = d.id",'LEFT')->where($search)->field("m.id,m.real_name")->order('m.first_letter ASC')->select();
        return array('st' => 1, 'data' => $data);
    }

    //获取子公司所有员工
    public function get_com_member_ls($com_id = 0){
        $data = $this->m->alias('m')->join("department d on m.department_id = d.id",'LEFT')->where(" find_in_set({$com_id}, d.path) AND m.st = 1")->field("m.id,m.real_name")->select();
        return array('st' => 1, 'data' => $data);
    }
}