<?php
namespace App\Logic;

class BaseLogic
{
    public $m;


    public function filter(&$data, $filterConfig)
    {

        foreach($data as &$d) {
            foreach($filterConfig as $field => $value) {
                if(is_array($value)) {
                    $d[$field] = $value[$d[$field]];
                }else{
                    $d[$field] = call_user_func($value, $d[$field]);
                }
            }
        }
    }
}