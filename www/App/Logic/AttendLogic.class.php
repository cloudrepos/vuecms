<?php
namespace App\Logic;

/**
 * 附件类，都是静态方法，调用方式
 * \App\Logic\AttendLogic::getAttendLs()
 * @package App\Logic
 */
class AttendLogic
{
	/**
	 * 获取附件列表
	 * @param $params array		条件数组
	 * @return array
	 */
    static function ls($params)
    {
        //列表
		$_POST = $params;
		$where = parseWhere();

        $data = M('attend')->where($where)->order('id asc')->select();
		foreach($data as &$d) {
			$d['tag'] = json_decode($d['tag'], true);
			$d['tag'] = implode(' ', $d['tag']);
		}
        return $data;
    }

	/**
	 * 保存附件
	 * @param $module	模块
	 * @param $pk		id
	 * @param $attend	附件数据
	 */
	static function save($module, $pk, $attend)
	{
		$db = M('attend');
		$old_ids = $db->where(array('module' => $module, 'pk' => $pk))->col('id');
		$new_ids = array();
		foreach($attend as $a) {
			$a['up_dt'] = date('Y-m-d');
			$a['tag'] = explode(' ', $a['tag']);
			$a['tag'] = json_encode($a['tag'], 256);
			if($a['id']) {	//保存
				$rs = $db->save($a);
			}else{	//添加
				$a['module'] = $module;
				$a['pk'] = $pk;
				$a['add_dt'] = date('Y-m-d');
				$rs = $a['id'] = $db->add($a);
			}
			if($rs === false) json(0, '保存附件信息错误');
			$new_ids[] = $a['id'];
		}
		$del_ids = array_diff($old_ids, $new_ids);
		if($del_ids) {
			$rs = $db->where(array('id' => array('in', $del_ids)))->delete();
			if($rs === false) json(0, '删除附件信息失败');
		}
	}
}