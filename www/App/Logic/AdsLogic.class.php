<?php
namespace App\Logic;

class AdsLogic
{
	public $m;

	public function __construct()
	{
		$this->m = M('ads');
	}

	public function ls()
	{
		$data = $this->m->order("id DESC")->select();
		foreach ($data as &$d) {
			$d['cover'] = $d['cover'] ? json_decode($d['cover'], true) : array();
		}
		return array('st' => 1, 'data' => $data);
	}

	public function get($id)
	{
		$id = (int) $id;
		$row = $this->m->find($id);
		$row['cover'] = $row['cover'] ? json_decode($row['cover'], true) : array();
		return array('st' => 1, 'data' => $row);
	}

	public function save($post)
	{
		$id = (int) $post['id'];
		$post['cover'] = json_encode($post['cover'], 256);
		if($id) {
			$rs = $this->m->where("id = $id")->save($post);
			if($rs !== false) {
				json(1);
			}
		}else{
			$rs = $this->m->add($post);
			if($rs !== false) {
				json(1, $rs);
			}
		}
		json(0, '保存错误');
	}

	public function del($id)
	{
		$id = (int) $id;
		if(!$id) json(0, '参数错误');
		$rs = $this->m->where("id = $id")->delete();
		if($rs === false) {
				json(0, '删除失败');
		}else{
			json(1);
		}
	}

	//修改状态
	public function st($id, $st)
	{
		$rs = $this->m->where("id = $id")->setField('st', $st);
		if($rs !== false) {
			return array('st' => 1);
		}else{
			return array('st' => 0, 'msg' => '修改状态错误');
		}
	}
}