<?php
namespace App\Logic;

class ResumeLogic
{
    public $m;
    public function __construct()
    {
        $this->m = M('resume');
    }
    public function ls()
    {
        //列表
        $page = isset($_POST['page']) ? $_POST['page'] : 1;
        $page = max($page, 1);
        $size = isset($_POST['size']) ? $_POST['size'] : 15;
        $start = $page * $size - $size;
        
        if($_POST['keyword']) {
            $where[] = "real_name LIKE '%{$_POST['keyword']}%'";
        }
        if($_POST['education']) {
            $where[] = "education = '{$_POST['education']}'";
        }
        $where = $where ? implode(' AND ', $where) : '';
        
        $data = $this->m->where($where)->order('id desc')->limit($start, $size)->select();
        foreach ($data as &$d) {
            $d['ts'] = strtotime($d['ts']); //转为时间戳，在前端以演示时间戳转字符串过滤
        }
        $total = $this->m->where($where)->count();
        
        return array('st' => 1, 'data' => $data, 'total' => $total);
    }
    
    public function get($id)
    {
        $id = (int) $id;
        $row = $this->m->find($id);
        $row['school'] = json_decode($row['school'], true);
        $row['project'] = json_decode($row['project'], true);
        $row['attend'] = json_decode($row['attend'], true);
        if(!$row['attend']) $row['attend'] = array();
        return array('st' => 1, 'data' => $row);
    }
    
    //职位列表
    public function job()
    {
        $data = $this->m->field('job')->group('job')->col('job');
        $data = array_filter($data);
        return array('st' => 1, 'data' => $data);
    }
    
    public function delete($id)
    {
        if(is_numeric($id)) {
            $rs = $this->m->where("id = $id")->delete();
        }else{
            $rs = $this->m->where("id IN($id)")->delete();
        }
        if($rs === false) {
            return array('st' => 0, 'msg' => '删除失败');
        }else{
            return array('st' => 1);
        }
    }
    
    public function submit($post)
    {
        //允许编辑字段
        $id = (int) $post['id'];
        //附件处理
        foreach($post['attend'] as &$a) {
            unset($a['status']);
            unset($a['uid']);
        }
        $post['attend'] = json_encode($post['attend'], 256);
        if($id) {
            $allow = array('real_name','job','education','birthday','email','phone','qqwechat','gender','is_married','school','project', 'attend');
            $row = allow_edit($post, $allow);
            $row['project']= json_encode($row['project'], 256);
            $row['school']= json_encode($row['school'], 256);
            $rs = $this->m->where("id = $id")->save($row);
        }else{
            $post['ts'] = date('Y-m-d H:i:s');
            $rs = $this->m->add($post);
        }
        if($rs !== false) {
            return array('st' => 1);
        }else{
            return array('st' => 0, 'msg' => '保存错误');
        }
    }

	//修改状态
	public function st($id, $st)
	{
		$rs = $this->m->where("id = $id")->setField('st', $st);
		if($rs !== false) {
			return array('st' => 1);
		}else{
			return array('st' => 0, 'msg' => '修改状态错误');
		}
	}
}