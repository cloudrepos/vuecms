<?php
namespace App\Controller;

use App\Logic\MemberLogic;

class MemberController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new MemberLogic();
    }

    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }

    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }

    public function setPri()
    {
        $data = $this->L->setPri($_POST);
        json($data);
    }

    public function get_id_realname_ls(){
        $data = $this->L->get_id_realname_ls();
        json($data);
    }
}