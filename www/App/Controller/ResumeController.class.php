<?php
namespace App\Controller;

use App\Logic\ResumeLogic;

class ResumeController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new ResumeLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }
    
    //单个/批量删除
    public function job()
    {
        $data = $this->L->job();
        json($data);
    }

    //单个/批量删除
    public function delete($id)
    {
        $data = $this->L->delete($id);
        json($data);
    }
    
    public function submit()
    {
        $data = $this->L->submit($_POST);
        json($data);
    }

	public function st($id, $st)
	{
		$data = $this->L->st($id, $st);
		json($data);
	}
}