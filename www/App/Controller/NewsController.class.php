<?php
namespace App\Controller;

use App\Logic\NewsLogic;

class NewsController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new NewsLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }

    public function save()
    {
        $data = $this->L->save($_POST);
        json($data);
    }

	public function del($id)
	{
		$data = $this->L->del($id);
		json($data);
	}

	public function cate()
	{
		$map = M('news_cate')->map('id', 'title');
		json(1, $map);
	}
}