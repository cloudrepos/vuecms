<?php
namespace App\Controller;

use App\Logic\AdminLogic;

class AdminController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new AdminLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }
    
    //单个/批量删除
    public function delete($id)
    {
        $data = $this->L->delete($id);
        json(1);
    }

	public function edit()
	{
		$data = $this->L->edit($_POST);
		json(1);
	}
    
    public function save()
    {
        $data = $this->L->save($_POST);
        json(1);
    }

	public function st($id, $st)
	{
		$data = $this->L->st($id, $st);
		json($data);
	}

	public function del($id)
	{
		$this->L->del($id);
	}
}