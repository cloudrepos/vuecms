<?php
namespace App\Controller;

use App\Logic\AdsItemLogic;

class AdsItemController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new AdsItemLogic();
    }
    
    public function ls($pid)
    {
        $data = $this->L->ls($pid);
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }

    public function save()
    {
        $this->L->save($_POST);
    }

	public function del($id)
	{
		$this->L->del($id);
	}
}