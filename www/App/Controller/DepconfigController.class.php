<?php
namespace App\Controller;

use App\Logic\DepconfigLogic;

class DepconfigController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new DepconfigLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }

    public function save()
    {
        $data = $this->L->save($_POST);
        json($data);
    }

    //删除
    public function delete($id)
    {
        $data = $this->L->delete($id);
        json($data);
    }
}