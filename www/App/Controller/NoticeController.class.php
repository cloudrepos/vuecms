<?php
namespace App\Controller;

use App\Logic\NoticeLogic;

class NoticeController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new NoticeLogic();
    }
    
    public function ls()
    {
        
        $data = $this->L->ls();
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }
    
    public function job()
    {
        $data = $this->L->job($id);
        json($data);
    }
    
    //单个/批量删除
    public function del($id)
    {
        $data = $this->L->del($id);
        json($data);
    }
    
    public function save()
    {
        $data = $this->L->save($_POST);
        json($data);
    }

    public function st($id, $st)
    {
        $data = $this->L->st($id, $st);
        json($data);
    }
}