<?php
namespace App\Controller;

use App\Logic\RoleLogic;

class RoleController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new RoleLogic();
    }
    
    //权限列表
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }

    //权限列表(key=>value值)
    public function map()
    {
        $data = $this->L->map();
        json($data);
    }
    
    //获取所有权限
    public function allpri()
    {
        $menu = include(APP_PATH.'Common/Conf/menu.php');
        json(1, $menu);
    }

    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }
    
    public function submit()
    {
        $data = $this->L->submit($_POST);
        json($data);
    }

    public function st($id, $st)
    {
        $data = $this->L->st($id, $st);
        json($data);
    }

    //排序
    public function px()
    {
        $this->L->px($_POST['ids']);
    }
}