<?php
namespace App\Controller;

use App\Logic\SectionLogic;

class SectionController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new SectionLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }

    public function save()
    {
        $this->L->save($_POST);
    }

	public function del($id)
	{
		$this->L->del($id);
	}

	public function st($id, $st)
	{
		$data = $this->L->st($id, $st);
		json($data);
	}
}