<?php
namespace App\Controller;

use App\Logic\AuthLogic;

class IndexController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->L = new AuthLogic();
    }

    public function index()
    {
		$title = '标题';
		$data = array('a' => 123, 'b' => 456);
		$this->display('index', get_defined_vars());
    }

	public function login()
	{
		if($_GET['debug']) {
			echo '<pre>';
			print_r($_SERVER);
			exit;
		}
		$user = I('post.user');
		$pwd = I('post.pwd');

		if (!$user) {
			return json(0, '请输入账号');
		}
		if (!$pwd) {
			return json(0, '请输入密码');
		}
		//登录
		$id     = $this->L->login($user, $pwd);

		//获取token
		$token = $this->L->createToken($id, $user);

		//获取用户信息，菜单，路由
		$admin = M('admin')->find($id);
		if(!$admin['avatar']) $admin['avatar'] = '/static/image/avatar_man.gif';
		$admin['token'] = $token;
		$data = $this->L->getMenuRouter($admin);
		$admin['menu'] = $data['menu'];
		$admin['router'] = $data['router'];

		json(1, $admin);
	}
}