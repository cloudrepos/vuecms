<?php
namespace App\Controller;

use App\Logic\CateLogic;

class CateController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new CateLogic();
    }
    
    public function ls($module)
    {
        $data = $this->L->ls($module);
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
		json(1, $data);
    }

    public function save()
    {
        $this->L->save($_POST);
    }

	public function del($id)
	{
		$this->L->del($id);
	}

	//分类select数据
	public function map($module)
	{
		$data = $this->L->ls($module);
		$map = array();
		foreach($data['data'] as $d) {
			if($d['lv'] == 2) $d['title'] = '　　' . $d['title'];
			if($d['lv'] == 3) $d['title'] = '　　　　' . $d['title'];
			$d['id'] = $d['id'].'_';	//如果不加个后辍，json格式会按索引数据方式进行按id排序，这样层级关系就错乱了
			$map[$d['id']] = $d['title'];
		}
		json(1, $map);
	}

	//保存自定义字段配置
	public function save_setting($id)
	{
		$id = (int) $id;
		$this->L->save_setting($id, I('post.'));
	}

	//加载分类自定义字段
	public function custom($id)
	{
		$custom = $this->L->custom($id);
		json(1, $custom);
	}
}