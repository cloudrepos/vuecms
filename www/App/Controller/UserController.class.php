<?php
namespace App\Controller;

use App\Logic\UserLogic;

class UserController extends BaseController
{
    public $L;
    public function __construct()
    {
        parent::__construct();
        $this->L = new UserLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json($data);
    }
    
    //单个/批量删除
    public function job()
    {
        $data = $this->L->job();
        json($data);
    }

    //单个/批量删除
    public function delete($id)
    {
        $data = $this->L->delete($id);
        json($data);
    }
    
    public function submit()
    {
        $data = $this->L->submit($_POST);
        json($data);
    }
}