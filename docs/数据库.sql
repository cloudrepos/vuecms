/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : vuecms

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2018-09-14 13:40:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) DEFAULT '' COMMENT '登录账号',
  `name` varchar(255) DEFAULT '' COMMENT '姓名',
  `avatar` varchar(255) DEFAULT '' COMMENT '头像',
  `nickname` varchar(255) DEFAULT '' COMMENT '昵称',
  `email` varchar(255) DEFAULT '' COMMENT '邮箱',
  `mobile` varchar(255) DEFAULT '' COMMENT '手机号',
  `pwd` varchar(255) DEFAULT '' COMMENT '密码',
  `hospital` varchar(255) DEFAULT '' COMMENT '医院',
  `dep` varchar(255) DEFAULT '' COMMENT '科室',
  `role` varchar(255) DEFAULT '' COMMENT '角色',
  `st` tinyint(1) DEFAULT '1' COMMENT '是否可用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='后台用户表';

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'admin', 'admin', 'http://www.i-sanger.cn/uploads/avatar/20161117/582d3be78558e_150_150.jpg', '', '', '', 'f7cfe7684623abf18d523f6fa25d64a8', '', '', 'admin', '1');
INSERT INTO `admin` VALUES ('2', 'test', '测试号', '/static/image/avatar_man.gif', '王总编', '1232@qq.com', '13566554785', 'e10adc3949ba59abbe56e057f20f883e', '人民日报', '社会新闻', 'admin', '1');

-- ----------------------------
-- Table structure for ads
-- ----------------------------
DROP TABLE IF EXISTS `ads`;
CREATE TABLE `ads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `width` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '1=单图模式 2=多图模式',
  `img` varchar(255) DEFAULT '' COMMENT '单图模式下的图片',
  `url` varchar(255) DEFAULT '' COMMENT '单图模式下的链接',
  `memo` varchar(255) DEFAULT NULL,
  `st` tinyint(1) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='广告图';

-- ----------------------------
-- Records of ads
-- ----------------------------
INSERT INTO `ads` VALUES ('1', '顶部横幅', '1920', '700', '1', 'http://mvc_api.qn40.com/upload/files/20180813/8bc4225152eb645a07ac9a5fdf1173fa.jpg', 'http://www.baidu.com', '单图模式之下，因为只有一张图在这里直接就可以上传了', '1');
INSERT INTO `ads` VALUES ('3', '幻灯片', '1920', '600', '2', '', '', '多图模式下，这里只设置广告位信息，具体的图片要到“内容设置“里去处理', '1');

-- ----------------------------
-- Table structure for ads_item
-- ----------------------------
DROP TABLE IF EXISTS `ads_item`;
CREATE TABLE `ads_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '广告位id',
  `px` int(11) DEFAULT '50' COMMENT '排序',
  `title` varchar(255) DEFAULT '',
  `sub_title` varchar(255) DEFAULT '',
  `img` varchar(255) DEFAULT '',
  `url` varchar(255) DEFAULT '' COMMENT '链接',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ads_item
-- ----------------------------
INSERT INTO `ads_item` VALUES ('1', '3', '40', '标题122', '子标题1', 'http://www.chipsea.com/UploadFiles/20180518/162047100.jpg', 'http://www.baidu.com');
INSERT INTO `ads_item` VALUES ('2', '3', '50', '标题2', '子标题2', 'http://api.sangermvc.lc/upload/files/20180816/54e6ed95c364e8ab8972a151a1468cdc.jpg', 'http://www.baidu.com');
INSERT INTO `ads_item` VALUES ('3', '3', '50', '212', '12', '', '12212');

-- ----------------------------
-- Table structure for attend
-- ----------------------------
DROP TABLE IF EXISTS `attend`;
CREATE TABLE `attend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` char(20) DEFAULT '' COMMENT '模块',
  `pk` int(11) DEFAULT '0' COMMENT '主键',
  `cate` char(10) DEFAULT '文档' COMMENT '分类',
  `tags` varchar(255) DEFAULT '' COMMENT '标签',
  `dt` date DEFAULT NULL COMMENT '上传日期',
  `title` varchar(255) DEFAULT '' COMMENT '文档名称',
  `url` varchar(255) DEFAULT '' COMMENT '文档地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件';

-- ----------------------------
-- Records of attend
-- ----------------------------

-- ----------------------------
-- Table structure for cate
-- ----------------------------
DROP TABLE IF EXISTS `cate`;
CREATE TABLE `cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) DEFAULT '0' COMMENT '父id',
  `title` varchar(255) DEFAULT '',
  `lv` tinyint(4) DEFAULT '0' COMMENT '层级',
  `module` char(255) DEFAULT '' COMMENT '模块',
  `custom` text COMMENT '自定义字段配置',
  `st` tinyint(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='多级分类';

-- ----------------------------
-- Records of cate
-- ----------------------------
INSERT INTO `cate` VALUES ('1', '0', '公司新闻', '1', 'news', null, '1');
INSERT INTO `cate` VALUES ('2', '0', '媒体报道', '1', 'news', null, '1');
INSERT INTO `cate` VALUES ('3', '0', '企业文化', '1', 'news', null, '1');
INSERT INTO `cate` VALUES ('7', '0', '国际风向', '1', 'news', null, '1');
INSERT INTO `cate` VALUES ('8', '2', '纸媒', '2', 'news', null, '1');
INSERT INTO `cate` VALUES ('9', '2', '微博报道', '2', 'news', null, '1');
INSERT INTO `cate` VALUES ('10', '8', '人民日报', '3', 'news', null, '1');
INSERT INTO `cate` VALUES ('16', '0', '1212', '1', 'album', null, '1');
INSERT INTO `cate` VALUES ('17', '16', '12323', '2', 'album', null, '1');
INSERT INTO `cate` VALUES ('21', '1', '内部新闻', '2', 'news', null, '1');
INSERT INTO `cate` VALUES ('22', '21', '秘闻录', '3', 'news', null, '1');
INSERT INTO `cate` VALUES ('23', '0', '1233', '1', 'phone', null, '1');
INSERT INTO `cate` VALUES ('24', '0', 'cpu', '1', 'product', '[{\"name\":\"单核睿频\",\"type\":\"input\",\"en\":\"single_core\"},{\"name\":\"全核睿频\",\"type\":\"input\",\"en\":\"full_core\"},{\"name\":\"技术演变\",\"type\":\"textarea\",\"en\":\"yanbian\"},{\"name\":\"类型\",\"type\":\"radio\",\"option\":\"盒装\\n散片\",\"en\":\"type\"},{\"name\":\"指令集\",\"type\":\"checkbox\",\"option\":\"X86\\nEM64T\\nMMX\\nSSE\\nSSE2\\nSSE3\\n3D-NOW\",\"en\":\"zhilin\"},{\"en\":\"test\",\"name\":\"绩效\",\"type\":\"input\"}]', '1');
INSERT INTO `cate` VALUES ('25', '0', '显卡', '1', 'product', '[{\"type\":\"int\",\"name\":\"显存\",\"unit\":\"G\",\"en\":\"xiancun\"},{\"en\":\"bit\",\"name\":\"位宽\",\"type\":\"int\",\"unit\":\"bit\"},{\"name\":\"核心频率\",\"type\":\"input\",\"en\":\"core_hz\"},{\"name\":\"显存频率\",\"type\":\"input\",\"en\":\"xiancun_hz\"},{\"type\":\"float\",\"name\":\"浮点运算次数\",\"unit\":\"TFLOPs\",\"en\":\"fudian\"},{\"type\":\"editor\",\"name\":\"详情\",\"en\":\"editor\"},{\"type\":\"date\",\"en\":\"date\",\"name\":\"发行日期\"},{\"type\":\"datetime\",\"name\":\"开售时间\",\"en\":\"datetime\"},{\"type\":\"image\",\"name\":\"背面图\",\"en\":\"image\"},{\"type\":\"upload\",\"en\":\"file\",\"name\":\"手册\"}]', '1');
INSERT INTO `cate` VALUES ('26', '24', 'Intel', '2', 'product', '', '1');
INSERT INTO `cate` VALUES ('27', '24', 'AMD', '2', 'product', null, '1');
INSERT INTO `cate` VALUES ('28', '25', 'NVIDIA 1050TI', '2', 'product', null, '1');
INSERT INTO `cate` VALUES ('29', '25', 'NVIDIA 1060', '2', 'product', null, '1');
INSERT INTO `cate` VALUES ('30', '29', 'NVIDIA 1060 6G', '3', 'product', null, '1');
INSERT INTO `cate` VALUES ('31', '29', 'NVIDIA 1060 5G', '3', 'product', null, '1');
INSERT INTO `cate` VALUES ('32', '29', 'NVIDIA 1060 3G', '3', 'product', null, '1');
INSERT INTO `cate` VALUES ('35', '27', 'r5', '3', 'product', null, '1');
INSERT INTO `cate` VALUES ('36', '27', 'r7', '3', 'product', null, '1');
INSERT INTO `cate` VALUES ('37', '26', '8代酷睿', '3', 'product', '', '1');
INSERT INTO `cate` VALUES ('38', '26', '7代酷睿', '3', 'product', null, '1');
INSERT INTO `cate` VALUES ('39', '0', '', '0', '', 'null', '1');
INSERT INTO `cate` VALUES ('40', '0', '', '0', '', 'null', '1');
INSERT INTO `cate` VALUES ('41', '0', '', '0', '', 'null', '1');
INSERT INTO `cate` VALUES ('42', '0', '', '0', '', 'null', '1');
INSERT INTO `cate` VALUES ('43', '0', '', '0', '', 'null', '1');
INSERT INTO `cate` VALUES ('44', '0', '', '0', '', 'null', '1');
INSERT INTO `cate` VALUES ('45', '0', '1212', '1', 'photo', 'null', '1');
INSERT INTO `cate` VALUES ('46', '45', '2323', '2', 'photo', 'null', '1');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) DEFAULT '0' COMMENT '日期',
  `title` varchar(255) DEFAULT '',
  `add_dt` date DEFAULT NULL,
  `source` varchar(255) DEFAULT '' COMMENT '来源',
  `cover` varchar(255) DEFAULT '' COMMENT '封面',
  `content` text COMMENT '内容',
  `admin_id` int(11) DEFAULT '0' COMMENT '添加人',
  `admin_name` char(10) DEFAULT '' COMMENT '添加人',
  PRIMARY KEY (`id`),
  KEY `name` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=141 DEFAULT CHARSET=utf8 COMMENT='新闻';

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('1', '9', '亮相深圳健博会，引发强烈关注！亮相深圳健博会，引发强烈关注！亮相深圳健博会，引发强烈关注！', '2018-04-06', '科技', 'http://www.chipsea.com/thumb.ashx?image=/UploadFiles/20180509/095653256.jpg&width=245&height=150', '<p><strong style=\"color: rgb(230, 0, 0);\">亮相深圳健博会，引发强烈关注！亮相深圳健博会，引发强烈关注！亮相深圳健博会，引发强烈关注！</strong></p>', '2', '王医生1');
INSERT INTO `news` VALUES ('2', '10', '测试', '2018-07-24', '测试', 'http://www.chipsea.com/thumb.ashx?image=/UploadFiles/20180509/095653256.jpg&width=245&height=150', '<pre class=\"ql-syntax\" spellcheck=\"false\">&lt;?php\nphpinfo();\n\n</pre>', '2', '王医生1');
INSERT INTO `news` VALUES ('3', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('6', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('7', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('8', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('9', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('10', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('11', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('12', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('13', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('14', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('15', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('16', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('17', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('18', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('19', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('20', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('21', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('22', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('23', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('24', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('25', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('26', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('27', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('28', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('29', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('30', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('31', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('32', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('33', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('34', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('35', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('36', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('37', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('38', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('39', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('40', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('41', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('42', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('43', '21', '添加资讯', '2018-07-29', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('44', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('45', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('46', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('47', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('48', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('49', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('50', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('51', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('52', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('53', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('54', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('55', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('56', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('57', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('58', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('59', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('60', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('61', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('62', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('63', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('64', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('65', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('66', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('67', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('68', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('69', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('70', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('71', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('72', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('73', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('74', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('75', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('76', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('77', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('78', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('79', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('80', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('81', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('82', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('83', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('84', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('85', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('86', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('87', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('88', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('89', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('90', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('91', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('92', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('93', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('94', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('95', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('96', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('97', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('98', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('99', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('100', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('101', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('102', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('103', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('104', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('105', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('106', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('107', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('108', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('109', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('110', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('111', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('112', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('113', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('114', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('115', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('116', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('117', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('118', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('119', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('120', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('121', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('122', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('123', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('124', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('125', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('126', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('127', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('128', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('129', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('130', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('131', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('132', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('133', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('134', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('135', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('136', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('137', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('138', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('139', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');
INSERT INTO `news` VALUES ('140', '21', '添加资讯', '2018-07-30', '百度', '', '<p>这边是一堆内容</p>', '0', '');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) DEFAULT '1' COMMENT '1=公司 2=部门',
  `title` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `ts` int(11) DEFAULT '0' COMMENT '时间戳',
  `st` tinyint(1) DEFAULT '1' COMMENT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of notice
-- ----------------------------
INSERT INTO `notice` VALUES ('2', '1', '添加', '12323', '1517296646', '0');
INSERT INTO `notice` VALUES ('3', '1', '弹窗添加公告', '弹窗添加公告\n弹窗添加公告\n弹窗添加公告\n弹窗添加公告\n弹窗添加公告', '1517297820', '1');
INSERT INTO `notice` VALUES ('10', '1', '3334', '222', '1517299542', '1');
INSERT INTO `notice` VALUES ('11', '1', '12323', '1222', '1517299553', '0');
INSERT INTO `notice` VALUES ('12', '1', '11111111112', '222', '1517299559', '1');
INSERT INTO `notice` VALUES ('22', '1', '12212', '<p>21232</p>', '1533264230', '1');
INSERT INTO `notice` VALUES ('23', '1', '1', '<p>2</p>', '1533264265', '1');
INSERT INTO `notice` VALUES ('24', '1', '212', '<p>222</p>', '1534403961', '1');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cate_id` int(11) DEFAULT '0' COMMENT '分类',
  `title` varchar(255) DEFAULT '' COMMENT '标题',
  `cover` varchar(255) DEFAULT '' COMMENT '封面图片',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '价格',
  `summary` text COMMENT '简介',
  `main` text COMMENT '主要参数',
  `content` text COMMENT '内容',
  `custom` text COMMENT '自定义字段值',
  `custom2` text COMMENT '自定义属性分段',
  `text` text COMMENT '自定义文本',
  `spec` text COMMENT '自定义字段',
  `pic` varchar(255) DEFAULT '' COMMENT '功能框图',
  `add_dt` date DEFAULT NULL COMMENT '添加日期',
  `admin_id` int(11) DEFAULT '0' COMMENT '添加人',
  `admin_name` char(10) DEFAULT '' COMMENT '添加人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='产品';

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '30', '影驰神将1060 6GOC版', 'http://api.sangermvc.lc/upload/files/20180816/108da28839113b5a91884bf63fe60596.jpg', '1799.03', '简介', '芯片厂商：NVIDIA\r\n显卡芯片：GeForce GTX 1060\r\n显示芯片系列：NVIDIA GTX 10系列\r\n制作工艺：16纳米\r\n核心代号：GP106-400\r\n核心频率：1556/1771MHz\r\nCUDA核心：1280个纠错', '<p>其它参数</p><p>显卡类型 主流级</p><p>散热方式 三风扇散热+热管散热</p><p>3D API DirectX 12，OpenGL 4.5</p><p>供电模式 5+1相</p><p>支持HDCP 是</p><p>最大功耗 120W</p><p>建议电源 400W以上</p><p>其它特点 支持GameWorks技术，3D Vision技术技术</p><p>上市日期 2016年07月</p><p>性能评分 87022纠错</p>', '{\"date\":\"2018-08-16\",\"datetime\":\"2018-08-31 10:12:12\",\"xiancun\":6,\"bit\":192,\"core_hz\":\"1500-1779MHZ\",\"xiancun_hz\":\"8000MHZ\",\"fudian\":\"2.2\",\"image\":[{\"name\":\"u=3224360923,3909419427&fm=27&gp=0.jpg\",\"url\":\"http:\\/\\/api.sangermvc.lc\\/upload\\/files\\/20180816\\/6256e6fac2138651f3878b3db2dc7c81.jpg\",\"status\":\"finished\",\"percentage\":100,\"uid\":1534413833815}]}', '[{\"title\":\"外形规格\",\"data\":[{\"key\":\"颜色\",\"value\":\"黑色\"},{\"key\":\"风扇\",\"value\":\"双风扇\"}]}]', '[{\"title\":\"外设特性\",\"content\":\"14位双向I\\/O口和1个输入口\\n1路蜂鸣器输出\\n4×14的LCD驱动\\n可选择内部晶振或WDT晶振作为时钟源\\n可选择两种不同的LCD驱动波形\\n可选择不同的偏置电压产生方式\\n低电压检测（LVD）引脚（内部提供2.4V、2.5V、2.6V、2.7V、2.8V3、.2V 、3.6V电压比较）\\n2个外部中断\"},{\"title\":\"模拟特性\",\"content\":\"24位分辨率,有效精度15位（PGA为68时，输出速率为7.8KHz）\\n内部集成的可编程增益放大器\\nADC的输出速率30.5Hz~62.5KHz\\n内带电荷泵\\n内带稳压器供传感器和调制器\"}]', '[{\"key\":\"颜色\",\"value\":\"黑色\"},{\"key\":\"风扇\",\"value\":\"双风扇\"}]', '', '2018-06-29', '2', '测试号');
INSERT INTO `product` VALUES ('2', '37', 'i7 8700k', 'http://api.sangermvc.lc/upload/files/20180816/fcf4fdc693172096a52c7558393d1703.jpg', '2299.00', '简介', '芯片厂商：NVIDIA\r\n显卡芯片：GeForce GTX 1060\r\n显示芯片系列：NVIDIA GTX 10系列\r\n制作工艺：16纳米\r\n核心代号：GP106-400\r\n核心频率：1556/1771MHz\r\nCUDA核心：1280个纠错', '<p>222222</p>', '{\"single_core\":\"4.7G\",\"full_core\":\"3.9G\",\"yanbian\":\"这儿是一大段话\",\"type\":\"散片\",\"zhilin\":[\"X86\",\"SSE2\",\"MMX\"],\"test\":\"1212\"}', '[{\"title\":\"技术参数\",\"data\":[{\"key\":\"基频\",\"value\":\"2.8G\"},{\"key\":\"单核睿频\",\"value\":\"4.7G\"},{\"key\":\"全核睿频\",\"value\":\"3.9G\"}]},{\"title\":\"其它\",\"data\":[{\"key\":\"核心面积\",\"value\":\"56mm*56mm\"},{\"key\":\"散热材料\",\"value\":\"硅脂\"}]}]', 'null', 'null', '', '2018-07-11', '2', '测试号');
INSERT INTO `product` VALUES ('3', '37', 'i5 8400', '', '0.00', '2', '3', '', '{\"single_core\":\"4.1G\",\"full_core\":\"3.7G\",\"yanbian\":\"多行文本框\",\"type\":\"散片\",\"zhilin\":[\"EM64T\",\"SSE2\",\"X86\"]}', null, 'null', 'null', '', '2018-08-02', '2', '测试号');
INSERT INTO `product` VALUES ('5', '28', '七彩虹 1050TI', '', '1069.00', null, null, '', '{\"single_core\":\"12\",\"full_core\":\"23\",\"yanbian\":\"34222\",\"type\":\"散片\",\"zhilin\":[\"EM64T\",\"MMX\"],\"date\":\"\",\"datetime\":\"\",\"xiancun\":4,\"bit\":128}', null, 'null', 'null', '', '2018-08-07', '2', '测试号');

-- ----------------------------
-- Table structure for resume
-- ----------------------------
DROP TABLE IF EXISTS `resume`;
CREATE TABLE `resume` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '和member表id一致',
  `sessionid` char(32) DEFAULT NULL,
  `job` char(20) DEFAULT NULL COMMENT '应聘职位',
  `real_name` varchar(255) NOT NULL COMMENT '姓名',
  `gender` tinyint(1) DEFAULT '1' COMMENT '0=女',
  `birthday` date DEFAULT NULL,
  `email` varchar(255) DEFAULT '' COMMENT '个人电子邮箱',
  `phone` char(30) DEFAULT '' COMMENT '联系电话',
  `qqwechat` char(30) DEFAULT '' COMMENT 'QQ微信',
  `is_married` tinyint(4) DEFAULT '0' COMMENT '是否已婚',
  `nationality` char(10) DEFAULT '' COMMENT '国籍',
  `education` char(10) DEFAULT '' COMMENT '最高学历',
  `residence` varchar(255) DEFAULT '' COMMENT '居住地址',
  `politics` char(2) DEFAULT '' COMMENT '政治面貌',
  `ts` datetime DEFAULT NULL COMMENT '添加时间',
  `last_school` varchar(255) DEFAULT '' COMMENT '最后学校',
  `last_biye` char(10) DEFAULT '' COMMENT '最后毕业',
  `major` char(20) DEFAULT '' COMMENT '专业',
  `school` text COMMENT '教育经历json',
  `project` text COMMENT '实习经历',
  `xingqu` varchar(255) DEFAULT '' COMMENT '兴趣',
  `memo` text COMMENT '自我介绍',
  `attend` text COMMENT '简历',
  `st` tinyint(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COMMENT='自助简历';

-- ----------------------------
-- Records of resume
-- ----------------------------
INSERT INTO `resume` VALUES ('35', 'dr3l9ne2al1ko0ad6cne3ogmm6', '医学研究员', '陈锦文', '1', '1991-09-28', '15036695525@163.com', '15578984613', '1683857813', '0', '中国', '硕士', '广西南宁', '党员', '2017-10-22 20:49:36', '广西大学', '2018-06-20', '植物种质资源学', '[{\"start\":\"2015-09-10\",\"end\":\"2018-06-20\",\"school\":\"广西大学\",\"education\":\"硕士\",\"major\":\"植物种质资源学\",\"fangx\":\"芒果开花基因克隆转基因功能验证\"},{\"school\":\"2323\",\"education\":\"232\",\"end\":\"2018-08-22T16:00:00.000Z\",\"major\":\"123\",\"start\":\"2018-08-27T16:00:00.000Z\"}]', '[{\"project\":\"mvc\",\"start\":\"2018-07-31T16:00:00.000Z\",\"end\":\"2018-08-28T16:00:00.000Z\"},{\"project\":\"cost\",\"start\":\"2018-07-30T16:00:00.000Z\",\"end\":\"2018-08-30T16:00:00.000Z\"}]', '[\"\\u4e0b\\u68cb\"]', null, '[{\"name\":\"0df431adcbef7609d425655222dda3cc7dd99ed5.jpg\",\"url\":\"http:\\/\\/api.sangermvc.lc\\/upload\\/files\\/20180816\\/108da28839113b5a91884bf63fe60596.jpg\",\"percentage\":100}]', '1');
INSERT INTO `resume` VALUES ('10', 'rdvg0ae27cn1koofhit2gej1k4', '产品工程师', '田建文', '1', '1987-12-12', '12@125.com', '13816466135', '', '1', '中国', '硕士', '巴拉巴拉巴拉…', '党员', '2017-09-12 11:26:27', '', null, '', '[{\"start\":\"\",\"end\":\"\",\"school\":\"\",\"education\":\"\",\"major\":\"\"}]', '[{\"start\":\"2015-09-12\",\"end\":\"2017-09-12\",\"project\":\"交集加我\"}]', '[\"\\u770b\\u4e66\"]', '', null, '1');
INSERT INTO `resume` VALUES ('11', '9vh5ts5d1fhea3kfno38cjpeo5', '医学研究员', '胡倩', '0', '2017-09-12', '123@majorbio.com', '12345678', '', '0', '中国', '博士后', '上海周浦', '群众', '2017-09-12 11:27:28', '', null, '', '[{\"start\":\"\",\"end\":\"\",\"school\":\"\",\"education\":\"\",\"major\":\"\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('12', 'sdlrg5uqg8pdcabvu4p9opha57', '生物信息分析工程师', '李世林', '1', '1993-04-01', '1574494@qq.com', '15902138324', '', '0', '中国', '本科', '上海', '团员', '2017-09-12 11:30:41', '南通', '2015-09-11', '人力资源', '[{\"start\":\"2011-09-11\",\"end\":\"2015-09-11\",\"school\":\"南通\",\"education\":\"本科\",\"major\":\"人力资源\"}]', '[{\"start\":\"2011-09-12\",\"end\":\"2015-09-12\",\"project\":\"\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('14', '2hr12llodao12ddmuos114fo02', '遗传分析工程师', '秦海怡', '0', '1981-07-29', 'haiyi.qin@majorbio.com', '13811406220', '', '1', '中国', '硕士', '北京市海淀区安宁庄东路', '群众', '2017-09-12 11:44:34', '', null, '', '[{\"start\":\"\",\"end\":\"\",\"school\":\"\",\"education\":\"\",\"major\":\"\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('26', 'jp7nr7knip50m10afuu70qqa76', '医学研究员', '房炜森', '1', '1992-11-03', 'Fcholes@163.com', '17864276133', '2271668008', '0', '中国', '硕士', '山东青岛中国海洋大学鱼山路5号', '团员', '2017-10-01 13:58:35', '中国海洋大学', '2017-12-01', '生物工程', '[{\"start\":\"2015-08-1\",\"end\":\"2017-12-1\",\"school\":\"中国海洋大学\",\"education\":\"硕士研究生\",\"major\":\"生物工程(海洋生物医用材料)\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('17', '1k70tkk8v07ihqeuren7bo4t67', '医学研究员', '左建伟', '1', '1992-08-14', 'zuojianwei1992@163.com', '17864279179', '1547935989/ZJWGranite', '0', '中国', '硕士', '山东省青岛市', '团员', '2017-09-14 19:30:12', '中国海洋大学', '2018-06-31', '细胞生物学', '[{\"start\":\"2015-09-1\",\"end\":\"2018-6-31\",\"school\":\"中国海洋大学\",\"education\":\"理学硕士\",\"major\":\"细胞生物学\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('18', 'htp326nc1fgg7mh3c17kld4er6', '生物信息分析工程师', '刘娜', '0', '1992-10-06', '1781908198@qq.com', '17864272885', '1781908198', '0', '中国', '硕士', '青岛市市南区鱼山路5号', '团员', '2017-09-14 20:00:44', '中国海洋大学', '2017-12-31', '细胞生物学', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"细胞生物学\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '[]', '1');
INSERT INTO `resume` VALUES ('19', 'nrf0f1gj4sn5s2ar0mmshrqad6', '产品工程师', '丁园园', '0', '1993-04-19', '772638453@qq.com', '17853253126', '772638453', '0', '中国', '硕士', '山东省青岛市市南区鱼山路5号', '团员', '2017-09-14 20:09:32', '中国海洋大学', '2017-12-14', '生物工程', '[{\"start\":\"2016-09-14\",\"end\":\"2017-12-14\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"生物工程\"}]', '[{\"start\":\"2014-09-14\",\"end\":\"2017-01-14\",\"project\":\"大叶黄杨中黄酮类化合物的提取与纯化研究;马尾松中槲皮素提取工艺条件研究;普鲁兰多糖的提取纯化以及不同分子量普鲁兰多糖膜性质研究，以及其在胶囊制剂中的应用\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('20', 'jrklkcgikcbov2566t8t4dte11', '产品工程师', '张雅慧', '0', '1993-02-06', '564927609@qq.com', '15689932652', '564927609', '0', '中国', '硕士', '山东省青岛市市南区鱼山路5号', '党员', '2017-09-14 23:07:23', '河北科技大学', '2015-07-1', '药学', '[{\"start\":\"2011-09-1\",\"end\":\"2015-07-1\",\"school\":\"河北科技大学\",\"education\":\"本科\",\"major\":\"药学\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('21', '5kl0qrrm1dug26r9pdnubub464', '遗传分析师', '丁雅婷', '0', '1992-11-29', 'yatingding@qq.com', '17864272859', '741845874', '0', '中国', '硕士', '山东省青岛市市南区', '团员', '2017-09-15 09:51:48', '中国海洋大学', '2017-12-31', '遗传学', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"遗传学\"}]', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"project\":\"雨生红球藻大规模培养及优良藻种的筛选、雨生红球藻分子生物学相关研究\"}]', '[\"\\u6e38\\u620f\"]', '', null, '1');
INSERT INTO `resume` VALUES ('22', '0l240a215mn8kdino30f7pqn00', '生物信息分析工程师', '温奇秦', '1', '1992-10-05', 'wenqiqin321@163.com', '17853253130', 'w18360538143', '0', '中国', '硕士', '青岛市', '团员', '2017-09-15 15:35:12', '中国海洋大学', '2017-12-30', '生物工程', '[{\"start\":\"2016-09-1\",\"end\":\"2017-12-30\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"生物工程\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('27', 'mmrf04uaag0l7quu4cs88m09r1', '遗传分析师', '杨悦', '0', '1992-11-01', 'xllyang1101@163.com', '15046113209', '1075739834', '0', '中国', '硕士', '哈尔滨', '团员', '2017-10-05 18:44:00', '', null, '', '[]', '[{\"start\":\"2015-09-1\",\"end\":\"2017-09-12\",\"project\":\"上海辰山植物园联合培养\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('28', 'fi5eecc8es75eupf0ud8aop9b7', '遗传分析师', '丁婷婷', '0', '1993-01-25', 'dudude1108@163.com', '18845877580', '', '0', '中国', '硕士', '黑龙江省哈尔滨市', '团员', '2017-10-07 12:29:59', '哈尔滨医科大学', '2017-12-31', '基础医学', '[{\"start\":\"2011-09-1\",\"end\":\"2017-12-31\",\"school\":\"哈尔滨医科大学\",\"education\":\"硕士\",\"major\":\"基础医学七年制\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', '', null, '1');
INSERT INTO `resume` VALUES ('29', '08cqbd3dpvqkalc78obgdq5b73', '生物信息分析工程师', '赵越', '0', '1992-12-21', 'zylovedata@163.com', '18846047451', '18846047451', '0', '中国', '硕士', '黑龙江 哈尔滨', '团员', '2017-10-07 17:10:32', '哈尔滨医科大学', '2017-07-01', '基础医学', '[{\"start\":\"2011-09-01\",\"end\":\"2017-07-01\",\"school\":\"哈尔滨医科大学\",\"education\":\"硕士\",\"major\":\"基础医学七年制（生物信息））\"}]', '[{\"start\":\"2015-09-01\",\"end\":\"2016-11-01\",\"project\":\"生物医学数据库平台搭建\"},{\"start\":\"\",\"end\":\"2017-10-07\",\"project\":\"基于「全基因组重测序数据」，基于全基因组测序识别同卵异病孤独症患儿的基因组变异\"}]', '[]', '', null, '1');
INSERT INTO `resume` VALUES ('30', 'j28ffi4ph5lj4o5d0lhm2d1dh2', '产品工程师', '何芳', '0', '1993-02-17', '18817502036@163.com', '18817502036', '2251556791', '0', '中国', '硕士', '上海市徐汇区梅陇路130号', '党员', '2017-10-11 14:43:32', '华东理工大学', '2017-12-31', '生物化工', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"school\":\"华东理工大学\",\"education\":\"硕士\",\"major\":\"生物化工\"}]', '[{\"start\":\"2016-09-1\",\"end\":\"2017-08-1\",\"project\":\"外泌体的分离与检测\"}]', '[\"\\u5531\\u6b4c\",\"\\u4e0b\\u68cb\",\"\\u6e38\\u620f\"]', '', null, '1');
INSERT INTO `resume` VALUES ('31', '7gkf3vk9r59q8jucmjckddtr47', '销售工程师', '王楠', '1', '1993-06-02', 'wnan1993@163.com', '13122313379', 'wangnan6283196', '0', '中国', '硕士', '上海市徐汇区', '团员', '2017-10-11 22:05:42', '华东理工大学', '2018-07-1', '生物工程', '[{\"start\":\"2015-09-1\",\"end\":\"2018-07-1\",\"school\":\"华东理工大学\",\"education\":\"硕士\",\"major\":\"生物工程\"}]', '[{\"start\":\"2017-05-1\",\"end\":\"2017-08-10\",\"project\":\"兰州申联生物制药有限公司口蹄疫疫苗悬浮培养生产线的建立\"}]', '[]', null, null, '1');
INSERT INTO `resume` VALUES ('36', '4hqnb2e08os7fpc9jrusuilaj7', '医学研究员', '李兴娟', '0', '1992-11-24', '17761700937@163.com', '17761700937', '', '0', '中国', '硕士', '江苏省南京市玄武区卫岗1号', '团员', '2017-10-21 20:18:31', '南京农业大学', '2018-06-23', '微生物学', '[{\"start\":\"2015-09-5\",\"end\":\"2018-06-23\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"微生物学\",\"fangx\":\"微生物与宿主间的相互作用\"},{\"start\":\"2011-09-5\",\"end\":\"2015-06-22\",\"school\":\"廊坊师范学院\",\"education\":\"本科\",\"major\":\"生物技术\",\"fangx\":\"\"}]', '[{\"start\":\"2015-03-5\",\"end\":\"2015-05-25\",\"project\":\"实习\\/本科毕业设计   平菇细菌性褐斑病病原菌分离鉴定\"},{\"start\":\"2017-06-15\",\"end\":\"2017-08-29\",\"project\":\"田菁根瘤菌ROS相关基因ahpC和bcp在抗氧化及共生固氮中的功能研究\"},{\"start\":\"2015-11-6\",\"end\":\"2017-10-21\",\"project\":\"菜豆根瘤菌中影响共生岛转移的基因的研究\"}]', '[]', '', null, '1');
INSERT INTO `resume` VALUES ('37', 'v395voe8tkimjl7edmupp3cav5', '销售工程师', '许思佳', '0', '1992-06-13', '1103660010@qq.com', '18846170419', '', '0', '中国', '硕士', '哈尔滨市香坊区和兴路26号', '党员', '2017-10-22 21:25:57', '东北林业大学', '2018.07.01', '林木遗传育种', '[{\"start\":\"2015.09.01\",\"end\":\"2018.07.01\",\"school\":\"东北林业大学\",\"education\":\"硕士\",\"major\":\"林木遗传育种\",\"fangx\":\"分子生物学\"}]', '[]', '[]', '', null, '1');
INSERT INTO `resume` VALUES ('38', 'j8t2p7naort22v7qh7jccqbae1', '产品工程师', '林梅', '0', '1990-11-20', 'linmei1120@163.com', '18766534510', '', '0', '中国', '硕士', '山东省烟台市莱山区春晖路17号', '团员', '2017-10-23 15:07:33', '浙江海洋大学&amp;中国科学院烟台海岸带研究所', '2018-06-21', '设施农业', '[{\"start\":\"2016-09-11\",\"end\":\"2018-06-21\",\"school\":\"浙江海洋大学&amp;中国科学院烟台海岸带研究所\",\"education\":\"硕士\",\"major\":\"设施农业\",\"fangx\":\"海藻肥的功效验证\"}]', '[]', '[\"\\u6e38\\u620f\"]', null, null, '1');
INSERT INTO `resume` VALUES ('39', 'gf0ih7f29hcoeqgdojn544ttb4', '产品工程师', '孟振祥', '1', '1991-08-01', 'mengzhenxiang@outlook.com', '15062276200', '', '0', '中国', '硕士', '南京市南京农业大学', '团员', '2017-10-23 15:22:30', '南京农业大学', '2018-06-5', '动物营养与饲料学', '[{\"start\":\"2015-09-10\",\"end\":\"2018-06-5\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料学\",\"fangx\":\"瘤胃微生物\"}]', '[{\"start\":\"2015-09-23\",\"end\":\"2018-06-6\",\"project\":\"湖羊产学研项目\"}]', '[\"\\u5531\\u6b4c\",\"\\u6e38\\u620f\"]', null, null, '1');
INSERT INTO `resume` VALUES ('40', 'lm0iqi0gfjmpvher6b93vnaen7', '产品工程师', '张晓东', '1', '1991-12-27', '409856956@qq.com', '15850783964', '', '0', '中国', '硕士', '江苏省南京市南京农业大学', '党员', '2017-10-23 15:22:48', '南京农业大学', '2019-07-1', '动物科学', '[{\"start\":\"2016-09-1\",\"end\":\"2019-07-1\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物科学\",\"fangx\":\"消化道微生物\"}]', '[]', '[\"\\u5531\\u6b4c\"]', null, null, '1');
INSERT INTO `resume` VALUES ('41', 'svk2bsimajj9fei1grg866e741', '产品工程师', '陈慧子', '0', '1994-06-22', 'huizichen1994@outlook.com', '18260069910', '2455837387', '0', '中国', '硕士', '江苏省南京市玄武区童卫路卫岗一号', '团员', '2017-10-23 15:25:11', '南京农业大学', '2018-06-5', '动物营养与饲料科学', '[{\"start\":\"2015-09-5\",\"end\":\"2018-06-5\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"动物消化道微生物\"}]', '[{\"start\":\"2015-11-10\",\"end\":\"2018-06-5\",\"project\":\"国家973项目 盲肠灌注丙酸钠对生长猪机体代谢及食欲的影响\"}]', '[\"\\u4e0b\\u68cb\",\"\\u5531\\u6b4c\"]', '', null, '1');
INSERT INTO `resume` VALUES ('42', 'n2ithmu11vefehfnhj1va3i371', '产品工程师', '田时祎', '0', '1993-06-24', '731329348@qq.com', '15651989130', '731329348', '0', '中国', '硕士', '江苏省南京市玄武区卫岗1号', '团员', '2017-10-23 15:26:24', '南京农业大学', '2018-06-5', '动物营养与饲料科学', '[{\"start\":\"2015-09-3\",\"end\":\"2018-06-5\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"消化道微生物\"},{\"start\":\"2011-09-4\",\"end\":\"2015-06-6\",\"school\":\"南京农业大学\",\"education\":\"本科\",\"major\":\"动物科学\",\"fangx\":\"\"}]', '[{\"start\":\"2016-09-31\",\"end\":\"2017-09-24\",\"project\":\"低聚半乳糖对哺乳仔猪小肠肠道形态，功能发育，菌群结构的影响\"}]', '[\"\\u6e38\\u620f\"]', '', null, '1');
INSERT INTO `resume` VALUES ('43', 'dlmhsjlv0m2tvbvvso8u1lg813', '销售工程师', '吕雪晴', '0', '1993-01-15', '945302973@qq.com', '15895977157', '945302973', '0', '中国', '硕士', '江苏省南京市玄武区卫岗1号', '团员', '2017-10-23 15:40:00', '南京农业大学', '2019-08-5', '作物遗传育种', '[{\"start\":\"2015-09-4\",\"end\":\"2019-08-5\",\"school\":\"南京农业大学\",\"education\":\"硕士研究生\",\"major\":\"作物遗传育种\",\"fangx\":\"非生物逆境胁迫对水稻影响\"}]', '[{\"start\":\"2015-10-1\",\"end\":\"2018-09-5\",\"project\":\"运用real time pcr技术，western blot技术，DNA提取技术，RNA提取技术，GUS染色技术等。研究非生物逆境胁迫，如干旱、低温、高温、激素等，水稻ZFP150对其的抗性研究。研究ZFP150基因对水稻的影响。\"}]', '[]', '', null, '1');
INSERT INTO `resume` VALUES ('44', 'jfan7t1bg4mv66ra97lpqh39i3', '产品工程师', '张青', '0', '1994-12-08', '390540045@qq.com', '15651958780', '390540045', '0', '中国', '硕士', '南京农业大学', '党员', '2017-10-23 15:43:16', '南京农业大学', '2018-07-1', '动物营养与饲料科学', '[{\"start\":\"2015-09-1\",\"end\":\"2018-07-1\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"动物消化道微生物\"},{\"start\":\"2011-09-1\",\"end\":\"2015-07-1\",\"school\":\"江西农业大学\",\"education\":\"本科\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"\"}]', '[{\"start\":\"2018-02-06T16:00:00.000Z\",\"end\":\"2018-02-15T16:00:00.000Z\",\"project\":\"研究霉菌毒素对仔猪的影响及治疗改善方法\"},{\"project\":\"123\",\"start\":\"2018-01-30T16:00:00.000Z\",\"end\":\"2018-02-14T16:00:00.000Z\"}]', '[\"\\u4e0b\\u68cb\",\"\\u6e38\\u620f\"]', '', '[{\"name\":\"2017年应出勤日配置.txt\",\"url\":\"http:\\/\\/api.lc\\/upload\\/files\\/20180211\\/2e5f025541acb693fa0a4b3ec0edd10b.txt\"},{\"name\":\"9389373_48.jpg\",\"url\":\"http:\\/\\/api.lc\\/upload\\/files\\/20180212\\/8bc4225152eb645a07ac9a5fdf1173fa.jpg\"},{\"name\":\"无创胎儿亲子鉴定专题【配套】.rar\",\"url\":\"上传根目录不存在！请尝试手动创建:.\\/upload\"}]', '1');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户组ID',
  `name` varchar(32) NOT NULL COMMENT '用户组名称',
  `role` char(20) DEFAULT '' COMMENT '英文标识',
  `pri` text COMMENT '组权限',
  `px` tinyint(4) DEFAULT '1' COMMENT '排序(小->大)',
  `st` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='权限组';

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', '超管', 'admin', '', '1', '1');
INSERT INTO `role` VALUES ('2', '编辑', 'editor', '[\"patient\",\"patient_view\",\"cup_search\",\"center\",\"tumor_form\",\"user_form\",\"delete_user\",\"lv32\",\"news_cate\"]', '1', '1');

-- ----------------------------
-- Table structure for section
-- ----------------------------
DROP TABLE IF EXISTS `section`;
CREATE TABLE `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT '',
  `mode` char(10) DEFAULT 'textarea' COMMENT 'textarea/editor',
  `content` text,
  `st` tinyint(1) DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='片段';

-- ----------------------------
-- Records of section
-- ----------------------------
INSERT INTO `section` VALUES ('1', '关于公司', 'editor', '<h3>这是标题</h3>\r\n<p style=\"color:#d00\">用编辑器可以写一些较复杂的片段，但是由于前端和后端的样式环境不一样，可能样式不容易控制</p>', '1');
INSERT INTO `section` VALUES ('3', '400电话', 'editor', '<h1>111</h1>', '1');
INSERT INTO `section` VALUES ('6', '状态', 'textarea', '状态不在弹窗里设置，在列表页直接点击就可以修改了，更简洁一些', '0');

-- ----------------------------
-- Table structure for token
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(32) DEFAULT '',
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `exp_ts` int(11) NOT NULL DEFAULT '0' COMMENT '过期时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`,`admin_id`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=287 DEFAULT CHARSET=utf8 COMMENT='APP接口token';

-- ----------------------------
-- Records of token
-- ----------------------------
INSERT INTO `token` VALUES ('1', 'test_token', '2', '1537291087');
INSERT INTO `token` VALUES ('269', '37825d419f8bcd54665e027728b230c0', '2', '1531992590');
INSERT INTO `token` VALUES ('270', '5b7817a6ec83c488c6c6439c57bc2135', '2', '1532077915');
INSERT INTO `token` VALUES ('271', 'ecdd11dae22b075d55888e270dc45dfb', '1', '1532056242');
INSERT INTO `token` VALUES ('272', 'a6de271ef5e0032a0a9b831ccc7705de', '2', '1532172546');
INSERT INTO `token` VALUES ('273', 'b16800981522f1e3404feb2aa5d1fa88', '2', '1532320795');
INSERT INTO `token` VALUES ('274', '8edc21f4eb87932900fc74bb4465dd1a', '2', '1532422901');
INSERT INTO `token` VALUES ('275', '020edfe79a1790f64ec4cf418a166c4f', '2', '1532528717');
INSERT INTO `token` VALUES ('276', '6aa59926c36e5af5063e1cf469a03faa', '2', '1532577182');
INSERT INTO `token` VALUES ('277', '3d4b4d7b4d7b67f4a2a8c90fd1f85812', '2', '1532680227');
INSERT INTO `token` VALUES ('278', '2d13c4167dcedec5dbfe1435cde6b002', '2', '1532740841');
INSERT INTO `token` VALUES ('279', '4eb36a8a9b4c94fd2bcf995b40d32ce2', '2', '1533108010');
INSERT INTO `token` VALUES ('280', 'bbe8487d48c64d855b1aa40cfeb345a7', '2', '1533206153');
INSERT INTO `token` VALUES ('281', '50e7f832697fc241065b6ea998e5f142', '2', '1533290402');
INSERT INTO `token` VALUES ('282', '33bf303bc7184f59cc7464940bd62397', '2', '1533371717');
INSERT INTO `token` VALUES ('283', '9b0b6face081ca63acd572cbbaa15224', '2', '1533690286');
INSERT INTO `token` VALUES ('284', '38914e967112a84a8a7b66eab2a8ddd1', '2', '1534240861');
INSERT INTO `token` VALUES ('285', 'e2096edc713af81f15eda4640aee3a2b', '2', '1534316006');
INSERT INTO `token` VALUES ('286', '15d41da67d0a64b2f2fdf0829f2d0c05', '2', '1534501925');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '和member表id一致',
  `sessionid` char(32) DEFAULT NULL,
  `job` char(20) DEFAULT NULL COMMENT '应聘职位',
  `real_name` varchar(255) NOT NULL COMMENT '姓名',
  `gender` tinyint(1) DEFAULT '1' COMMENT '0=女',
  `birthday` date DEFAULT NULL,
  `email` varchar(255) DEFAULT '' COMMENT '个人电子邮箱',
  `phone` char(30) DEFAULT '' COMMENT '联系电话',
  `qqwechat` char(30) DEFAULT '' COMMENT 'QQ微信',
  `is_married` tinyint(4) DEFAULT '0' COMMENT '是否已婚',
  `nationality` char(10) DEFAULT '' COMMENT '国籍',
  `education` char(10) DEFAULT '' COMMENT '最高学历',
  `residence` varchar(255) DEFAULT '' COMMENT '居住地址',
  `politics` char(2) DEFAULT '' COMMENT '政治面貌',
  `ts` datetime DEFAULT NULL COMMENT '添加时间',
  `last_school` varchar(255) DEFAULT '' COMMENT '最后学校',
  `last_biye` char(10) DEFAULT '' COMMENT '最后毕业',
  `major` char(20) DEFAULT '' COMMENT '专业',
  `school` text COMMENT '教育经历json',
  `project` text COMMENT '实习经历',
  `xingqu` varchar(255) DEFAULT '' COMMENT '兴趣',
  `memo` text COMMENT '自我介绍',
  `st` tinyint(1) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COMMENT='自助简历';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('35', 'dr3l9ne2al1ko0ad6cne3ogmm6', '销售工程师', '陈锦文', '0', '1991-09-28', '15036695525@163.com', '15578984613', '1683857813', '0', '中国', '硕士', '广西南宁', '党员', '2017-10-22 20:49:36', '广西大学', '2018-06-20', '植物种质资源学', '[{\"start\":\"2015-09-10\",\"end\":\"2018-06-20\",\"school\":\"广西大学\",\"education\":\"硕士\",\"major\":\"植物种质资源学\",\"fangx\":\"芒果开花基因克隆转基因功能验证\"}]', '[]', '[]', null, '1');
INSERT INTO `user` VALUES ('10', 'rdvg0ae27cn1koofhit2gej1k4', '产品工程师', '田建文', '1', '1987-12-12', '12@125.com', '13816466135', '', '1', '中国', '硕士', '巴拉巴拉巴拉…', '党员', '2017-09-12 11:26:27', '', null, '', '[{\"start\":\"\",\"end\":\"\",\"school\":\"\",\"education\":\"\",\"major\":\"\"}]', '[{\"start\":\"2015-09-12\",\"end\":\"2017-09-12\",\"project\":\"交集加我\"}]', '[]', '', '1');
INSERT INTO `user` VALUES ('11', '9vh5ts5d1fhea3kfno38cjpeo5', '医学研究员', '胡倩', '0', '2017-09-12', '123@majorbio.com', '12345678', '', '0', '中国', '博士后', '上海周浦', '群众', '2017-09-12 11:27:28', '', null, '', '[{\"start\":\"\",\"end\":\"\",\"school\":\"\",\"education\":\"\",\"major\":\"\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('12', 'sdlrg5uqg8pdcabvu4p9opha57', '生物信息分析工程师', '李世林', '1', '1993-04-01', '1574494@qq.com', '15902138324', '', '0', '中国', '本科', '上海', '团员', '2017-09-12 11:30:41', '南通', '2015-09-11', '人力资源', '[{\"start\":\"2011-09-11\",\"end\":\"2015-09-11\",\"school\":\"南通\",\"education\":\"本科\",\"major\":\"人力资源\"}]', '[{\"start\":\"2011-09-12\",\"end\":\"2015-09-12\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('14', '2hr12llodao12ddmuos114fo02', '遗传分析工程师', '秦海怡', '0', '1981-07-29', 'haiyi.qin@majorbio.com', '13811406220', '', '1', '中国', '硕士', '北京市海淀区安宁庄东路', '群众', '2017-09-12 11:44:34', '', null, '', '[{\"start\":\"\",\"end\":\"\",\"school\":\"\",\"education\":\"\",\"major\":\"\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('26', 'jp7nr7knip50m10afuu70qqa76', '医学研究员', '房炜森', '1', '1992-11-03', 'Fcholes@163.com', '17864276133', '2271668008', '0', '中国', '硕士', '山东青岛中国海洋大学鱼山路5号', '团员', '2017-10-01 13:58:35', '中国海洋大学', '2017-12-01', '生物工程', '[{\"start\":\"2015-08-1\",\"end\":\"2017-12-1\",\"school\":\"中国海洋大学\",\"education\":\"硕士研究生\",\"major\":\"生物工程(海洋生物医用材料)\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('17', '1k70tkk8v07ihqeuren7bo4t67', '医学研究员', '左建伟', '1', '1992-08-14', 'zuojianwei1992@163.com', '17864279179', '1547935989/ZJWGranite', '0', '中国', '硕士', '山东省青岛市', '团员', '2017-09-14 19:30:12', '中国海洋大学', '2018-06-31', '细胞生物学', '[{\"start\":\"2015-09-1\",\"end\":\"2018-6-31\",\"school\":\"中国海洋大学\",\"education\":\"理学硕士\",\"major\":\"细胞生物学\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('18', 'htp326nc1fgg7mh3c17kld4er6', '生物信息分析工程师', '刘娜', '0', '1992-10-06', '1781908198@qq.com', '17864272885', '1781908198', '0', '中国', '硕士', '青岛市市南区鱼山路5号', '团员', '2017-09-14 20:00:44', '中国海洋大学', '2017-12-31', '细胞生物学', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"细胞生物学\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('19', 'nrf0f1gj4sn5s2ar0mmshrqad6', '产品工程师', '丁园园', '0', '1993-04-19', '772638453@qq.com', '17853253126', '772638453', '0', '中国', '硕士', '山东省青岛市市南区鱼山路5号', '团员', '2017-09-14 20:09:32', '中国海洋大学', '2017-12-14', '生物工程', '[{\"start\":\"2016-09-14\",\"end\":\"2017-12-14\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"生物工程\"}]', '[{\"start\":\"2014-09-14\",\"end\":\"2017-01-14\",\"project\":\"大叶黄杨中黄酮类化合物的提取与纯化研究;马尾松中槲皮素提取工艺条件研究;普鲁兰多糖的提取纯化以及不同分子量普鲁兰多糖膜性质研究，以及其在胶囊制剂中的应用\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('20', 'jrklkcgikcbov2566t8t4dte11', '产品工程师', '张雅慧', '0', '1993-02-06', '564927609@qq.com', '15689932652', '564927609', '0', '中国', '硕士', '山东省青岛市市南区鱼山路5号', '党员', '2017-09-14 23:07:23', '河北科技大学', '2015-07-1', '药学', '[{\"start\":\"2011-09-1\",\"end\":\"2015-07-1\",\"school\":\"河北科技大学\",\"education\":\"本科\",\"major\":\"药学\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('21', '5kl0qrrm1dug26r9pdnubub464', '遗传分析师', '丁雅婷', '0', '1992-11-29', 'yatingding@qq.com', '17864272859', '741845874', '0', '中国', '硕士', '山东省青岛市市南区', '团员', '2017-09-15 09:51:48', '中国海洋大学', '2017-12-31', '遗传学', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"遗传学\"}]', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"project\":\"雨生红球藻大规模培养及优良藻种的筛选、雨生红球藻分子生物学相关研究\"}]', '[\"\\u6e38\\u620f\"]', '', '1');
INSERT INTO `user` VALUES ('22', '0l240a215mn8kdino30f7pqn00', '生物信息分析工程师', '温奇秦', '1', '1992-10-05', 'wenqiqin321@163.com', '17853253130', 'w18360538143', '0', '中国', '硕士', '青岛市', '团员', '2017-09-15 15:35:12', '中国海洋大学', '2017-12-30', '生物工程', '[{\"start\":\"2016-09-1\",\"end\":\"2017-12-30\",\"school\":\"中国海洋大学\",\"education\":\"硕士\",\"major\":\"生物工程\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('27', 'mmrf04uaag0l7quu4cs88m09r1', '遗传分析师', '杨悦', '0', '1992-11-01', 'xllyang1101@163.com', '15046113209', '1075739834', '0', '中国', '硕士', '哈尔滨', '团员', '2017-10-05 18:44:00', '', null, '', '[]', '[{\"start\":\"2015-09-1\",\"end\":\"2017-09-12\",\"project\":\"上海辰山植物园联合培养\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('28', 'fi5eecc8es75eupf0ud8aop9b7', '遗传分析师', '丁婷婷', '0', '1993-01-25', 'dudude1108@163.com', '18845877580', '', '0', '中国', '硕士', '黑龙江省哈尔滨市', '团员', '2017-10-07 12:29:59', '哈尔滨医科大学', '2017-12-31', '基础医学', '[{\"start\":\"2011-09-1\",\"end\":\"2017-12-31\",\"school\":\"哈尔滨医科大学\",\"education\":\"硕士\",\"major\":\"基础医学七年制\"}]', '[{\"start\":\"\",\"end\":\"\",\"project\":\"\"}]', '[]', '', '1');
INSERT INTO `user` VALUES ('29', '08cqbd3dpvqkalc78obgdq5b73', '生物信息分析工程师', '赵越', '0', '1992-12-21', 'zylovedata@163.com', '18846047451', '18846047451', '0', '中国', '硕士', '黑龙江 哈尔滨', '团员', '2017-10-07 17:10:32', '哈尔滨医科大学', '2017-07-01', '基础医学', '[{\"start\":\"2011-09-01\",\"end\":\"2017-07-01\",\"school\":\"哈尔滨医科大学\",\"education\":\"硕士\",\"major\":\"基础医学七年制（生物信息））\"}]', '[{\"start\":\"2015-09-01\",\"end\":\"2016-11-01\",\"project\":\"生物医学数据库平台搭建\"},{\"start\":\"\",\"end\":\"2017-10-07\",\"project\":\"基于「全基因组重测序数据」，基于全基因组测序识别同卵异病孤独症患儿的基因组变异\"}]', '[\"\\u770b\\u4e66\"]', '', '1');
INSERT INTO `user` VALUES ('30', 'j28ffi4ph5lj4o5d0lhm2d1dh2', '产品工程师', '何芳', '0', '1993-02-17', '18817502036@163.com', '18817502036', '2251556791', '0', '中国', '硕士', '上海市徐汇区梅陇路130号', '党员', '2017-10-11 14:43:32', '华东理工大学', '2017-12-31', '生物化工', '[{\"start\":\"2015-09-1\",\"end\":\"2017-12-31\",\"school\":\"华东理工大学\",\"education\":\"硕士\",\"major\":\"生物化工\"}]', '[{\"start\":\"2016-09-1\",\"end\":\"2017-08-1\",\"project\":\"外泌体的分离与检测\"}]', '[\"\\u5531\\u6b4c\",\"\\u4e0b\\u68cb\",\"\\u6e38\\u620f\"]', '', '1');
INSERT INTO `user` VALUES ('31', '7gkf3vk9r59q8jucmjckddtr47', '销售工程师', '王楠', '1', '1993-06-02', 'wnan1993@163.com', '13122313379', 'wangnan6283196', '0', '中国', '硕士', '上海市徐汇区', '团员', '2017-10-11 22:05:42', '华东理工大学', '2018-07-1', '生物工程', '[{\"start\":\"2015-09-1\",\"end\":\"2018-07-1\",\"school\":\"华东理工大学\",\"education\":\"硕士\",\"major\":\"生物工程\"}]', '[{\"start\":\"2017-05-1\",\"end\":\"2017-08-10\",\"project\":\"兰州申联生物制药有限公司口蹄疫疫苗悬浮培养生产线的建立\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('36', '4hqnb2e08os7fpc9jrusuilaj7', '医学研究员', '李兴娟', '0', '1992-11-24', '17761700937@163.com', '17761700937', '', '0', '中国', '硕士', '江苏省南京市玄武区卫岗1号', '团员', '2017-10-21 20:18:31', '南京农业大学', '2018-06-23', '微生物学', '[{\"start\":\"2015-09-5\",\"end\":\"2018-06-23\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"微生物学\",\"fangx\":\"微生物与宿主间的相互作用\"},{\"start\":\"2011-09-5\",\"end\":\"2015-06-22\",\"school\":\"廊坊师范学院\",\"education\":\"本科\",\"major\":\"生物技术\",\"fangx\":\"\"}]', '[{\"start\":\"2015-03-5\",\"end\":\"2015-05-25\",\"project\":\"实习\\/本科毕业设计   平菇细菌性褐斑病病原菌分离鉴定\"},{\"start\":\"2017-06-15\",\"end\":\"2017-08-29\",\"project\":\"田菁根瘤菌ROS相关基因ahpC和bcp在抗氧化及共生固氮中的功能研究\"},{\"start\":\"2015-11-6\",\"end\":\"2017-10-21\",\"project\":\"菜豆根瘤菌中影响共生岛转移的基因的研究\"}]', '[]', '', '1');
INSERT INTO `user` VALUES ('37', 'v395voe8tkimjl7edmupp3cav5', '销售工程师', '许思佳', '0', '1992-06-13', '1103660010@qq.com', '18846170419', '', '0', '中国', '硕士', '哈尔滨市香坊区和兴路26号', '党员', '2017-10-22 21:25:57', '东北林业大学', '2018.07.01', '林木遗传育种', '[{\"start\":\"2015.09.01\",\"end\":\"2018.07.01\",\"school\":\"东北林业大学\",\"education\":\"硕士\",\"major\":\"林木遗传育种\",\"fangx\":\"分子生物学\"}]', '[]', '[]', '', '1');
INSERT INTO `user` VALUES ('38', 'j8t2p7naort22v7qh7jccqbae1', '产品工程师', '林梅', '0', '1990-11-20', 'linmei1120@163.com', '18766534510', '', '0', '中国', '硕士', '山东省烟台市莱山区春晖路17号', '团员', '2017-10-23 15:07:33', '浙江海洋大学&amp;中国科学院烟台海岸带研究所', '2018-06-21', '设施农业', '[{\"start\":\"2016-09-11\",\"end\":\"2018-06-21\",\"school\":\"浙江海洋大学&amp;中国科学院烟台海岸带研究所\",\"education\":\"硕士\",\"major\":\"设施农业\",\"fangx\":\"海藻肥的功效验证\"}]', '[]', '[]', null, '1');
INSERT INTO `user` VALUES ('39', 'gf0ih7f29hcoeqgdojn544ttb4', '产品工程师', '孟振祥', '1', '1991-08-01', 'mengzhenxiang@outlook.com', '15062276200', '', '0', '中国', '硕士', '南京市南京农业大学', '团员', '2017-10-23 15:22:30', '南京农业大学', '2018-06-5', '动物营养与饲料学', '[{\"start\":\"2015-09-10\",\"end\":\"2018-06-5\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料学\",\"fangx\":\"瘤胃微生物\"}]', '[{\"start\":\"2015-09-23\",\"end\":\"2018-06-6\",\"project\":\"湖羊产学研项目\"}]', '[]', null, '1');
INSERT INTO `user` VALUES ('40', 'lm0iqi0gfjmpvher6b93vnaen7', '产品工程师', '张晓东', '1', '1991-12-27', '409856956@qq.com', '15850783964', '', '0', '中国', '硕士', '江苏省南京市南京农业大学', '党员', '2017-10-23 15:22:48', '南京农业大学', '2019-07-1', '动物科学', '[{\"start\":\"2016-09-1\",\"end\":\"2019-07-1\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物科学\",\"fangx\":\"消化道微生物\"}]', '[]', '[]', null, '1');
INSERT INTO `user` VALUES ('41', 'svk2bsimajj9fei1grg866e741', '产品工程师', '陈慧子', '0', '1994-06-22', 'huizichen1994@outlook.com', '18260069910', '2455837387', '0', '中国', '硕士', '江苏省南京市玄武区童卫路卫岗一号', '团员', '2017-10-23 15:25:11', '南京农业大学', '2018-06-5', '动物营养与饲料科学', '[{\"start\":\"2015-09-5\",\"end\":\"2018-06-5\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"动物消化道微生物\"}]', '[{\"start\":\"2015-11-10\",\"end\":\"2018-06-5\",\"project\":\"国家973项目 盲肠灌注丙酸钠对生长猪机体代谢及食欲的影响\"}]', '[\"下棋\",\"唱歌\"]', '', '1');
INSERT INTO `user` VALUES ('42', 'n2ithmu11vefehfnhj1va3i371', '产品工程师', '田时祎', '0', '1993-06-24', '731329348@qq.com', '15651989130', '731329348', '0', '中国', '硕士', '江苏省南京市玄武区卫岗1号', '团员', '2017-10-23 15:26:24', '南京农业大学', '2018-06-5', '动物营养与饲料科学', '[{\"start\":\"2015-09-3\",\"end\":\"2018-06-5\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"消化道微生物\"},{\"start\":\"2011-09-4\",\"end\":\"2015-06-6\",\"school\":\"南京农业大学\",\"education\":\"本科\",\"major\":\"动物科学\",\"fangx\":\"\"}]', '[{\"start\":\"2016-09-31\",\"end\":\"2017-09-24\",\"project\":\"低聚半乳糖对哺乳仔猪小肠肠道形态，功能发育，菌群结构的影响\"}]', '[]', '', '1');
INSERT INTO `user` VALUES ('43', 'dlmhsjlv0m2tvbvvso8u1lg813', '销售工程师', '吕雪晴', '0', '1993-01-15', '945302973@qq.com', '15895977157', '945302973', '0', '中国', '硕士', '江苏省南京市玄武区卫岗1号', '团员', '2017-10-23 15:40:00', '南京农业大学', '2019-08-5', '作物遗传育种', '[{\"start\":\"2015-09-4\",\"end\":\"2019-08-5\",\"school\":\"南京农业大学\",\"education\":\"硕士研究生\",\"major\":\"作物遗传育种\",\"fangx\":\"非生物逆境胁迫对水稻影响\"}]', '[{\"start\":\"2015-10-1\",\"end\":\"2018-09-5\",\"project\":\"运用real time pcr技术，western blot技术，DNA提取技术，RNA提取技术，GUS染色技术等。研究非生物逆境胁迫，如干旱、低温、高温、激素等，水稻ZFP150对其的抗性研究。研究ZFP150基因对水稻的影响。\"}]', '[]', '', '1');
INSERT INTO `user` VALUES ('44', 'jfan7t1bg4mv66ra97lpqh39i3', '产品工程师', '张青', '0', '1994-12-08', '390540045@qq.com', '15651958780', '390540045', '0', '中国', '硕士', '南京农业大学', '党员', '2017-10-23 15:43:16', '南京农业大学', '2018-07-1', '动物营养与饲料科学', '[{\"start\":\"2015-09-1\",\"end\":\"2018-07-1\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"动物消化道微生物\"},{\"start\":\"2011-09-1\",\"end\":\"2015-07-1\",\"school\":\"江西农业大学\",\"education\":\"本科\",\"major\":\"动物营养与饲料科学\",\"fangx\":\"\"}]', '[{\"start\":\"2015年12月\",\"end\":\"2018年12月\",\"project\":\"研究霉菌毒素对仔猪的影响及治疗改善方法\"}]', '[\"下棋\",\"游戏\"]', '', '1');
INSERT INTO `user` VALUES ('45', '1om4c8i50vs2rrnsif9b2m1e80', '产品工程师', '张萌萌', '0', '1992-09-30', 'mengmengzhang930@163.com', '18751967698', '840999403', '0', '中国', '硕士', '河南许昌', '党员', '2017-10-23 15:49:28', '南京农业大学', '2018-06-30', '动物营养', '[{\"start\":\"2015-09-1\",\"end\":\"2018-06-30\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"动物营养\",\"fangx\":\"ZBTB16基因对肉牛肌内脂肪细胞分化的影响\"}]', '[{\"start\":\"2015-09-23\",\"end\":\"2017-11-30\",\"project\":\"ZBTB16基因对肉牛肌内脂肪细胞分化的影响\"}]', '[\"唱歌\"]', '', '1');
INSERT INTO `user` VALUES ('46', 'km78v7vbcm7rphou6m3lkq17h0', '生物信息分析工程师', '琚龙贞', '1', '1991-09-29', '2015101164@njau.edu.cn', '15295500906', '1483712916', '1', '中国', '硕士', '南京市玄武区南京农业大学', '党员', '2017-10-23 15:50:24', '南京农业大学', '2018-07-1', '生物信息', '[{\"start\":\"2015-09-1\",\"end\":\"2018-07-1\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"生物信息\",\"fangx\":\"重测序关联分析\"}]', '[{\"start\":\"2016-01-1\",\"end\":\"2018-01-1\",\"project\":\"棉花海岛棉重测序关联分析\"}]', '[\"\\u5531\\u6b4c\",\"\\u4e0b\\u68cb\"]', '', '1');
INSERT INTO `user` VALUES ('47', 'd7nifosjuem5de2gpvrg4kc954', '产品工程师', '张鑫楠', '0', '1992-09-29', '605940719@qq.com', '15805173894', '15805173894', '0', '中国', '硕士', '江苏省南京市玄武区卫岗一号', '团员', '2017-10-23 15:52:53', '南京农业大学', '2019-06-30', '作物遗传育种', '[{\"start\":\"2015-09-1\",\"end\":\"2019-06-30\",\"school\":\"南京农业大学\",\"education\":\"硕士\",\"major\":\"作物遗传育种\",\"fangx\":\"大豆分子生物学育种\"},{\"start\":\"2011-09-1\",\"end\":\"2015-06-30\",\"school\":\"南京农业大学\",\"education\":\"本科\",\"major\":\"农学\",\"fangx\":\"\"}]', '[{\"start\":\"2015-09-1\",\"end\":\"2017-11-30\",\"project\":\"大豆分子育种，课题主要内容为大豆含硫氨基酸相关基因的功能验证\"}]', '[\"\\u5531\\u6b4c\",\"\\u4e0b\\u68cb\"]', null, '1');
