## 1、安装nodeJS（如果已安装可以忽略）
http://blog.csdn.net/xxmeng2012/article/details/51492149
用`npm -v`命令确保安装nodeJS及npm成功
只要默认安装完node就行了，后面一大堆环境变更配置等可忽略

## 2、npm淘宝镜像（如果已安装可以忽略）
`npm install cnpm -g --registry=https://registry.npm.taobao.org`

## 3、Clone代码
git@gitee.com:ssangwy/vuecms.git

## 4.进入项目目录安装（可省略，因为有的人安装太慢，我放在版本库里了，也可以重装一次）
`npm install`

## 5、导入数据库
`docs/数据库.sql`

## 6、添加host
`127.0.0.1		admin.vuecms.lc	api.vuecms.lc	apix.vuecms.lc`

## 7、创建虚拟主机[以apache为例]
接口域名,api是thinkphp3.2  apix是thinkphp5.1
现在的前端其实已经只兼容thinkphp5.1了，留着3.2的接口只是为了在老服务器不好升级php5.6+版本时使用，一般可以忽略
安装thinkphp5.1版本请查看apix目录下的readme.md文件
```
<VirtualHost *:80>
    DocumentRoot "G:\www\vuecms\api\Public"
    ServerName api.vuecms.lc
    SetEnv   DB_HOST           localhost
	SetEnv   DB_USER           root
	SetEnv   DB_PWD
	SetEnv   DB_NAME           vuecms
</VirtualHost>
<VirtualHost *:80>
    DocumentRoot "G:\www\vuecms\apix\public"
    ServerName apix.vuecms.lc
    SetEnv   DB_HOST           localhost
	SetEnv   DB_USER           root
	SetEnv   DB_PWD
	SetEnv   DB_NAME           vuecms
</VirtualHost>
```
上面项目目录和数据库配置，请按你的实际情况配置
#### 后台编译后域名
```
<VirtualHost *:80>
    DocumentRoot "G:\www\vuecms\dist"
    ServerName admin.vuecms.lc
</VirtualHost>
```
编译后的域名虚拟主机不配置也行，本地都是用localhost:8080调试

在浏览器运行http://apix.vuecms.lc/admin/ls
如果输出json里有jwt_secret说明接口正常了，否则请调试接口

要注意很多人在这里配置失败，这通用是你的php环境配置权限引起的，我是在全局倒置里改过的
如果提示权限问题，可以在每个虚拟主机VirtualHost中添加Directory代码块
`
<Directory "G:\www\vuecms\apix\public">
    Options Indexes FollowSymLinks Multiviews
    AllowOverride All
    Order Allow,Deny
    Allow from all
</Directory>
`
## 8、运行开发环境
进入项目目录执行
`npm run dev`
此时已经装好开发环境, 打开浏览器输入地址进入
`localhost:8080`
开发环境工作目录: 用支持vue的编辑器打开vuecms\src即可，我用Visual Studio Code

## 9、编译与发布
进入vuecms目录执行
`npm run build`

编译后会生成dist目录,可以用`admin.vuecms.lc`访问编译后的网站
发布到线上也只需要发布dist目录
最后需要把本目录下的.htaccess文件复制到dist目录（不然无刷新进入其他页面后，一刷新页面就找不到页面）

为了方便提供了run_dev.bat和run_build.bat文件,里面的项目目录请自行修改
双击即可开启测试环境或编译



## 10、说明
配合HTML5的historyAPI达到不刷新页面,但url会变的效果,即刷新页面可以保持当前页面,而且可以支持浏览器的前进,后退键
配合H5的本地缓存,刷新页面或者编辑完返回列表是不会丢失状态,可以保持搜索条件和页码
菜单和权限根据API返回动态更新
本地开发环境是NodeJS支持下的ES6, 内置了打包、编译工具
编译后为打包好的纯浏览器环境
前端UI原本是用Element-ui，后来使用iview，整体更好一些，而且表格比较好实现自定义字段等功能