<?php
// [ 应用入口文件 ]
namespace think;


$params = file_get_contents("php://input"); //兼容非post模式
if($params && !$_POST) {
	$_POST = json_decode($params, true);//post
	if($_POST) {
		unset($_POST['_index']);    //iview自动加上的字段
		unset($_POST['_rowKey']);
	}
	$_REQUEST = array_merge($_POST, $_GET);
}

//定义常量
define('DEBUG_MOD', is_file('./debug'));
define('TS', $_SERVER['REQUEST_TIME']);
define('DS', DIRECTORY_SEPARATOR);

// 加载基础文件
require __DIR__ . '/../vendor/topthink/framework/base.php';

// 执行应用并响应
Container::get('app')->run()->send();
