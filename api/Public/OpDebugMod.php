<?php
define('DEBUG_FILE', './debug');

//是否已经打开了
function is_debug_mod()
{
    return is_file(DEBUG_FILE);
}

//打开
function open_debug_mod()
{
    if(is_debug_mod()){ return; }
    $fopen = fopen(DEBUG_FILE, 'x');
    fputs($fopen, 'debug');
    fclose($fopen);
}

//关闭
function close_debug_mod()
{
    if(is_debug_mod()){ unlink(DEBUG_FILE); }
}

//-------------------------------------操作---
$w = isset($_REQUEST['w']) ? trim($_REQUEST['w']) : '';
if($w == 'y'){ open_debug_mod(); exit(json_encode(array('c'=>0, 'm'=>'ok'))); }
elseif($w == 'n'){ close_debug_mod(); exit(json_encode(array('c'=>0, 'm'=>'ok'))); }
//--------------------------------------------

$is_debug_mod = is_debug_mod();
?>
<!DOCTYPE html>
<html lang="zh-cn">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>控制Debug模式</title>
        <script src="./static/js/jquery-2.1.1.min.js"></script>
        <style type="text/css">
            body{ font-size:12px; font-family: "微软雅黑", Arial, Verdana, Helvetica, sans-serif; }
            fieldset legend{ font-size: 14px; }
            .btn{ cursor: pointer; border-radius: 3px; padding: 1px 6px; color: #fff; }
            .btn-red{ border: 1px solid #D00402; background-color: red;}
            .btn-green{ border: 1px solid #008000; background-color: green;}
            a{ text-decoration: none; font-size: 14px;}
        </style>
    </head>
    <body>
        <fieldset>
            <legend>控制Debug模式</legend>
            <div>
                当前 <span style="background-color:#666; color:#fff;">调试模式</span> 为“<?php echo $is_debug_mod ? '<span style="color:green; font-weight:bold;">打开</span>' : '<span style="color:red; font-weight:bold;">关闭</span>'; ?>”状态，我要：
                <a href="javascript:;" class="btn <?php echo $is_debug_mod ? 'btn-red' : 'btn-green'; ?>" onclick="openDebugMod('<?php echo $is_debug_mod ? 'n' : 'y';;?>')">
                    <?php echo $is_debug_mod ? '☒ 关闭' : '► 打开'; ?>
                </a>
            </div>
        </fieldset>
        <script type="text/javascript">
            function openDebugMod(w)
            {
                if( ! confirm('确认？')){ return false; }
                $.ajax({
                    url:'./OpDebugMod.php?&w='+w,
                    dataType:'json',
                    type:'get',
                    success:function(msg){
                        if(msg.c != 0){
                            alert(msg.m);
                        }
                        window.location.reload();
                    },
                    error:function(){
                        alert('参数错误');
                    }
                });
            }
        </script>
    </body>
</html>
