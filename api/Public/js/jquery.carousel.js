;(function($, window, undefined) {
	function Carousel(ele, opts) {
		this.defaults = {
			size:'fullScreen',
			//size:[700,350],
			startIndex:0,
			horVertical:false,
			arrow: true,
			dots: true,
			autoPlay: false,
			interval: 3000,
			onLeave: function(index) {},
			onEnter: function(index) {}
		};
		this.opts = $.extend({},this.defaults, opts);
		this.index = this.opts.startIndex;
		this.container = $(ele); //外层容器
		this.item = this.container.find('.carousel-item'); //轮播容器
		this.opts.size === 'fullScreen' ? this.w = this.container.width() : this.w = this.opts.size[0];
	}

	Carousel.prototype.display = function() {
		//初始化轮播尺寸
		if (this.opts.size === 'fullScreen') {
			$('html,body').css({width:'100%',height:'100%'});
			$(this.container).css({width:'100%',height:'100%'});
		}else{
		    $(this.container).css({width:this.opts.size[0]+'px',height:this.opts.size[1]+'px'});
		}
		
		//初始化容器
		this.item.css({width: this.w}).wrapAll('<div class="carousel-wrap"></div>');
		
		//容器内容是否水平垂直居中
		if (this.opts.horVertical === true) {
			this.item.each(function(){
				$(this).wrapInner('<div class="table"><div class="table-cell"></div></div>');
			});
		}
		
		//初始化左右导航
		if (this.opts.arrow === true) {
			this.container.parent().append('<span class="carousel-arrow carousel-arrow-left"></span><span class="carousel-arrow carousel-arrow-right"></span>');
		}

		//初始化焦点
		if (this.opts.dots === true) {
			this.container.append('<div class="carousel-dots"></div>');
			var dots = this.item.length;
			for (var i = 0; i < dots; i++) {
				this.container.find('.carousel-dots').append('<i class="dots"></i>');
			}
			this.container.find('.carousel-dots').find(".dots").eq(this.index).addClass('current');
		}

		//初始化默认选中
		this.item.eq(this.index).css({'left': 0}).siblings().css({'left': -this.w});
	}
	
	Carousel.prototype.current = function(index) {
		this.container.find('.dots').eq(index).addClass('current').siblings().removeClass('current');
	}
	
	Carousel.prototype.toLeft = function() {
        var self = this;
		if (!this.item.is(":animated")) {
			this.item.eq(this.index).stop().animate({'left': this.w},500);
			this.item.eq(this.index - 1).stop().css({'left': -this.w}).stop().animate({'left': 0},500,function(){
                    //回调函数
                    if (self.opts.onLeave) {
                        self.opts.onLeave(self.index + 1 > self.item.length - 1 ? 0 : self.index + 1);
                    }
                    if (self.opts.onEnter) {
                        self.opts.onEnter(self.index);
                    }
            });
			this.index > 0 ? this.index--:this.index = this.item.length - 1;
		}
	}
	
	Carousel.prototype.toRight = function() {
        var self = this;
		if (!this.item.is(":animated")) {
			this.index >= this.item.length - 1 ? this.index = 0 : this.index++;
			this.item.eq(this.index - 1).stop().animate({'left': -this.w},500).siblings().css({'left': -this.w});
			this.item.eq(this.index).css({'left': this.w}).stop().animate({'left': 0},500,function(){
                    //回调函数
                    if (self.opts.onLeave) {
                        self.opts.onLeave(self.index == 0 ? self.item.length - 1 : self.index - 1);
                    }
                    if (self.opts.onEnter) {
                        self.opts.onEnter(self.index);
                    }
            });
        }
	}

	Carousel.prototype.autoPlay = function() {
		var self = this;
		if (this.opts.autoPlay) {
			function auto() {
				self.toRight();
				self.current(self.index);
			}
			this.timer = setInterval(auto, this.opts.interval);
		}
	}
	
	Carousel.prototype.init = function() {
		this.display();
		this.autoPlay();
		var self = this;
		this.container.parent().find('.carousel-arrow-left').bind('click',function() {
			self.toLeft();
			self.current(self.index);
		});
		this.container.parent().find('.carousel-arrow-right').bind('click',function() {
			self.toRight();
			self.current(self.index);
		});
		this.container.hover(function() {
			clearInterval(self.timer);
			$(this).find('.dots').bind('click',function() {
				var index = $(this).index();
				self.current(index);
				if (index > self.index) {
					if (!self.item.eq(index).is(":animated")) {
						self.item.eq(self.index).stop(false, true).animate({'left': -self.w},500);
						self.item.eq(index).css({'left': self.w}).stop(false, true).animate({'left': 0},500,function(){
                                //回调函数
                                if (self.opts.onEnter) {
                                    self.opts.onEnter(index);
                                }
                        });
					}
				} else if (index < self.index) {
					if (!self.item.eq(index).is(":animated")) {
						self.item.eq(self.index).stop(false, true).animate({'left': self.w},500);
						self.item.eq(index).css({'left': -self.w}).stop(false, true).animate({'left': 0},500,function(){
                                //回调函数
                                if (self.opts.onEnter) {
                                    self.opts.onEnter(index);
                                }
                        });
					}
				}
              if (self.opts.onLeave) {
                    self.opts.onLeave(self.index);
                }
				self.index = index;
			})
		},function() {
			if (self.opts.autoPlay) {
				self.autoPlay();
			}
		});
		
		$(window).resize(function() {
			self.item.css({width: self.container.width()});
			self.item.eq(self.index).stop().animate({'left': 0}).siblings().css({left: -self.container.width()});
			self.w = self.container.width();
		});
		
	}

	$.fn.helloCarousel = function(opts) {
		var carousel = new Carousel(this, opts);
		return carousel.init();
	}
})(jQuery, window);