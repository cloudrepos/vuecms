<?php
namespace app\www\controller;


class Index extends Base
{
    public function index()
    {
    	$cate = M('cate')->where(['module' => 'product', 'st' => 1, 'site' => SITE])->select();
    	foreach ($cate as &$c) {
    		$c['product'] = M('product')->where(['cate_id' => $c['id'], 'st' => 1, 'site' => SITE])->select();
	    }

        return view('/index', get_defined_vars());
    }

	public function contact()
	{
		return view('/contact', get_defined_vars());
	}

	public function ls()
	{
		return view('/ls', get_defined_vars());
	}

	public function detail()
	{
		return view('/detail', get_defined_vars());
	}
}
