<?php
namespace app\www\controller;

use think\facade\View;


class Base
{
    public function __construct()
    {
    	define('SITE', 1);
	    //全局模板变量
	    View::share('js_dir','/js/');
	    View::share('css_dir','/css/');
	    View::share('ver',"?v=1");
    }
}
