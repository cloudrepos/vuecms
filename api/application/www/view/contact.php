{layout name="layout" /}

		<p class="crumb"><img src="img/news.png"><a  href="javascript:;">首页</a>&gt;<a  href="javascript:;">服务中心地址</a></p>
		<ul class="address-list">
			<li>
				<a href="javascript:;">美吉亲子关系遗传咨询（北京服务中心）</a>
				<p>Beijing Major Judicial Authentication</p>
				<p>地址：北京市朝阳区</p>
				<h5>咨询电话：400-880-5576<a href="javascript:;">在线咨询</a></h5>
				<img src="img/address.png">
			</li>
			<li>
				<a href="javascript:;">美吉亲子关系遗传咨询（广州服务中心）</a>
				<p>Guangzhou Major Judicial Authentication</p>
				<p>地址：广东省广州市海珠区</p>
				<h5>咨询电话：400-880-5576<a href="javascript:;">在线咨询</a></h5>
				<img src="img/address.png">
			</li>
			<li>
				<a href="javascript:;">美吉亲子关系遗传咨询（贵阳服务中心）</a>
				<p>GuiYang Major Judicial Authentication</p>
				<p>地址：贵州省贵阳市南明区</p>
				<h5>咨询电话：400-880-5576<a href="javascript:;">在线咨询</a></h5>
				<img src="img/address.png">
			</li>
		</ul>