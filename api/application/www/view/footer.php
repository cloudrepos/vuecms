<!--弹窗-->
<div class="popupbox-wrap" id="popupbox" >
	<div class="popupbox">
		<a class="popup-close" href="javascript:;"></a>
		<div class="popupbox-top clear">
			<ul>
				<li><a href="javascript:;"></a></li>
				<li><a href="javascript:;"></a></li>
			</ul>
		</div>
	</div>
</div>
<!--footer-->
<div class="footer">
	<div class="center">
		<div class="clear">
			<div class="footer-left fl">
				<ul class="bor-ri">
					<li>关于美吉</li>
					<li><a href="javascript:;">机构简介</a></li>
					<li><a href="javascript:;">专家团队</a></li>
					<li><a href="javascript:;">先进设备</a></li>
					<li><a href="javascript:;">荣誉资质</a></li>
					<li><a href="javascript:;">联系我们</a></li>
				</ul>
				<ul>
					<li>服务项目</li>
					<li><a href="javascript:;">亲子检测</a></li>
					<li><a href="javascript:;">司法鉴定</a></li>
					<li><a href="javascript:;">医学检验</a></li>
				</ul>
				<ul class="border">
					<li>样本类型</li>
					<li><a href="javascript:;">无创样本</a></li>
					<li><a href="javascript:;">血痕样本</a></li>
					<li><a href="javascript:;">口腔试子</a></li>
					<li><a href="javascript:;">毛发样本</a></li>
					<li><a href="javascript:;">指甲样本</a></li>
					<li><a href="javascript:;">其他样本</a></li>
				</ul>
				<ul class="bor-ri">
					<li>美吉资讯</li>
					<li><a href="javascript:;">常见问题</a></li>
					<li><a href="javascript:;">行业动态</a></li>
					<li><a href="javascript:;">机构动态</a></li>
				</ul>
				<ul>
					<li>费用标准</li>
				</ul>
			</div>
			<div class="footer-right fr">
				<p>美吉医学检验中心</p>
				<span><img src="img/footer-tel.png">咨询热线</span>
				<h4>400 880 5576</h4>
				<span>服务时间：07：00-03：00（全年无休）</span>
				<a><img src="img/wechat.png">美吉亲子鉴定中心</a>
				<a><img src="img/wechat1.png">美吉医学检验中心</a>
			</div>
		</div>
		<p class="bot">Copyright © 2015上海美吉医学检验有限公司   备案号：沪ICP备15002673号-2 <img src="img/footer-bot.png"></p>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		$('.service-items-top a').click(function(){
			var _index = $(this).index();
			$(this).addClass('current').siblings().removeClass('current');
			$(this).parents('.service-items').find('.service-items-list').eq(_index).addClass('current').siblings().removeClass('current');
		});
		$('.cqst-left>p a').click(function(){
			var _index = $(this).index();
			$(this).addClass('current').siblings().removeClass('current');
			$(this).parents('.cqst-left').find('.cqst-left-list').eq(_index).addClass('current').siblings().removeClass('current');
		});
	});
</script>
<script type="text/javascript">
	//弹出层JS 居中悬浮
	var marknum = 0;
	function showPopUp(){
		$("#popupbox").show();
		marknum = 1;
	}
	function closePopUp(){
		$("#popupbox").hide();
		marknum = 0;
	}
	function showAgain(){
		if (marknum == 0) {
			showPopUp();
		} else {
			return false;
		}
	}
	$(".popup-close").click(function(){
		closePopUp();
	});
	$(function() {
		if(location.hash != '#debug') {
			setInterval('showAgain();',12000);
		}
	});

	//轮播图
	$(".hello-carousel-1").helloCarousel({
		container: '.hello-carousel-1',
		size:[1920,500],
		autoPlay: true,
		interval: 3000,
		startIndex:0,
		horVertical:false,
		arrow: false,
		dots: true
	});
</script>
</body>
</html>