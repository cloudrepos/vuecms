{include file="header" /}

<!-- 首页轮播图 -->
<div class="carousel-outer">
	<div class="carousel">
		<div class="hello-carousel hello-carousel-1">
			<?php foreach (repeat(1) as $r):?>
			<div class="carousel-item"><a href="#"><img src="{$r.img|img}" alt="{$r.title}"></a></div>
			<?php endforeach;?>
		</div>
	</div>
</div>




<!--服务项目-->
<div class="service-items center wrap">
	<p class="index-title">服务项目<i></i></p>
	<div class="service-items-top">

		<?php foreach ($cate as $k => $c):?>
		<a <?=$k == 0 ? 'class="current"' : ''?> href="javascript:;">{$c.title}</a>
		<?php endforeach;?>

	</div>

	<?php foreach ($cate as $k => $data):?>
	<div class="service-items-list <?=$k == 0 ? 'current' : ''?>">
		<ul class="clear">
			<?php foreach ($data['product'] as $r):?>
			<li>
				<i><img src="{$r.cover|img}"></i>
				<p>{$r.title}</p>
				<span>{$r.sub_title}</span>
				<span>{$r.summary}</span>
				<span><a href="/product/{$r.id}">详细介绍</a>|<a href="javascript:;">费用咨询</a></span>
			</li>
			<?php endforeach;?>
		</ul>
	</div>
	<?php endforeach;?>

</div>
<!--样本类型-->
<div class="sample-type wrap">
	<div class="center">
		<p class="index-title">样本类型<i></i></p>
		<div>
			<ul>
				<?php foreach (repeat(2) as $r):?>
				<li>
					<a href="javascript:;">
						<p>{$r.title}</p>
						<span>{$r.sub_title}</span>
						<span>{$r.summary}</span>
						<h6>在线咨询</h6>
					</a>
					<img src="{$r.img|img}">
				</li>
				<?php endforeach;?>
			</ul>
		</div>
	</div>
</div>
<!--服务流程-->
<div class="process wrap">
	<div class="center">
		<p class="index-title">服务流程<i></i></p>
		<div>
			<ul class="clear">
				<?php foreach (repeat(3) as $r):?>
				<li>
					<a href="javascript:;">
						<i class="trans"><img src="{$r.img|img}"></i>
						<p class="trans">{$r.title}</p>
						<span>{$r.sub_title}</span>
					</a>
				</li>
				<?php endforeach;?>
			</ul>
		</div>
	</div>
</div>
<!--美吉医学检验中心-->
<div class="cqst wrap">
	<div class="center">
		<p class="index-title">美吉医学检验中心<i></i></p>
		<div class="clear">
			<div class="cqst-left fl">
				<p><a class="current" href="javascript:;">关于我们</a><a href="javascript:;">专家团队</a><a href="javascript:;">先进设备</a><a href="javascript:;">荣誉资质</a></p>
				<div  class="cqst-left-list clear current">
					<div><img src="img/mj.png"></div>
					<div><span>上海美吉医学检验有限公司广州分公司配备世界一流的高通量测序平台以及权威专家团队，为出具科学、准确、客观的鉴定结果提供有力保障。目前，本公司已开展司法亲子鉴定、匿名亲子关系遗传咨询、无创胎儿亲子关系遗传咨询等dna亲子鉴定和遗传咨询服务.....</span><a href="javascript:;">[详情+]</a></div>
				</div>
				<div class="cqst-left-list clear">
					<div><img src="img/mj.png"></div>
					<div><span>2上海美吉医学检验有限公司广州分公司配备世界一流的高通量测序平台以及权威专家团队，为出具科学、准确、客观的鉴定结果提供有力保障。目前，本公司已开展司法亲子鉴定、匿名亲子关系遗传咨询、无创胎儿亲子关系遗传咨询等dna亲子鉴定和遗传咨询服务.....</span><a href="javascript:;">[详情+]</a></div>
				</div>
				<div class="cqst-left-list clear">
					<div><img src="img/mj.png"></div>
					<div><span>3上海美吉医学检验有限公司广州分公司配备世界一流的高通量测序平台以及权威专家团队，为出具科学、准确、客观的鉴定结果提供有力保障。目前，本公司已开展司法亲子鉴定、匿名亲子关系遗传咨询、无创胎儿亲子关系遗传咨询等dna亲子鉴定和遗传咨询服务.....</span><a href="javascript:;">[详情+]</a></div>
				</div>
				<div class="cqst-left-list clear">
					<div><img src="img/mj.png"></div>
					<div><span>4上海美吉医学检验有限公司广州分公司配备世界一流的高通量测序平台以及权威专家团队，为出具科学、准确、客观的鉴定结果提供有力保障。目前，本公司已开展司法亲子鉴定、匿名亲子关系遗传咨询、无创胎儿亲子关系遗传咨询等dna亲子鉴定和遗传咨询服务.....</span><a href="javascript:;">[详情+]</a></div>
				</div>
			</div>
			<div class="cqst-right fr">
				<p class="tit"><span>常见问题</span></p>
				<ul>
					<li><a href="javascript:;">广东做落户亲子鉴定价位一般是多少？</a></li>
					<li><a href="javascript:;">上海亲子鉴定机构哪家好？</a></li>
					<li><a href="javascript:;">用头发做亲子鉴定可以保存多久？</a></li>
					<li><a href="javascript:;">怎样做匿名亲子鉴定？</a></li>
					<li><a href="javascript:;">广东做无创胎儿亲子鉴定哪家好?</a></li>
					<li><a href="javascript:;">个人亲子鉴定与司法亲子鉴定有什么？</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>

{include file="footer" /}