<!DOCTYPE html>
<html lang="">
<head>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<link rel="stylesheet" type="text/css" href="/css/index.css" />
	<script type="text/javascript" src="/js/jquery.min.js"></script>
	<script type="text/javascript" src="/js/jquery.carousel.js"></script>
</head>

<body>
<!--  header -->
<div class="header">
	<div class="center clear">
		<a class="home" href="javascript:;"><img src="img/home.png">欢迎访问美吉医学检验中心</a>
		<a class="tel" href="javascript:;"><img src="img/tel.png">客服热线：<span>400-880-5576</span></a>
	</div>
</div>
<!--  导航  -->
<div class="nav clear">
	<div class="center">
		<a class="logo fl" href="javascript:;">美吉医学检验中心</a>
		<ul class="fr">
			<li> <a class="current" href="javascript:;">首页</a></li>
			<li> <a class="has-select" href="javascript:;">服务项目</a>
				<div class="service-item">
					<ul>
						<li>亲子检测类</li>
						<li><a href="javascript:;">无创胎儿亲子鉴定</a></li>
						<li><a href="javascript:;">个人隐私亲子检测</a></li>
						<li><a href="javascript:;">亲缘亲子关系检测</a></li>
						<li><a href="javascript:;">个体识别检测</a></li>
					</ul>
					<ul>
						<li>司法鉴定类</li>
						<li><a href="javascript:;">司法诉讼亲子鉴定</a></li>
						<li><a href="javascript:;">落户上户口亲子鉴定</a></li>
					</ul>
					<ul>
						<li>医学检验类</li>
						<li><a href="javascript:;">HPV自采样检测</a></li>
						<li><a href="javascript:;">ctDNA肿瘤超早期筛查</a></li>
						<li><a href="javascript:;">全基因组染色体疾病筛查</a></li>
						<li><a href="javascript:;">叶酸代谢基因检测</a></li>
					</ul>
				</div>
			</li>
			<li> <a class="has-select" href="javascript:;">样本类型</a>
				<div class="sample">
					<ul>
						<li><a href="javascript:;">无创样本</a></li>
						<li><a href="javascript:;">血痕样本</a></li>
						<li><a href="javascript:;">口腔试子</a></li>
						<li><a href="javascript:;">毛发样本</a></li>
						<li><a href="javascript:;">指甲样本</a></li>
						<li><a href="javascript:;">其他样本</a></li>
					</ul>
				</div>
			</li>
			<li> <a href="javascript:;">专家团队</a></li>
			<li> <a href="javascript:;">费用标准</a></li>
			<li> <a href="javascript:;">联系我们</a></li>
		</ul>
	</div>
</div>