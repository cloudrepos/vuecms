<?php
namespace app\api\middleware;

use app\common\base\Errors;
use think\Request;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use app\common\exception\BaseException;
use app\common\logic\AuthLogic;

class Cors
{
    public function handle(Request $request, \Closure $next)
    {
	    header("Access-Control-Allow-Origin: *");   //为避免反复删浏览器数据，重启浏览器，先用星号
	    header("Access-Control-Allow-Methods: PUT,POST,GET,DELETE,OPTIONS");
	    header("Access-Control-Allow-Headers: Authorization,X-Requested-With,accept,origin,content-type,X_Requested_With,site-id");
	    header("Access-Control-Max-Age: 86400000"); //预检结果缓存时间,调试阶段请注释
	    header("Cache-Control:no-cache,must-revalidate,no-store");//禁止缓存
	    if(request()->isOptions()){ exit('prev check ok'); }//option请求在输出header之后直接返回

	    //下一步
        return $next($request);
    }
}
