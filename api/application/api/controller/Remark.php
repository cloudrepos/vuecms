<?php
namespace app\api\controller;

use app\common\logic\RemarkLogic;

class Remark
{
    public $L;
    public function __construct()
    {
        $this->L = new RemarkLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        ok($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
	    ok($data);
    }

    public function save()
    {
        $this->L->save($_POST);
	    ok();
    }

	public function del($id)
	{
		$this->L->del($id);
		ok();
	}

	public function st($id, $st)
	{
		$data = $this->L->st($id, $st);
		ok($data);
	}

	//获取备注内容
	public function content()
	{
		$name = I('get.name');
		$remark = M('remark')->where(['name' => $name])->find();
		ok($remark['content']);
	}
}