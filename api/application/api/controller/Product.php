<?php
namespace app\api\controller;

use think\Db;
use app\common\logic\ProductLogic;

class Product
{
	public $L;
	public function __construct()
	{
		$this->L = new ProductLogic();
	}

	public function ls()
	{
		$data = $this->L->ls();
		ok($data);
	}

	public function get($id)
	{
		$data = $this->L->get($id);
		ok($data);
	}

	public function save()
	{
		Db::startTrans();
		$this->L->save($_POST);
		Db::commit();
		ok();
	}

	public function del($id)
	{
		$data = $this->L->del($id);
		ok($data);
	}

	public function cate()
	{
		$map = M('news_cate')->map('id', 'title');
		ok($map);
	}
}
