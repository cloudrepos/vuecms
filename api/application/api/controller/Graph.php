<?php
namespace  app\api\controller;

class Graph
{
	public function draw()
	{
		$data = '{"pvalue_data":[[0.295,0.0332,0.411],[1,0.0332,0.0332],[0.201,0.541,0.0332],[0.687,0.0332,0.125],[0.0332,0.0332,1]],"correlation_data":[[0.393,-0.707,0.314],[0,-0.707,0.707],[0.471,0.236,-0.707],[-0.157,0.707,-0.55],[-0.707,0.707,0]],"color_list":["blue","turquoise","brown","green","yellow"],"columns":["A","B","C"],"rows":["MEblue","MEturquoise","MEbrown","MEgreen","MEyellow"],"module_data":[217,635,206,53,111],"params":{"title":"Correlation between  module and trait","start_color":"#0099FF","middle_color":"#FFFFFF","end_color":"#FF0000","if_gap":"false","show_value":true},"size":{"width":800,"height":375}}';

		ok(json_decode($data));
	}

}