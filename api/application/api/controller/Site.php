<?php
namespace app\api\controller;

use app\common\logic\SiteLogic;

class Site
{
    public $L;
    public function __construct()
    {
        $this->L = new SiteLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        ok($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
	    ok($data);
    }

    public function save()
    {
        $this->L->save($_POST);
	    ok();
    }

	public function del($id)
	{
		$this->L->del($id);
		ok();
	}

	public function st($id, $st)
	{
		$data = $this->L->st($id, $st);
		ok($data);
	}

	//站点select数据
	public function map()
	{
		$data = $this->L->ls();
		ok($data);
	}
}