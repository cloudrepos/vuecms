<?php
namespace app\api\controller;

use app\common\logic\AdminLogic;

class Admin
{
    public $L;
    public function __construct()
    {
        $this->L = new AdminLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        ok($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
	    ok($data);
    }
    
    //单个/批量删除
    public function delete($id)
    {
        $this->L->delete($id);
	    ok();
    }

	public function edit()
	{
		$this->L->edit($_POST);
		ok();
	}
    
    public function save()
    {
        $data = $this->L->save($_POST);
	    ok();
    }

	public function st($id, $st)
	{
		$this->L->st($id, $st);
		ok();
	}

	public function del($id)
	{
		$this->L->del($id);
		ok();
	}
}