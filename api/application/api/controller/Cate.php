<?php
namespace app\api\controller;

use app\common\logic\CateLogic;

class Cate
{
	public $L;
	public function __construct()
	{
		$this->L = new CateLogic();
	}

	public function ls($module)
	{
		$data = $this->L->ls($module);
		ok($data);
	}

	public function get($id)
	{
		$data = $this->L->get($id);
		ok($data);
	}

	public function save()
	{
		$this->L->save($_POST);
		ok();
	}

	public function del($id)
	{
		$this->L->del($id);
		ok();
	}

	//分类select数据
	public function map($module)
	{
		$data = $this->L->ls($module);
		$map = array();
		foreach($data as $d) {
			if($d['lv'] == 2) $d['title'] = '　├　' . $d['title'];
			if($d['lv'] == 3) $d['title'] = '　　　├　' . $d['title'];
			$d['id'] = $d['id'].'_';	//如果不加个后辍，json格式会按索引数据方式进行按id排序，这样层级关系就错乱了
			$map[$d['id']] = $d['title'];
		}
		ok($map);
	}

	//保存自定义字段配置
	public function save_setting()
	{
		$id = (int) I('get.id');
		$this->L->save_setting($id, I('post.'));
		ok();
	}

	//加载分类自定义字段
	public function custom($id)
	{
		$custom = $this->L->custom($id);
		ok($custom);

	}
}
