<?php
namespace app\api\controller;

use app\common\logic\AdminLogic;
use app\common\logic\AuthLogic;

class Auth
{
	public function __construct()
	{
		$this->Auth = new AuthLogic();
		$this->Admin = new AdminLogic();
	}

	//用户登录
    public function login()
    {
        $id = $this->Auth->login(I('post.user', ''), I('post.pwd', ''));
        $jwt = $this->Auth->createToken($id);
        //获取用户信息，菜单，路由
        $admin = $this->Admin->get($id);
        $data = $this->Auth->getMenuRouter($admin['role']);
        $admin['menu'] = $data['menu'];
        $admin['router'] = $data['router'];
        $admin['Authorization'] = $jwt;
        return json()->header(['Authorization'=>$jwt])->data(rs(0, 'ok', $admin));
    }
}
