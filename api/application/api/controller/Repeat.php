<?php
namespace app\api\controller;

use app\common\logic\RepeatLogic;

class Repeat
{
    public $L;
    public function __construct()
    {
        $this->L = new RepeatLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        foreach ($data as &$d) {
        	$d['number'] = M('repeat_item')->where("pid = {$d['id']}")->count();
        }
        ok($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
	    ok($data);
    }

    public function save()
    {
        $this->L->save($_POST);
	    ok();
    }

	public function del($id)
	{
		$this->L->del($id);
		ok();
	}

	public function st($id, $st)
	{
		$data = $this->L->st($id, $st);
		ok($data);
	}
}