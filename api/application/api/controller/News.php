<?php
namespace app\api\controller;

use app\common\logic\NewsLogic;


class News
{
    public $L;
    public function __construct()
    {
        $this->L = new NewsLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
	    ok($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
	    ok($data);
    }

    public function save()
    {
        $this->L->save($_POST);
	    ok();
    }

	public function del($id)
	{
		$this->L->del($id);
		ok();
	}

	public function cate()
	{
		$map = M('news_cate')->map('id', 'title');
		ok($map);
	}

	public function st($id, $st)
	{
		$this->L->st($id, $st);
		ok();
	}
}