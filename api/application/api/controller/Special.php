<?php
namespace app\api\controller;

use app\common\logic\SpecialLogic;

class Special
{
    public $L;
    public function __construct()
    {
        $this->L = new SpecialLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        json($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
        json(1, $data);
    }

    public function save()
    {
		M()->startTrans();
        $this->L->save($_POST);
		M()->commit();
		json(1);
    }

	public function del($id)
	{
		$data = $this->L->del($id);
		json($data);
	}

	public function cate()
	{
		$map = M('news_cate')->map('id', 'title');
		json(1, $map);
	}
}