<?php
namespace app\api\controller;

use app\common\logic\RepeatItemLogic;

class RepeatItem
{
    public $L;
    public function __construct()
    {
        $this->L = new RepeatItemLogic();
    }
    
    public function ls($pid)
    {
        $data = $this->L->ls($pid);
        ok($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
	    ok($data);
    }

    public function save()
    {
        $this->L->save($_POST);
	    ok();
    }

	public function del($id)
	{
		$this->L->del($id);
		ok();
	}
}