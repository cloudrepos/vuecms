<?php
namespace app\api\controller;

use app\common\base\Errors;
use app\common\exception\BaseException;

class Pub
{
    public function miss()
    {
        throw new BaseException(4000, Errors::$codes[4000]);
    }
}
