<?php
namespace app\api\controller;

use app\common\base\Menu;
use app\common\logic\RoleLogic;

class Role
{
	public $L;
	public function __construct()
	{
		$this->L = new RoleLogic();
	}

	//权限列表
	public function ls()
	{
		$data = $this->L->ls();
		ok($data);
	}

	//权限列表(key=>value值)
	public function map()
	{
		$data = $this->L->map();
		ok($data);
	}

	//获取所有权限
	public function allpri()
	{
		ok(Menu::$menu);
	}

	public function get($id)
	{
		$data = $this->L->get($id);
		ok($data);
	}

	public function save()
	{
		$this->L->save($_POST);
		ok();
	}

	public function st($id, $st)
	{
		$this->L->st($id, $st);
		ok();
	}

	//排序
	public function px()
	{
		$this->L->px($_POST['ids']);
		ok();
	}
}