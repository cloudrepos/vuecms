<?php
namespace app\api\controller;

use Think\Upload;
use Think\facade\Env;
class Ueditor
{
	public $L;
	public function __construct()
	{

	}

	public function config()
	{
		$config = include(Env::get('root_path').'/config/ueditor.php');
		$action = $_GET['action'];
		switch ($action) {
			case 'config':
				$result =  json_encode($config);
				break;
			case 'uploadimage':
			case 'uploadscrawl':
			case 'uploadvideo':
			case 'uploadfile':
				$result = $this->upload();
				break;
			case 'listimage':
				$result = $this->ls();
				break;
			case 'listfile':
				$result = $this->ls();
				break;
			case 'catchimage':
				//抓取远程文件
				//$result = include("action_crawler.php");
				break;
			default:
				$result = json_encode(array(
					'state'=> '请求地址出错'
				));
				break;
		}

		/* 输出结果 */
		if (isset($_GET["callback"])) {
			if (preg_match("/^[\w_]+$/", $_GET["callback"])) {
				echo htmlspecialchars($_GET["callback"]) . '(' . $result . ')';
			} else {
				echo json_encode(array(
					'state'=> 'callback参数不合法'
				));
			}
		} else {
			echo $result;
		}
	}

	public function upload()
	{
		$admin_id = ADMIN_ID;
		if($admin_id < 1){
			echo json_encode(array('state'=> '请登录'), 256);exit;
		}
		//thinkphp 5文件上传
		$file = request()->file('upfile');
		$info = $file->move('./upload/files/');

		if($info){
			$ext = $info->getExtension();
			$title = $info->getFilename();
			$file2 = $info->getSaveName();
			$url = 'http://'.$_SERVER['HTTP_HOST'].'/upload/files/'.$file2;

			$return = array(
				"state" => "SUCCESS",          //上传状态，上传成功时必须返回"SUCCESS"
				"url" => $url,            //返回的地址
				"title" => $_FILES['upfile']['name'],          //显示在图片的alt中
				"original" => $_FILES['upfile']['name'],       //原始文件名
				"type" => $ext,            //文件类型
				"size" => $_FILES['upfile']['size'],           //文件大小
			);
			/* 返回数据 */
			echo json_encode($return); exit;
		}else{
			echo json_encode(array('state'=> $file->getError()), 256);exit;
		}
	}


	//上传文件
	public function ls()
	{
		echo 'list files';
		exit;
		include "Uploader.class.php";

		/* 判断类型 */
		switch ($_GET['action']) {
			/* 列出文件 */
			case 'listfile':
				$allowFiles = $CONFIG['fileManagerAllowFiles'];
				$listSize = $CONFIG['fileManagerListSize'];
				$path = $CONFIG['fileManagerListPath'];
				break;
			/* 列出图片 */
			case 'listimage':
			default:
				$allowFiles = $CONFIG['imageManagerAllowFiles'];
				$listSize = $CONFIG['imageManagerListSize'];
				$path = $CONFIG['imageManagerListPath'];
		}
		$allowFiles = substr(str_replace(".", "|", join("", $allowFiles)), 1);

		/* 获取参数 */
		$size = isset($_GET['size']) ? htmlspecialchars($_GET['size']) : $listSize;
		$start = isset($_GET['start']) ? htmlspecialchars($_GET['start']) : 0;
		$end = $start + $size;

		/* 获取文件列表 */
		$path = $_SERVER['DOCUMENT_ROOT'] . (substr($path, 0, 1) == "/" ? "" : "/") . $path;
		$files = getfiles($path, $allowFiles);
		if (!count($files)) {
			return json_encode(array(
				"state" => "no match file",
				"list" => array(),
				"start" => $start,
				"total" => count($files)
			));
		}

		/* 获取指定范围的列表 */
		$len = count($files);
		for ($i = min($end, $len) - 1, $list = array(); $i < $len && $i >= 0 && $i >= $start; $i--) {
			$list[] = $files[$i];
		}

		/* 返回数据 */
		$result = json_encode(array(
			"state" => "SUCCESS",
			"list" => $list,
			"start" => $start,
			"total" => count($files)
		));

		return $result;


		/**
		 * 遍历获取目录下的指定类型的文件
		 * @param $path
		 * @param array $files
		 * @return array
		 */
		function getfiles($path, $allowFiles, &$files = array())
		{
			if (!is_dir($path)) return null;
			if (substr($path, strlen($path) - 1) != '/') $path .= '/';
			$handle = opendir($path);
			while (false !== ($file = readdir($handle))) {
				if ($file != '.' && $file != '..') {
					$path2 = $path . $file;
					if (is_dir($path2)) {
						getfiles($path2, $allowFiles, $files);
					} else {
						if (preg_match("/\.(" . $allowFiles . ")$/i", $file)) {
							$files[] = array(
								'url' => substr($path2, strlen($_SERVER['DOCUMENT_ROOT'])),
								'mtime' => filemtime($path2)
							);
						}
					}
				}
			}
			return $files;
		}
	}
}