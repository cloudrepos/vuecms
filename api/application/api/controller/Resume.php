<?php
namespace app\api\controller;

use app\common\logic\ResumeLogic;

class Resume
{
    public $L;
    public function __construct()
    {
        $this->L = new ResumeLogic();
    }
    
    public function ls()
    {
        $data = $this->L->ls();
        ok($data);
    }
    
    public function get($id)
    {
        $data = $this->L->get($id);
	    ok($data);
    }
    
    //单个/批量删除
    public function job()
    {
        $data = $this->L->job();
	    ok($data);
    }

    //单个/批量删除
    public function del($id)
    {
        $data = $this->L->del($id);
	    ok($data);
    }
    
    public function save()
    {
        $this->L->save($_POST);
	    ok();
    }

	public function st($id, $st)
	{
		$this->L->st($id, $st);
		ok();
	}
}