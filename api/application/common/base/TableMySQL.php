<?php
namespace app\common\base;

class TableMySQL
{
    public static $tables = [
        'admin'=>'admin',
        'role'=>'role',
    ];

    public static function getTableName($t = '')
    {
        return isset(self::$tables[$t]) ? self::$tables[$t] : null;
    }
}
