<?php
namespace app\common\base;

use app\common\traits\HtmlSelect;

class Vars
{
    use HtmlSelect;

    //账号来源定义 TW = Third Way
    const TW_MIDDLE = 0;
    const TW_OM = 1;
    const TW_SANGER = 2;
    const TW_NGS_CUSTOMER = 3;
    const TW_SANGER_CUSTOMER = 4;
    const TW_FIRST_CUSTOMER = 5;

    //账号来源
    public static $third_way = [
        self::TW_MIDDLE=>[
            'k'=>self::TW_MIDDLE,
            'v'=>'网盘系统账号',
            'secret'=>''
        ],
        self::TW_OM=>[
            'k'=>self::TW_OM,
            'v'=>'OM账号',
            'secret'=>'bd10d382f5d34bd3bb3ee6ba87ea31c2'
        ],
        self::TW_SANGER=>[
            'k'=>self::TW_SANGER,
            'v'=>'云平台账号',
            'secret'=>'4b73565ce74b4401a764193e042fa0cd'
        ],
        self::TW_NGS_CUSTOMER=>[
            'k'=>self::TW_NGS_CUSTOMER,
            'v'=>'高通量客户',
            'secret'=>'c6e0acabe4ca4fcab502d887728e5859'
        ],
        self::TW_SANGER_CUSTOMER=>[
            'k'=>self::TW_SANGER_CUSTOMER,
            'v'=>'云平台客户',
            'secret'=>'532ba640fe4f46f99f266fa3fcf070d9'
        ],
        self::TW_FIRST_CUSTOMER=>[
            'k'=>self::TW_FIRST_CUSTOMER,
            'v'=>'常规客户',
            'secret'=>'69de7c372b3441bf8e0794000be34006'
        ]
    ];

    //性别
    public static $gender = [
        1=>['k'=>1, 'v'=>'男', 'css'=>'text-info'],
        0=>['k'=>0, 'v'=>'女', 'css'=>'text-danger']
    ];

    //用户状态
    public static $st = [
        0=>['k'=>0, 'v'=>'禁用', 'css'=>'text-muted'],
        1=>['k'=>1, 'v'=>'正常', 'css'=>'text-success']
    ];

    //bucket object 创建或共享
    public static $own_type = [
        0=>['k'=>0, 'v'=>'创建', 'css'=>'text-success'],
        1=>['k'=>1, 'v'=>'共享', 'css'=>'text-info']
    ];

    //object_user 删除状态
    public static $del = [
        0=>['k'=>0, 'v'=>'正常', 'css'=>'text-primary'],
        1=>['k'=>1, 'v'=>'删除', 'css'=>'text-red']
    ];

    //性别select
    public static function getGenderSelect($choose=-1, $name='cm_user_gender', $onchange='', $disabled=false, $fst='----')
    {
        return self::getSelect0(self::$gender, $choose, $name, $onchange, $disabled, $fst);
    }
    //性别kv
    public static function getGenderKeyByValue($gender_v = '男')
    {
        $vk = array_column(self::$gender, 'k', 'v');
        return (array_key_exists($gender_v, $vk)) ? $vk[$gender_v] : 1;
    }

    //用户状态select
    public static function getStSelect($choose=-1, $name='st', $onchange='', $disabled=false, $fst='----')
    {
        return self::getSelect0(self::$st, $choose, $name, $onchange, $disabled, $fst);
    }

    //用户状态select
    public static function getOwnTypeSelect($choose=-1, $name='tp', $onchange='', $disabled=false, $fst='----')
    {
        return self::getSelect0(self::$own_type, $choose, $name, $onchange, $disabled, $fst);
    }

    //用户状态select
    public static function getDelSelect($choose=-1, $name='del', $onchange='', $disabled=false, $fst='----')
    {
        return self::getSelect0(self::$del, $choose, $name, $onchange, $disabled, $fst);
    }

    //账号来源select
    public static function getThirdWaySelect($choose=-1, $name='third_way', $onchange='', $disabled=false, $fst='----')
    {
        return self::getSelect0(self::$third_way, $choose, $name, $onchange, $disabled, $fst);
    }
}
