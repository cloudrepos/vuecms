<?php
namespace app\common\base;

use think\Db;
use app\common\traits\ErrorRecord;

class BaseModel
{
    use ErrorRecord;

    /**
     * 得到表数据
     *
     * @param int    $w    1:计数;  2:数据  3:计数&数据  4:一条数据
     * @param array  $sql  array('sql_ct'=>'计数语句', 'sql'=>'数据语句')
     *
     * @return mixed
     */
    public function getData($w=0, $sql=[])
    {
        //只得到计数
        if($w === 1)
        {
            $ct = Db::query($sql['sql_ct']);
            return intval($ct[0]['tp_count']);
        }

        //只得到数据
        if($w === 2)
        {
            return Db::query($sql['sql']);
        }

        //得到计数 & 数据
        if($w === 3)
        {
            $datas = [];
            $ct = Db::query($sql['sql_ct']);
            $datas['tot'] = intval($ct[0]['tp_count']);
            $datas['datas'] = Db::query($sql['sql']);
            return $datas;
        }

        //得到一条数据
        if($w === 4)
        {
            $rs = Db::query($sql['sql']);
            if(empty($rs)){
                return [];
            }else{
                return array_shift($rs);
            }
        }

        return [];
    }

    /**
     * 新增数据
     *
     * @param string $table
     * @param array  $row
     *
     * @throws
     *
     * @return int
     */
    public function addRow($table='', $row=[])
    {
        $table = trim($table); if($table == ''){ throw new \Exception('缺少表'); }
        if(empty($row) || !is_array($row)){ throw new \Exception('缺少数据'); }
        $insertID = Db::table($table)->insertGetId($row);
        $insertID = intval($insertID);
        if($insertID < 1){ throw new \Exception('写入失败'); }
        else{ return $insertID; }
    }

    /**
     * 批量新增数据
     *
     * @param string $table
     * @param array  $data
     *
     * @throws
     *
     * @return int
     */
    public function addAllData($table='', $data=[])
    {
        $table = trim($table); if($table == ''){ throw new \Exception('缺少表'); }
        if(empty($data) || !is_array($data)){ throw new \Exception('缺少数据'); }
        $numRows = Db::table($table)->insertAll($data);
        $numRows = intval($numRows);
        if($numRows < 1){ throw new \Exception('写入失败'); }
        else{ return $numRows; }
    }

    /**
     * 更新表数据
     *
     * @param string $table
     * @param array  $where
     * @param array  $dat
     *
     * @throws
     *
     * @return int|boolean
     */
    public function updateRow($table='', $where=[], $dat=[])
    {
        $table = trim($table); if($table == ''){ throw new \Exception('缺少表'); }
        if(empty($where) || !is_array($where)){ throw new \Exception('缺少更新条件'); }
        if(empty($dat) || !is_array($dat)){ throw new \Exception('缺少更新数据'); }
        $numRows = Db::table($table)->where($where)->update($dat);
        $numRows = intval($numRows);
        return $numRows;
    }

    /**
     * 删除表数据
     *
     * @param string $table
     * @param array  $where
     *
     * @throws
     *
     * @return int|boolean
     */
    public function delRow($table='', $where=[])
    {
        $table = trim($table); if($table == ''){ throw new \Exception('缺少表'); }
        if(empty($where) || !is_array($where)){ throw new \Exception('缺少删除条件'); }
        $numRows = Db::table($table)->where($where)->delete();
        $numRows = intval($numRows);
        return $numRows;
    }

    /**
     * 更新字段增减
     *
     * @param string $tp
     * @param string $table
     * @param array  $where
     * @param string $field
     * @param int    $step
     *
     * @throws
     *
     * @return int|boolean
     */
    public function setIncDec($tp='i', $table='', $where=[], $field='', $step=1)
    {
        $tp = trim($tp); if( ! in_array($tp, ['i', 'd'])){ throw new \Exception('缺少类型'); }
        $table = trim($table); if($table == ''){ throw new \Exception('缺少表'); }
        if(empty($where) || !is_array($where)){ throw new \Exception('缺少删除条件'); }
        $field = trim($field); if($field == ''){ throw new \Exception('缺少更新字段'); }
        $step = intval($step); if($step < 1){ $step = 1; }
        if($tp == 'i'){
            $numRows = Db::table($table)->where($where)->setInc($field, $step);
        }else{
            $numRows = Db::table($table)->where($where)->setDec($field, $step);
        }
        $numRows = intval($numRows);
        return $numRows;
    }

    /**
     * 更新字段增
     *
     * @param string $table
     * @param array  $where
     * @param string $field
     * @param int    $step
     *
     * @return int|boolean
     */
    public function setFieldInc($table='', $where=[], $field='', $step=1)
    {
        return $this->setIncDec('i', $table, $where, $field, $step);
    }

    /**
     * 更新字段
     *
     * @param string $table
     * @param array  $where
     * @param string $field
     * @param int    $step
     *
     * @return int|boolean
     */
    public function setFieldDec($table='', $where=[], $field='', $step=1)
    {
        return $this->setIncDec('d', $table, $where, $field, $step);
    }
}
