<?php
namespace app\common\model\admin;

use think\Db;
use app\common\base\BaseModel;

class AdminModel extends BaseModel
{
    private static $_instance = null;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * 得到表数据【此写法适用于：复杂、可变、多条件查询】
     *
     * @param int    $w        1:计数;  2:数据  3:计数&数据  4:一条数据
     * @param array  $search
     * @param string $fields
     * @param int    $page
     * @param int    $pageSize
     * @param string $by
     *
     * @throws
     *
     * @return mixed
     */
    public function getAdmin($w=3, $search=[], $fields='*', $page=0, $pageSize=0, $by='')
    {
        //查询条件
        $where = [];
        if( ! empty($search))
        {
            isset($search['id']) && $where[] = ['id', '=', intval($search['id'])];
            isset($search['id_neq']) && $where[] = ['id', '<>', intval($search['id_neq'])];
            isset($search['user']) && $search['user']!='' && $where[] = ['user', '=', $search['user']];
            isset($search['user_like']) && $search['user_like']!='' && $where[] = ['user', 'like', '%'.$search['user_like'].'%'];
            isset($search['name']) && $search['name']!='' && $where[] = ['name', '=', $search['name']];
            isset($search['name_like']) && $search['name_like']!='' && $where[] = ['name', 'like', '%'.$search['name_like'].'%'];
            isset($search['nick_name']) && $search['nick_name']!='' && $where[] = ['nick_name', '=', $search['nick_name']];
            isset($search['nick_name_like']) && $search['nick_name_like']!='' && $where[] = ['nick_name', 'like', '%'.$search['nick_name_like'].'%'];
            isset($search['email']) && $search['email']!='' && $where[] = ['email', '=', $search['email']];
            isset($search['email_like']) && $search['email_like']!='' && $where[] = ['email', 'like', '%'.$search['email_like'].'%'];
            isset($search['mobile']) && $search['mobile']!='' && $where[] = ['mobile', '=', $search['mobile']];
            isset($search['mobile_like']) && $search['mobile_like']!='' && $where[] = ['mobile', 'like', '%'.$search['mobile_like'].'%'];
            isset($search['dep']) && $search['dep']!='' && $where[] = ['dep', '=', $search['dep']];
            isset($search['dep_like']) && $search['dep_like']!='' && $where[] = ['dep', 'like', '%'.$search['dep_like'].'%'];
            isset($search['role']) && $search['role']!='' && $where[] = ['role', '=', $search['role']];
        }

        //排序
        $by = trim($by);
        if($by == ''){
            $by = 'id dsc';
        }

        $sql['sql_ct'] = Db::table(tb('admin'))->field($fields)->where($where)->order($by)->fetchSql(true)->count();
        $sql['sql']    = Db::table(tb('admin'))->field($fields)->where($where)->order($by)->limit((($page-1)*$pageSize), $pageSize)->fetchSql(true)->select();
        //ddd($sql,1,1);

        return $this->getData($w, $sql);
    }

    /**
     * 添加
     *
     * @param array $dat
     *
     * @return mixed
     */
    public function addAdmin($dat = [])
    {
        return $this->addRow(tb('admin'), $dat);
    }

    /**
     * 修改
     *
     * @param array $dat 需要更新的数据
     * @param array $where
     *
     * @return mixed
     */
    public function updateAdmin($where=[], $dat=[])
    {
        return $this->updateRow(tb('admin'), $where, $dat);
    }

    /**
     * 删除
     *
     * @param array $where
     *
     * @return mixed
     */
    public function delAdmin($where = [])
    {
        return $this->delRow(tb('admin'), $where);
    }
}
