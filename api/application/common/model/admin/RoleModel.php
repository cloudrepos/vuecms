<?php
namespace app\common\model\admin;

use think\Db;
use app\common\base\BaseModel;

class RoleModel extends BaseModel
{
    private static $_instance = null;

    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * 得到表数据【此写法适用于：复杂、可变、多条件查询】
     *
     * @param int    $w        1:计数;  2:数据  3:计数&数据  4:一条数据
     * @param array  $search
     * @param string $fields
     * @param int    $page
     * @param int    $pageSize
     * @param string $by
     *
     * @throws
     *
     * @return mixed
     */
    public function getRole($w=3, $search=[], $fields='*', $page=0, $pageSize=0, $by='')
    {
        //查询条件
        $where = [];
        if( ! empty($search))
        {
            isset($search['id']) && $where[] = ['id', '=', intval($search['id'])];
            isset($search['id_neq']) && $where[] = ['id', '<>', intval($search['id_neq'])];
            isset($search['name']) && $search['name']!='' && $where[] = ['name', '=', $search['name']];
            isset($search['role']) && $search['role']!='' && $where[] = ['role', '=', $search['role']];
            isset($search['st']) && intval($search['st'])>0 && $where[] = ['st', '=', intval($search['st'])];
        }

        //排序
        $by = trim($by);
        if($by == ''){
            $by = 'id dsc';
        }

        $sql['sql_ct'] = Db::table(tb('role'))->field($fields)->where($where)->order($by)->fetchSql(true)->count();
        $sql['sql']    = Db::table(tb('role'))->field($fields)->where($where)->order($by)->limit((($page-1)*$pageSize), $pageSize)->fetchSql(true)->select();
        //ddd($sql,1,1);

        return $this->getData($w, $sql);
    }

    /**
     * 添加
     *
     * @param array $dat
     *
     * @return mixed
     */
    public function addRole($dat = [])
    {
        return $this->addRow(tb('role'), $dat);
    }

    /**
     * 修改
     *
     * @param array $where
     * @param array $dat 需要更新的数据
     *
     * @return mixed
     */
    public function updateRole($where=[], $dat=[])
    {
        return $this->updateRow(tb('role'), $where, $dat);
    }

    /**
     * 删除
     *
     * @param array $where
     *
     * @return mixed
     */
    public function delRole($where = [])
    {
        return $this->delRow(tb('role'), $where);
    }
}
