<?php
namespace app\common\traits;

trait HtmlSelect
{
    //空select
    public static $blank_select = '<select><option> - - 请选择 - - </option></select>';

    /**
     * 组装select
     *      适用格式：
     *      $datas = [1=>['k'=>1, 'v'=>'aaa'], 2=>['k'=>2, 'v'=>'bbb']]
     *
     * @param array   $datas
     * @param int     $choose 默认选中
     * @param string  $name 名称、ID
     * @param string  $onchange 事件
     * @param boolean $disabled
     * @param mixed   $fst
     * @param boolean $sis (show in select 在下拉中显示)
     *
     * @return string
     */
    public static function getSelect0($datas=[], $choose=-1, $name='dft', $onchange='', $disabled=false, $fst='---', $sis=false)
    {
        $dis = $disabled ? 'disabled="disabled"' : '';
        $choose = intval($choose);
        $html = '<select autocomplete="off" class="form-control form-focus input_text" name="'.$name.'" id="'.$name.'" onchange="'.$onchange.'" '.$dis.'>';
        if(false !== $fst)
        {
            $html .= '<option value="-1">'.$fst.'</option>';
        }
        if( ! empty($datas))
        {
            foreach($datas as $v)
            {
                if(true===$sis && isset($v['sis']))
                {
                    if(false === $v['sis']){ continue; }
                }
                $sel = ($v['k'] === $choose) ? 'selected="selected"' : '';
                $html .= '<option value="'.$v['k'].'" '.$sel.'>'.$v['v'].'</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * 组装select
     *      适用格式：
     *      $datas = ['hello'=>'aaa', 'er'=>'bbb', 'you'=>'xxx']
     *
     * @param array   $datas
     * @param int     $choose 默认选中
     * @param string  $name 名称、ID
     * @param string  $onchange 事件
     * @param boolean $disabled
     * @param mixed   $fst
     *
     * @return string
     */
    public static function getSelect1($datas=[], $choose=-1, $name='dft', $onchange='', $disabled=false, $fst='---')
    {
        $dis = $disabled ? 'disabled="disabled"' : '';
        $html = '<select autocomplete="off" class="form-control form-focus input_text" name="'.$name.'" id="'.$name.'" onchange="'.$onchange.'" '.$dis.'>';
        if(false !== $fst)
        {
            $html .= '<option value="-1">'.$fst.'</option>';
        }
        if( ! empty($datas))
        {
            foreach($datas as $k=>$v)
            {
                $sel = ($k == $choose) ? 'selected="selected"' : '';
                $html .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * 组装select
     *      适用格式：
     *      $datas = [1=>'aaa', 2=>'bbb', 3=>'xxx']
     *
     * @param array   $datas
     * @param int     $choose 默认选中
     * @param string  $name 名称、ID
     * @param string  $onchange 事件
     * @param boolean $disabled
     * @param mixed   $fst
     *
     * @return string
     */
    public static function getSelect2($datas=[], $choose=-1, $name='dft', $onchange='', $disabled=false, $fst='---')
    {
        $dis = $disabled ? 'disabled="disabled"' : '';
        $html = '<select autocomplete="off" class="form-control form-focus input_text" name="'.$name.'" id="'.$name.'" onchange="'.$onchange.'" '.$dis.'>';
        if(false !== $fst)
        {
            $html .= '<option value="-1">'.$fst.'</option>';
        }
        if( ! empty($datas))
        {
            $choose = intval($choose);
            foreach($datas as $k=>$v)
            {
                $sel = ($k === $choose) ? 'selected="selected"' : '';
                $html .= '<option value="'.$k.'" '.$sel.'>'.$v.'</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * 组装select
     *      适用格式：
     *      $datas = array(123, 'abc')
     *
     * @param array   $datas
     * @param string  $choose 默认选中
     * @param string  $name 名称、ID
     * @param string  $onchange 事件
     * @param boolean $disabled
     * @param mixed   $fst
     *
     * @return string
     */
    public static function getSelect4($datas=array(), $choose='', $name='dft', $onchange='', $disabled=false, $fst='---')
    {
        $dis = $disabled ? 'disabled="disabled"' : '';
        $html = '<select autocomplete="off" name="'.$name.'" id="'.$name.'" onchange="'.$onchange.'" '.$dis.'>';
        if(false !== $fst)
        {
            $html .= '<option value="-1">'.$fst.'</option>';
        }
        if( ! empty($datas))
        {
            foreach($datas as $v)
            {
                $sel = ($v === $choose) ? 'selected="selected"' : '';
                $html .= '<option value="'.$v.'" '.$sel.'>'.$v.'</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }

    /**
     * 组装select
     *      适用格式：
     *      $datas = array('a'=>array('k'=>'a', 'v'=>'xxx'), 'b'=>array('k'=>'b', 'v'=>'yyy'))
     *
     * @param array   $datas
     * @param string  $choose 默认选中
     * @param string  $name 名称、ID
     * @param string  $onchange 事件
     * @param boolean $disabled
     * @param mixed   $fst
     *
     * @return string
     */
    public static function getSelect5($datas=array(), $choose='', $name='dft', $onchange='', $disabled=false, $fst='---')
    {
        $dis = $disabled ? 'disabled="disabled"' : '';
        $html = '<select autocomplete="off" name="'.$name.'" id="'.$name.'" onchange="'.$onchange.'" '.$dis.'>';
        if(false !== $fst)
        {
            $html .= '<option value="-1">'.$fst.'</option>';
        }
        if( ! empty($datas))
        {
            foreach($datas as $k=>$v)
            {
                $sel = ($k === $choose) ? 'selected="selected"' : '';
                $html .= '<option value="'.$k.'" '.$sel.'>'.$v['v'].'</option>';
            }
        }
        $html .= '</select>';
        return $html;
    }
}
