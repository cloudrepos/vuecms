<?php
namespace app\common\traits;

trait ErrorRecord
{
    private $_error;

    /**
     * 设置错误信息
     *
     * @param string $str
     *
     * @return void
     */
    public function setError($str = '')
    {
        $this->_error = $str;
    }

    /**
     * 得到错误信息
     *
     * @return string
     */
    public function getError()
    {
        return $this->_error;
    }
}
