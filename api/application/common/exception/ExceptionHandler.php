<?php
namespace app\common\exception;

use Exception;
use think\exception\Handle;

class ExceptionHandler extends Handle
{
    private $c = 1000;
    private $m = 'ok';
    private $d = [];

    public function render(Exception $e)
    {
        if($e instanceof BaseException)
        {
            $this->c = $e->c;
            $this->m = $e->m;
            $this->d = $e->d;
        }
        else
        {
            if(config('app_debug')){
                return parent::render($e);
            }
            $this->c = 1000;
            $this->m = $e->getMessage();
            $this->d = [];
        }
        return json(rs($this->c, $this->m, ['url'=>request()->url()]));
    }
}
