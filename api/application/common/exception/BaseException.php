<?php
namespace app\common\exception;

use think\Exception;

class BaseException extends Exception
{
    public $c = 1000;
    public $m = 'ok';
    public $d = [];

    public function __construct($c=0, $m='', $d=[])
    {
        if($c > 0){ $this->c = $c; }
        if($m != ''){ $this->m = $m; }
        if( ! empty($d)){ $this->d = $d; }
    }
}
