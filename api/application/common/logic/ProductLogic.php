<?php
namespace app\common\logic;

class ProductLogic
{
	private function M()
	{
		return M('product');
	}

	public function ls()
	{
		//站点内容搜索
		siteWhere();
		$data = $this->M()->parseWhere()->pagin()->px()->select();

		foreach ($data as &$d) {
			$d['cate_id'] = getCate($d['cate_id']);
		}

		$total = $this->M()->parseWhere()->count();

		return array('data' => $data, 'total' => $total);
	}

	public function get($id)
	{
		$id = (int) $id;
		$row = $this->M()->find($id);
		return $row;
	}

	public function save($post)
	{
		$id = (int) $post['id'];
		$post['cate_id'] = (int) $post['cate_id'];
		if($id) {
			$rs = $this->M()->update($post);
			if($rs === false) {
				error('保存错误');
			}
		}else{
			$post['admin_id'] = ADMIN_ID;
			$post['admin_name'] = getAdmin(ADMIN_ID);
			$post['add_dt'] = date('Y-m-d');
			$id = $this->M()->insert($post);
			if($id === false) {
				error('添加错误');
			}
		}
		return true;
	}

	public function del($id)
	{
		$id = (int) $id;
		if(!$id) error('删除失败');
		$rs = $this->M()->where("id = $id")->delete();
		if($rs === false) {
			return error('删除失败');
		}else{
			return true;
		}
	}
}