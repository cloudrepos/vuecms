<?php
namespace app\common\logic;

class SiteLogic
{

	private function M()
	{
		return M('site');
	}
	
	public function ls()
	{
		$data = $this->M()->order("id ASC")->select();
		return $data;
	}

	public function get($id)
	{
		$id = (int) $id;
		$row = $this->M()->find($id);
		return $row;
	}

	public function save($post)
	{
		$id = (int) $post['id'];
		if($id) {
			$rs = $this->M()->where('id', $id)->update($post);
			if($rs !== false) {
				return true;
			}
		}else{
			$rs = $this->M()->insert($post);
			if($rs !== false) {
				return true;
			}
		}
		error('保存错误');
	}

	public function del($id)
	{
		$id = (int) $id;
		if(!$id) error( '参数错误');
		$rs = $this->M()->where('id', $id)->delete();
		if($rs === false) {
			error( '删除失败');
		}else{
			return true;
		}
	}

	//修改状态
	public function st($id, $st)
	{
		$rs = $this->M()->where("id = $id")->setField('st', $st);
		if($rs !== false) {
			return array('st' => 1);
		}else{
			return array('st' => 0, 'msg' => '修改状态错误');
		}
	}
}