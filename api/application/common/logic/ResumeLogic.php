<?php
namespace app\common\logic;


class ResumeLogic
{
	private function M()
	{
		return M('resume');
	}
	
    public function ls()
    {
        $data = $this->M()->parseWhere()->order('id desc')->pagin()->select();
        foreach ($data as &$d) {
            $d['ts'] = strtotime($d['ts']); //转为时间戳，在前端以演示时间戳转字符串过滤
        }
        $total = $this->M()->parseWhere()->count();
        
        return array('data' => $data, 'total' => $total);
    }
    
    public function get($id)
    {
        $id = (int) $id;
        $row = $this->M()->find($id);
        $row['school'] = json_decode($row['school'], true);
        $row['project'] = json_decode($row['project'], true);
        $row['attend'] = json_decode($row['attend'], true);
        if(!$row['attend']) $row['attend'] = array();
        return $row;
    }
    
    //职位列表
    public function job()
    {
        $data = $this->M()->field('job')->group('job')->column('job');
        $data = array_filter($data);
        return $data;
    }
    
    public function del($id)
    {
        if(is_numeric($id)) {
            $rs = $this->M()->where('id', $id)->delete();
        }else{
            $rs = $this->M()->where('id', 'in', $id)->delete();
        }
        if($rs === false) {
            error('删除失败');
        }else{
            return true;
        }
    }
    
    public function save($post)
    {
        //允许编辑字段
        $id = (int) $post['id'];
        //附件处理
        foreach($post['attend'] as &$a) {
            unset($a['status']);
            unset($a['uid']);
        }
        $post['attend'] = json_encode($post['attend'], 256);
        if($id) {
	        $post['project']= json_encode($post['project'], 256);
	        $post['school']= json_encode($post['school'], 256);
            $rs = $this->M()->where('id', $id)->update($post);
        }else{
            $post['ts'] = date('Y-m-d H:i:s');
            $rs = $this->M()->insert($post);
        }
        if($rs !== false) {
	        return true;
        }else{
	        error('保存错误');
        }
    }

	//修改状态
	public function st($id, $st)
	{
		$rs = $this->M()->where('id', $id)->setField('st', $st);
		if($rs !== false) {
			return true;
		}else{
			error('修改状态错误');
		}
	}
}