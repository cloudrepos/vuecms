<?php
namespace app\common\logic;

class NewsLogic
{
	private function M()
	{
		return M('news');
	}

	public function ls()
	{
		//站点内容搜索
		siteWhere();
		$data = $this->M()->parseWhere()->pagin()->px()->select();

		foreach ($data as &$d) {
			$d['cate_id'] = getCate($d['cate_id']);
		}
		$total = $this->M()->parseWhere()->count();

		return array('data' => $data, 'total' => $total);
	}

	public function get($id)
	{
		$id = (int) $id;
		$row = $this->M()->find($id);
		return $row;
	}

	public function save($post)
	{
		$id = (int) $post['id'];
		$post['cate_id'] = (int) $post['cate_id'];
		if($id) {
			$rs = $this->M()->update($post);
			if($rs !== false) {
				return true;
			}
		}else{
			$post['admin_id'] = ADMIN_ID;
			$post['admin_name'] = getAdmin(ADMIN_ID);
			$post['add_dt'] = date('Y-m-d');
			$rs = $this->M()->insert($post);
			if($rs !== false) {
				return true;
			}
		}
		return error('保存错误');
	}

	public function del($id)
	{
		$id = (int) $id;
		if(!$id) error('参数错误');
		$rs = $this->M()->where('id', $id)->delete();
		if($rs === false) {
			error('删除失败');
		}else{
			return true;
		}
	}

	//修改状态
	public function st($id, $st)
	{
		$rs = $this->M()->where('id', $id)->setField('st', $st);
		if($rs !== false) {
			return true;
		}else{
			error('修改状态错误');
		}
	}
}