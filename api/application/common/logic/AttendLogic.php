<?php
namespace app\common\logic;

class AttendLogic
{
	/**
	 * 获取附件列表
	 * @param $params array		条件数组
	 * @return array
	 */
	static function ls($params)
	{
		//列表
		$_POST = $params;

		$data = M('attend')->parseWhere()->order('id asc')->select();
		foreach($data as &$d) {
			$d['tag'] = json_decode($d['tag'], true);
			$d['tag'] = implode(' ', $d['tag']);
		}
		return $data;
	}

	/**
	 * 保存附件
	 * @param $module	模块
	 * @param $pk		id
	 * @param $attend	附件数据
	 */
	static function save($module, $pk, $attend)
	{
		$old_ids = getCol("SELECT id FROM attend WHERE module = '$module' AND `pk` = $pk");
		$new_ids = array();
		foreach($attend as $a) {
			$a['up_dt'] = date('Y-m-d');
			$a['tag'] = explode(' ', $a['tag']);
			$a['tag'] = json_encode($a['tag'], 256);
			if($a['id']) {	//保存
				$rs = M('attend')->update($a);
			}else{	//添加
				$a['module'] = $module;
				$a['pk'] = $pk;
				$a['add_dt'] = date('Y-m-d');
				$rs = $a['id'] = M('attend')->insertGetId($a);
			}
			if($rs === false) json(0, '保存附件信息错误');
			$new_ids[] = $a['id'];
		}
		$del_ids = array_diff($old_ids, $new_ids);
		if($del_ids) {
			$rs = M('attend')->where('id', 'in', $del_ids)->delete();
			if($rs === false) json(0, '删除附件信息失败');
		}
	}
}