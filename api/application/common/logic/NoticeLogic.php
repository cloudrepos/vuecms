<?php
namespace app\common\logic;

class NoticeLogic
{
	private function M()
	{
		return M('notice');
	}

	public function ls()
	{
		//站点内容搜索
		siteWhere();
		$data = $this->M()->parseWhere()->pagin()->px()->select();

		return $data;
	}
    
    public function get($id)
    {
        $id = (int) $id;
        $row = $this->M()->find($id);
        return $row;
    }
    
    public function del($id)
    {
        if(is_numeric($id)) {
            $rs = $this->M()->where('id', $id)->delete();
        }else{
            $rs = $this->M()->where('id', 'in', $id)->delete();
        }
        if($rs === false) {
        	error('删除失败');
        }else{
            return true;
        }
    }
    
    public function save($post)
    {
        $id = (int) $post['id'];
        //允许编辑字段
        if($id) {
            $rs = $this->M()->where('id', $id)->update($post);
        }else{
            $post['ts'] = time();
            $rs = $this->M()->insert($post);
        }
        if($rs !== false) {
	        return true;
        }else{
	        error('保存错误');
        }
    }

    //修改状态
    public function st($id, $st)
    {
        $rs = $this->M()->where('id', $id)->setField('st', $st);
        if($rs !== false) {
	        return true;
        }else{
	        error('修改状态错误');
        }
    }
}