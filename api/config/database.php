<?php
if(isset($_SERVER['DB_HOST'])) {
	//本地开发环境推荐把数据库信息配置在apache虚拟主机里，省得不同开发人员的配置冲突
	/*<VirtualHost *:80>
		DocumentRoot "D:\www\sangerMVC\api\Public"
		ServerName api.sangermvc.lc
		SetEnv   DB_HOST           192.168.10.51
		SetEnv   DB_USER           sgmvc
		SetEnv   DB_PWD            123456
		SetEnv   DB_NAME           sangermvc
	</VirtualHost>*/
	$db_host = $_SERVER['DB_HOST'];
	$db_user = $_SERVER['DB_USER'];
	$db_pwd = $_SERVER['DB_PWD'];
	$db_name = $_SERVER['DB_NAME'];
}else{
	//线上推荐根据api域名配置不同参数，多一个域名就多一份配置
	if($_SERVER['HTTP_HOST'] == 'mvc_apix.qn40.com'){
		$db_host = '127.0.0.1';
		$db_pwd = 'mvc';
		$db_user = 'mvc';
		$db_name = 'mvc';
	}
}
return [
    // 数据库类型
    'type'            => 'mysql',
    // 服务器地址
    'hostname'        => $db_host,
    // 数据库名
    'database'        => $db_name,
    // 用户名
    'username'        => $db_user,
    // 密码
    'password'        => $db_pwd,
    // 端口
    'hostport'        => '',
    // 连接dsn
    'dsn'             => '',
    // 数据库连接参数
    'params'          => [],
    // 数据库编码默认采用utf8
    'charset'         => 'utf8',
    // 数据库表前缀
    'prefix'          => '',
    // 数据库调试模式
    'debug'           => true,
    // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
    'deploy'          => 0,
    // 数据库读写是否分离 主从式有效
    'rw_separate'     => false,
    // 读写分离后 主服务器数量
    'master_num'      => 1,
    // 指定从服务器序号
    'slave_no'        => '',
    // 自动读取主库数据
    'read_master'     => false,
    // 是否严格检查字段是否存在
    'fields_strict'   => true,
    // 数据集返回类型
    'resultset_type'  => 'array',
    // 自动写入时间戳字段
    'auto_timestamp'  => false,
    // 时间字段取出后的默认时间格式
    'datetime_format' => 'Y-m-d H:i:s',
    // 是否需要进行SQL性能分析
    'sql_explain'     => false,
    // Builder类
    'builder'         => '',
    // Query类
    'query'           => '\\think\\db\\Query',
    // 是否需要断线重连
    'break_reconnect' => false,
    // 断线标识字符串
    'break_match_str' => [],

	//mongodb配置
	'mongo' => [
		'type'        => '\think\mongo\Connection',
		'query'          => '\think\mongo\Query',   // 设置查询类
		'hostname'    => '192.168.10.186',
		'database'    => 'Biosphere',
		'hostport'        => 27017,
		'username'    => '',
		'password'    => '',
		'charset'     => 'utf8',
		'prefix'      => '',
		// 强制把_id转换为id
		'pk_convert_id'  => true,
	],
];
