<?php
use think\facade\Route;

use app\api\middleware\Cors;
use app\api\middleware\CheckJwtToken;

Route::domain('api', function () {

	Route::option('*', 'api/index/index')->middleware([Cors::class]);

	//index
	Route::get('/', 'api/Index/index');
	Route::any('index/login', 'api/Auth/login');
	Route::any('index/remark', 'api/index/remark');
	Route::any('index/upload', 'api/index/upload')->middleware([CheckJwtToken::class]);


	//admin
	Route::group('admin', function (){
		Route::any('/ls', 'api/admin/ls');
		Route::get('/get/id/:id', 'api/admin/get');
		Route::get('/del/id/:id', 'api/admin/del');
		Route::post('/edit', 'api/admin/edit');
		Route::any('/save', 'api/admin/save');
		Route::get('/st/id/:id/st/:st', 'api/admin/st');
	})->middleware([CheckJwtToken::class]);


	//repeat
	Route::group('repeat', function (){
		Route::any('/ls', 'api/repeat/ls');
		Route::get('/get/id/:id', 'api/repeat/get');
		Route::get('/del/id/:id', 'api/repeat/del');
		Route::post('/save', 'api/repeat/save');
		Route::get('/st/id/:id/st/:st', 'api/repeat/st');
	})->middleware([CheckJwtToken::class]);

	//repeatItem
	Route::group('repeat_item', function (){
		Route::any('/ls/pid/:pid', 'api/repeat_item/ls');
		Route::get('/get/id/:id', 'api/repeat_item/get');
		Route::get('/del/id/:id', 'api/repeat_item/del');
		Route::any('/save', 'api/repeat_item/save');
	})->middleware([CheckJwtToken::class]);


	//graph
	Route::group('graph', function() {
		Route::post('/draw', 'api/graph/draw');
	})->middleware([CheckJwtToken::class]);



	//cate
	Route::group('cate', function (){
		Route::any('/ls/module/:module', 'api/cate/ls');
		Route::get('/get/id/:id', 'api/cate/get');
		Route::get('/del/id/:id', 'api/cate/del');
		Route::any('/save', 'api/cate/save');
		Route::any('/save_setting/id/:id', 'api/cate/save_setting');
		Route::any('/map/module/:module', 'api/Cate/map');//输出产品分类map
		Route::get('/custom/id/:id', 'api/Cate/custom');//获取分类自定义字段
	})->middleware([CheckJwtToken::class]);

	//product
	Route::group('product', function (){
		Route::any('/ls', 'api/Product/ls');
		Route::get('/get/id/:id', 'api/Product/get');
		Route::get('/del/id/:id', 'api/Product/del');
		Route::any('/save', 'api/product/save');
		Route::get('/cate', 'api/product/cate');
	})->middleware([CheckJwtToken::class]);

	//news
	Route::group('news', function (){
		Route::any('/ls', 'api/news/ls');
		Route::any('/get/id/:id', 'api/news/get');
		Route::get('/del/id/:id', 'api/news/del');
		Route::any('/save', 'api/news/save');
		Route::get('/cate', 'api/news/cate');
		Route::get('/st/id/:id/st/:st', 'api/news/st');
	})->middleware([CheckJwtToken::class]);

	//notice
	Route::group('notice', function (){
		Route::any('/ls', 'api/notice/ls');
		Route::get('/get/id/:id', 'api/notice/get');
		Route::get('/del/id/:id', 'api/notice/del');
		Route::any('/save', 'api/notice/save');
		Route::get('/job/id/:id', 'api/notice/job');
		Route::get('/st/id/:id/st/:st', 'api/notice/st');
	})->middleware([CheckJwtToken::class]);

	//resume
	Route::group('resume', function (){
		Route::any('/ls', 'api/resume/ls');
		Route::get('/get/id/:id', 'api/resume/get');
		Route::get('/del/id/:id', 'api/resume/del');
		Route::any('/save', 'api/resume/save');
		Route::get('/job', 'api/resume/job');
		Route::get('/st/id/:id/st/:st', 'api/resume/st');
	})->middleware([CheckJwtToken::class]);

	//role
	Route::group('role', function (){
		Route::any('/ls', 'api/role/ls');
		Route::get('/get/id/:id', 'api/role/get');
		Route::any('/save', 'api/role/save');
		Route::get('/map', 'api/role/map');
		Route::get('/allpri', 'api/role/allpri');
		Route::post('/px', 'api/role/px');
		Route::get('/st/id/:id/st/:st', 'api/role/st');
	})->middleware([CheckJwtToken::class]);

	//piece
	Route::group('piece', function (){
		Route::any('/ls', 'api/piece/ls');
		Route::get('/get/id/:id', 'api/piece/get');
		Route::get('/del/id/:id', 'api/piece/del');
		Route::any('/save', 'api/piece/save');
		Route::get('/st/id/:id/st/:st', 'api/piece/st');
	})->middleware([CheckJwtToken::class]);

	//special
	Route::group('special', function (){
		Route::any('/ls', 'api/special/ls');
		Route::get('/get/id/:id', 'api/special/get');
		Route::get('/del/id/:id', 'api/special/del');
		Route::any('/save', 'api/special/save');
		Route::get('/cate', 'api/special/cate');
	})->middleware([CheckJwtToken::class]);

	//notice
	Route::group('notice', function (){
		Route::any('/ls', 'api/notice/ls');
		Route::get('/get/id/:id', 'api/notice/get');
		Route::get('/job/id/:id', 'api/notice/job');
		Route::get('/del/id/:id', 'api/notice/del');
		Route::any('/save', 'api/notice/save');
		Route::get('/st/id/:id/st/:st', 'api/notice/st');
	})->middleware([CheckJwtToken::class]);

	//ueditor
	Route::group('ueditor', function (){
		Route::any('/config', 'api/ueditor/config');
		Route::any('/ls', 'api/ueditor/ls');
		Route::any('/upload', 'api/ueditor/upload');
	})->middleware([CheckJwtToken::class]);

	//remark
	Route::group('remark', function (){
		Route::any('/ls', 'api/remark/ls');
		Route::get('/content', 'api/remark/content');
		Route::get('/get/id/:id', 'api/remark/get');
		Route::get('/del/id/:id', 'api/remark/del');
		Route::post('/save', 'api/remark/save');
		Route::get('/st/id/:id/st/:st', 'api/remark/st');
	})->middleware([CheckJwtToken::class]);

	//site
	Route::group('site', function (){
		Route::any('/ls', 'api/site/ls');
		Route::get('/map', 'api/site/map');
		Route::get('/get/id/:id', 'api/site/get');
		Route::get('/del/id/:id', 'api/site/del');
		Route::post('/save', 'api/site/save');
		Route::get('/st/id/:id/st/:st', 'api/site/st');
	})->middleware([CheckJwtToken::class]);
	//没有匹配到路由
	Route::miss('pub/miss');

});
