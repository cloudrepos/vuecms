<?php
use think\facade\Route;



Route::domain('www', function () {
	Route::get('/', 'www/index/index');
	Route::get('/ls', 'www/index/ls');
	Route::get('/contact', 'www/index/contact');
	Route::get('/detail', 'www/index/detail');

	//没有匹配到路由
	Route::miss('pub/miss');
});
